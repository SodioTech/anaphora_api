from pymongo import MongoClient
import time
import commands
import random
from geopy.geocoders import Nominatim

geolocator = Nominatim()

lat_lng = [(22.5329291,88.37857833),
(22.51290667,88.37512264),
(22.51566101,88.3421182),
(22.50554993,88.40896923),
(22.51565751,88.32157836),
(22.61991452,88.39999505),
(22.53461812,88.40082792),
(22.49366313,88.37115497),
(22.61313578,88.28725472),
(22.56014685,88.44071821),
(22.55770326,88.33511548),
(22.56650301,88.2775899),
(22.50177348,88.34865866),
(22.63300191,88.31365716),
(22.63818324,88.38320715),
(22.65582822,88.35158672),
(22.64246348,88.41662447),
(22.5988155,88.34917248),
(22.58473336,88.4349543),
(22.57189459,88.30615335),
(22.64248204,88.37055615),
(22.63828527,88.37982224),
(22.62203996,88.42593943),
(22.56121874,88.36678041),
(22.49502567,88.33368861),
(22.53860784,88.34163194),
(22.58695571,88.26990182),
(22.59772061,88.31807904),
(22.51411638,88.35216041),
(22.49192255,88.3610112),
(22.55133434,88.37989324),
(22.5648449,88.40965156),
(22.62770847,88.32653524),
(22.54495211,88.39822568),
(22.594318,88.38306887),
(22.59682188,88.36962985),
(22.54464311,88.37067932),
(22.55190349,88.32547256),
(22.58699335,88.45175712),
(22.64234802,88.33751527),
(22.63827965,88.29899787),
(22.58923152,88.28364446),
(22.51685004,88.37700774),
(22.54562293,88.31880006),
(22.56225961,88.31751378),
(22.60036483,88.31095503),
(22.60453235,88.32207199),
(22.59921738,88.31831393),
(22.64037151,88.38383986),
(22.61549962,88.33609986)]

users = ["code32","perry","toto","lucy","faheelkamran","nave"]

client = MongoClient('localhost',27017)
dbase = client['anaphora_api_sample']
result = dbase.collection_names()
print result

def toSecs(t):
    return int(t.split(":")[0])*3600 + int(t.split(":")[1])*60

for i in range(len(users)):
    epoch_milliseconds = time.time()
    #location = geolocator.reverse(str(lat_lng[i][0]) + "," + str(lat_lng[i][1]))
    #data = {
    #    "userName": users[i],
    #    "userLocation": [lat_lng[i][0],lat_lng[i][1]],
    #    "locationName" : location.address,
    #    "logTimeStamp": epoch_milliseconds
    #}
    #print data
    #result = dbase.locationcurrents.insert_one(data)
    #print result.inserted_id

for i in range(len(lat_lng)):
    epoch_milliseconds = time.time()
    location = geolocator.reverse(str(lat_lng[i][0]) + "," + str(lat_lng[i][1]))
    index = random.randint(0,len(users) - 1)
    data = {
        "locationHistoryLoggedID": "location" + str(epoch_milliseconds),
        "userName": users[index],
        "userLocation": [lat_lng[i][0],lat_lng[i][1]],
        "locationName" : location.address,
        "logTimeStamp": epoch_milliseconds
    }
    print data
    result = dbase.locationhistories.insert_one(data)
    print result.inserted_id
