'use strict';
var Model = require('./ModelLinks.js');

/*
Official Guide
https://socket.io/docs/rooms-and-namespaces/
*/

function userOnline(io, socket, conversation_JSON_data, client_browser) {
    Model.UserSockets.findOne({
            userName: conversation_JSON_data.userName
        })
        .then(data => {
            // console.log(data);
            if (data != null) {
                return Model.UserSockets.update({
                    userName: conversation_JSON_data.userName
                }, {
                    socketID: socket.id,
                    status: true,
                    client: client_browser,
                }, {
                    multi: true,
                    upsert: false
                })
            } else {
                return Model.UserSockets({
                    userName: conversation_JSON_data.userName,
                    socketID: socket.id,
                    status: true,
                    chatroom_status: false,
                    chatroom_user_coord: [0.00, 0.00],
                    client: client_browser,
                }).save()
            }
        })
        .then(data => {
            // console.log(data);
        })
        .catch(err => {
            // console.log(err);
        });
}

function userOffline(io, socket, conversation_JSON_data) {
    Model.UserSockets.update({
            socketID: socket.id
        }, {
            status: false,
            chatroom_status: false,
        }, {
            multi: true,
            upsert: false
        })
        .then(data => {
            // console.log(data);
            return Model.ChatRoom.find({
                socketID: socket.id
            })
        })
        .then(data => {
            for (var i = 0; i < data.length; i++) {
                io.to(data[i].conversationGroupID).emit('left', data[i]);
            }
            return Model.ChatRoom.deleteMany({
                socketID: socket.id
            })
        })
        .then(data => {
            // console.log(data);
        })
        .catch(err => {
            // console.log(err);
        });
}

function enterGroup(io, socket, conversation_JSON_data) {
    Model.ChatRoom({
            conversationGroupID: conversation_JSON_data.conversationGroupID,
            userName: conversation_JSON_data.userName,
            socketID: socket.id
        }).save()
        .then(data => {
            // console.log(data);
            socket.join(conversation_JSON_data.conversationGroupID);
            io.to(conversation_JSON_data.conversationGroupID).emit('joined', conversation_JSON_data);
            getAllUsersInGroup(io, socket, conversation_JSON_data);
        })
        .catch(err => {
            // console.log(err);
        });
}

function leaveGroup(io, socket) {
    Model.ChatRoom.find({
            socketID: socket.id
        })
        .then(data => {
            for (var i = 0; i < data.length; i++) {
                io.to(data[i].conversationGroupID).emit('left', data[i]);
            }
            return Model.ChatRoom.deleteMany({
                socketID: socket.id
            })
        })
        .then(data => {
            // console.log(data);
        })
        .catch(err => {
            // console.log(err);
        });
}

function getAllUsersInGroup(io, socket, conversation_JSON_data) {
    Model.ChatRoom.find({
            conversationGroupID: conversation_JSON_data.conversationGroupID
        })
        .then(data => {
            // console.log(data);
            io.to(conversation_JSON_data.conversationGroupID).emit("users", data);
        })
        .catch(err => {
            // console.log(err);
        });
}

function getMessageInChatRoom(io, socket, conversation_JSON_data) {
    if (conversation_JSON_data != undefined) {
        // console.log(conversation_JSON_data.conversationsID);
        Model.ConversationList.findOne({
            conversationsID: conversation_JSON_data.conversationsID
        }, function (error, data) {
            if (error || data == null) {
                // console.log(error);
                io.to(conversation_JSON_data.conversationGroupID).emit('error', error);
            } else {
                // console.log(data);
                io.to(conversation_JSON_data.conversationGroupID).emit('refresh', data);
            }
        });
    } else {
        // io.to(conversation_JSON_data.conversationGroupID).emit('error', "Data not found");
    }
}

function getDataInChatRoom(io, socket, conversation_JSON_data) {
    if (conversation_JSON_data != undefined) {
        io.to(conversation_JSON_data.conversationGroupID).emit('data', conversation_JSON_data);
    } else {
        // io.to(conversation_JSON_data.conversationGroupID).emit('error', "Data not found");
    }
}

function onlineUsers(io, socket, conversation_JSON_data) {
    Model.UserSockets.findOne({
            userName: conversation_JSON_data.userName,
        })
        .then(data => {
            // console.log(data);
            io.to(conversation_JSON_data.conversationGroupID).emit("online", data);
        })
        .catch(err => {
            // console.log(err);
        });
}

function enterChatroomSearch(io, socket, conversation_JSON_data) {

    var lat = parseFloat(conversation_JSON_data.lat).toFixed(6);
    var lng = parseFloat(conversation_JSON_data.lng).toFixed(6);

    Model.UserSockets.update({
            socketID: socket.id
        }, {
            user_gender: conversation_JSON_data.user_gender,
            chatroom_status: true,
            chatroom_opt_type: conversation_JSON_data.type,
            chatroom_opt_gender: conversation_JSON_data.gender,
            chatroom_user_coord: [lat, lng]
        }, {
            multi: true,
            upsert: false
        })
        .then(data => {
            console.log(data);
            io.to(socket.id).emit('searching', true);
        })
        .catch(err => {
            console.log(err);
            io.to(socket.id).emit('searching', false);
        });
}

function leaveChatroomSearch(io, socket) {
    console.log(socket.id);
    Model.UserSockets.update({
            socketID: socket.id
        }, {
            chatroom_status: false,
        }, {
            multi: true,
            upsert: false
        })
        .then(data => {
            console.log(data);
        })
        .catch(err => {
            console.log(err);
        });
}

module.exports = function (io) {

    // Set socket.io listeners.
    io.on('connection', (socket) => {
        console.log("Socket ID => ", socket.id);
        console.log("Socket Options => ", socket.handshake.headers['user-agent']);
        console.log("Socket Params => ", socket.handshake.query.userName);

        userOnline(io, socket, socket.handshake.query, socket.handshake.headers['user-agent']);

        socket.on('enter', (conversation_JSON_data) => {
            console.log("SOCKET ========================================== enter");
            console.log(conversation_JSON_data);
            enterGroup(io, socket, conversation_JSON_data);
        });

        socket.on('leave', (conversation_JSON_data) => {
            console.log("SOCKET ========================================== leave");
            userOffline(io, socket);
        });

        socket.on('isonline', (conversation_JSON_data) => {
            console.log("SOCKET ========================================== leave");
            onlineUsers(io, socket, conversation_JSON_data);
        });

        socket.on('disconnecting', (reason) => {
            console.log("SOCKET ========================================== disconnected");
            userOffline(io, socket);
        });

        socket.on('message', (conversation_JSON_data) => {
            console.log("SOCKET ========================================== message");
            console.log(conversation_JSON_data);
            getMessageInChatRoom(io, socket, conversation_JSON_data);
        });

        socket.on('data', (conversation_JSON_data) => {
            console.log("SOCKET ========================================== data");
            console.log(conversation_JSON_data);
            getDataInChatRoom(io, socket, conversation_JSON_data);
        });

        socket.on('status', (conversation_JSON_data) => {
            console.log("SOCKET ========================================== status");
            console.log(conversation_JSON_data);
            getAllUsersInGroup(io, socket, conversation_JSON_data)
        });

        socket.on('chaton', (conversation_JSON_data) => {
            console.log("SOCKET ========================================== chaton");
            console.log(conversation_JSON_data);
            enterChatroomSearch(io, socket, conversation_JSON_data);
        });

        socket.on('chatoff', () => {
            console.log("SOCKET ========================================== chatoff");
            leaveChatroomSearch(io, socket);
        });

    });
};
