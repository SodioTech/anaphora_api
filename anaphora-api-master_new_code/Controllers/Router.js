'use strict';
var express = require('express');
var expressListRoutes   = require('express-list-routes');

const MiddlewareProcess = require('./Middleware'),
UserCredController = require('./../Functions/UserCred'),
UserPostsController = require('./../Functions/UserPosts'),
LocationFetchController = require('./../Functions/LocationFetch'),
LocationLogController = require('./../Functions/LocationLog'),
FeedFetchersController = require('./../Functions/FeedFetchers'),
ConnectionsController = require('./../Functions/Connections'),
FileUploadUtilityController = require('./../Functions/Utility/FileUploadUtility'),
OperationsController = require('./../Functions/Operations'),
AlertsController = require('./../Functions/AlertManager'),
RecommendationController = require('./../Functions/Recommendation');

module.exports = function (app) {

    const apiRoutes = express.Router(),
    authRoutes = express.Router(),
    profileRoutes = express.Router(),
    postRoutes = express.Router(),
    userRoutes = express.Router(),
    locationRoutes = express.Router(),
    historyRoutes = express.Router(),
    feedRoutes = express.Router(),
    chatRoutes = express.Router(),
    fileRoutes = express.Router(),
    recommRoutes = express.Router(),
    notificationRoutes = express.Router();

    authRoutes.post('/signup', UserCredController.userRegistration, UserCredController.userLogin);
    authRoutes.post('/signin', UserCredController.userLogin);
    authRoutes.post('/adminLogin', UserCredController.adminLogin);
    authRoutes.put('/reset', UserCredController.resetPassword);
    authRoutes.get('/email/:email', UserCredController.getUsername);
    authRoutes.get('/photo/:userName', UserCredController.userGetProfileImage);

    profileRoutes.put('/update', UserCredController.userProfileUpdate, UserCredController.userGetProfileDetails);
    profileRoutes.get('/details/:userName', UserCredController.userGetProfileDetails);
    //FOR ADMIN
    profileRoutes.put('/BlockOrUnblockUser', UserCredController.userBlockOrUnblock);
    profileRoutes.post('/usersDetails', UserCredController.getAllUsersDetails);

    postRoutes.post('/add', UserPostsController.addPostToFeed);
    postRoutes.post('/report', UserPostsController.reportPost);
    postRoutes.post('/delete', UserPostsController.deletePost);
    postRoutes.post('/like', UserPostsController.likePost);
    postRoutes.post('/comment/add', UserPostsController.addCommentToPost);
    postRoutes.post('/comment/remove', UserPostsController.removeCommentFromPost);

    locationRoutes.get('/:lat/:lng/:distance/:offset/:limit', LocationFetchController.searchAroundUser);

    historyRoutes.post('/location/log', LocationLogController.logUserLocationHistory);

    userRoutes.get('/followers', ConnectionsController.myFollowers);
    userRoutes.get('/following', ConnectionsController.iAmFollowing);
    userRoutes.post('/send', ConnectionsController.connectionRequest);
    userRoutes.post('/block', ConnectionsController.blockConnection);

    feedRoutes.get('/my/:userName/:fromtime', FeedFetchersController.myFeed);
    feedRoutes.get('/connections/:fromtime', FeedFetchersController.myConnectionFeed);
    feedRoutes.post('/search', FeedFetchersController.searchPublicFeed);
    feedRoutes.get('/details/:postID', FeedFetchersController.getFeed);

    chatRoutes.post('/create', OperationsController.createConversationGroup);
    chatRoutes.post('/random', OperationsController.createRandomConversationGroup, OperationsController.createConversationGroup);
    chatRoutes.get('/', OperationsController.getAllConversationGroups);
    chatRoutes.get('/find/:search_text', OperationsController.findInAllConversationGroups);
    chatRoutes.post('/update', OperationsController.updateConversationGroup);
    chatRoutes.post('/delete/', OperationsController.deleteConversationGroup);
    chatRoutes.post('/user/add', OperationsController.addUserToConversationGroup);
    chatRoutes.post('/user/remove', OperationsController.removeUserFromConversationGroup);
    chatRoutes.post('/message/send', OperationsController.sendMessageToConversationGroup);
    chatRoutes.post('/message/read', OperationsController.readMessagesByUserConversationGroup);
    chatRoutes.post('/message', OperationsController.getAllMessagesConversationGroup);
    chatRoutes.post('/message/delete', OperationsController.deleteMessageFromConversationGroup);

    notificationRoutes.get('/', AlertsController.getAllAlerts);

    fileRoutes.post('/:type', FileUploadUtilityController.fileUpload);

    recommRoutes.post('/', RecommendationController.recommend);
    recommRoutes.get('/:userName', RecommendationController.getRecommendation);
    recommRoutes.post('/my', RecommendationController.getMyRecommendations);

    apiRoutes.use('/auth', authRoutes);
    apiRoutes.use('/profile', MiddlewareProcess.logger, profileRoutes);
    apiRoutes.use('/post', MiddlewareProcess.logger, postRoutes);
    apiRoutes.use('/explore', MiddlewareProcess.logger, locationRoutes);
    apiRoutes.use('/history', MiddlewareProcess.logger, historyRoutes);
    apiRoutes.use('/connection', MiddlewareProcess.logger, userRoutes);
    apiRoutes.use('/feed', MiddlewareProcess.logger, feedRoutes);
    apiRoutes.use('/chat', MiddlewareProcess.logger, chatRoutes);
    apiRoutes.use('/notifications', MiddlewareProcess.logger, notificationRoutes);
    apiRoutes.use('/recommend', MiddlewareProcess.logger, recommRoutes);
    apiRoutes.use('/file', fileRoutes);

    // expressListRoutes({ prefix: '/api/auth' }, 'API: Authentication', authRoutes );
    // expressListRoutes({ prefix: '/api/profile' }, 'API: User Profile', profileRoutes );
    // expressListRoutes({ prefix: '/api/post' }, 'API: Posts', postRoutes );
    // expressListRoutes({ prefix: '/api/explore' }, 'API: Explore Users Around', locationRoutes );
    // expressListRoutes({ prefix: '/api/history' }, 'API: Log User Location and Searches', historyRoutes );
    // expressListRoutes({ prefix: '/api/connection' }, 'API: User Connections', userRoutes );
    // expressListRoutes({ prefix: '/api/feed' }, 'API: User Post Feeds', feedRoutes );
    // expressListRoutes({ prefix: '/api/chat' }, 'API: Chat', chatRoutes );
    // expressListRoutes({ prefix: '/api/file' }, 'API: File Uploads', fileRoutes );

    // Set url for API group routes
    // route middleware that will happen on every request
    app.use('/api', apiRoutes);
};
