"use strict";
var Model = require("./ModelLinks.js");
const socketIDs = {};
console.log(socketIDs);
/*
Official Guide
https://socket.io/docs/rooms-and-namespaces/
*/

function userOnline(io, socket, conversation_JSON_data, client_browser) {
	Model.UserSockets.findOne({
			userName: conversation_JSON_data.userName
		})
		.then(data => {
			// console.log(data);
			if (data != null) {
				return Model.UserSockets.update({
					userName: conversation_JSON_data.userName
				}, {
					socketID: socket.id,
					status: true,
					client: client_browser
				}, {
					multi: true,
					upsert: false
				});
			} else {
				return Model.UserSockets({
					userName: conversation_JSON_data.userName,
					socketID: socket.id,
					status: true,
					chatroom_status: false,
					chatroom_user_coord: [1.12, 1.12],
					client: client_browser
				}).save();
			}
		})
		.then(data => {
			// console.log(data);
		})
		.catch(err => {
			// console.log(err);
		});
}

function userOffline(io, socket) {
	Model.UserSockets.update({
			socketID: socket.id
		}, {
			status: false,
			chatroom_status: false
		}, {
			multi: true,
			upsert: false
		})
		.then(data => {
			// console.log(data);
			return Model.ChatRoom.find({
				socketID: socket.id
			});
		})
		.then(data => {
			for (var i = 0; i < data.length; i++) {
				io.to(data[i].conversationGroupID).emit("left", data[i]);
			}
			return Model.ChatRoom.deleteMany({
				socketID: socket.id
			});
		})
		.then(data => {
			leaveChatroomSearch(io, socket);
			// console.log(data);
		})
		.catch(err => {
			// console.log(err);
		});
}

function enterGroup(io, socket, conversation_JSON_data) {
	Model.ChatRoom({
			conversationGroupID: conversation_JSON_data.conversationGroupID,
			userName: conversation_JSON_data.userName,
			socketID: socket.id
		})
		.save()
		.then(data => {
			// console.log(data);
			socket.join(conversation_JSON_data.conversationGroupID);
			io
				.to(conversation_JSON_data.conversationGroupID)
				.emit("joined", conversation_JSON_data);
			getAllUsersInGroup(io, socket, conversation_JSON_data);
		})
		.catch(err => {
			// console.log(err);
		});
}

function leaveGroup(io, socket) {
	Model.ChatRoom.find({
			socketID: socket.id
		})
		.then(data => {
			for (var i = 0; i < data.length; i++) {
				io.to(data[i].conversationGroupID).emit("left", data[i]);
				socket.leave(data[i].conversationGroupID);
			}
			return Model.ChatRoom.deleteMany({
				socketID: socket.id
			});
		})
		.then(data => {
			// console.log(data);
		})
		.catch(err => {
			// console.log(err);
		});
}

function getAllUsersInGroup(io, socket, conversation_JSON_data) {
	Model.ChatRoom.find({
			conversationGroupID: conversation_JSON_data.conversationGroupID
		})
		.then(data => {
			// console.log(data);
			io.to(conversation_JSON_data.conversationGroupID).emit("users", data);
		})
		.catch(err => {
			// console.log(err);
		});
}

function getMessageInChatRoom(io, socket, conversation_JSON_data) {
	if (conversation_JSON_data != undefined) {
		// console.log(conversation_JSON_data.conversationsID);
		Model.ConversationList.findOne({
				conversationsID: conversation_JSON_data.conversationsID
			},
			function (error, data) {
				if (error || data == null) {
					// console.log(error);
					io
						.to(conversation_JSON_data.conversationGroupID)
						.emit("error", error);
				} else {
					// console.log(data);
					io
						.to(conversation_JSON_data.conversationGroupID)
						.emit("refresh", data);
				}
			}
		);
	} else {
		// io.to(conversation_JSON_data.conversationGroupID).emit('error', "Data not found");
	}
}

function updateHomeChats(io, socket, data) {
	console.log('rec', socketIDs[data.receiver], socket.id, socketIDs);
	if (data) {
		io.to(socketIDs[data.receiver]).emit('gotRealTimeInfo', data);
	}
};

function getDataInChatRoom(io, socket, conversation_JSON_data) {
	if (conversation_JSON_data != undefined) {
		io
			.to(conversation_JSON_data.conversationGroupID)
			.emit("data", conversation_JSON_data);
	} else {
		// io.to(conversation_JSON_data.conversationGroupID).emit('error', "Data not found");
	}
}

function onlineUsers(io, socket, conversation_JSON_data) {
	Model.UserSockets.findOne({
			userName: conversation_JSON_data.userName
		})
		.then(data => {
			// console.log(data);
			io.to(conversation_JSON_data.conversationGroupID).emit("online", data);
		})
		.catch(err => {
			// console.log(err);
		});
}

// function enterChatroomSearch(io, socket, conversation_JSON_data) {

//     var lat = parseFloat(conversation_JSON_data.lat).toFixed(6);
//     var lng = parseFloat(conversation_JSON_data.lng).toFixed(6);

//     Model.UserSockets.update({
//             socketID: socket.id
//         }, {
//             user_gender: conversation_JSON_data.user_gender,
//             chatroom_status: true,
//             chatroom_opt_type: conversation_JSON_data.type,
//             chatroom_opt_gender: conversation_JSON_data.opt_gender,
//             chatroom_user_coord: [lat, lng]
//         }, {
//             multi: true,
//             upsert: false
//         })
//         .then(data => {
//             console.log(data);
//             io.to(socket.id).emit('searching', true);
//         })
//         .catch(err => {
//             console.log(err);
//             io.to(socket.id).emit('searching', false);
//         });
// }

function searchChatroom(io, socket, conversation_JSON_data) {
	var socket_user_list_flag = false;
	var distance = 100 / 111.128;
	var lat = parseFloat(conversation_JSON_data.lat);
	var lng = parseFloat(conversation_JSON_data.lng);
	var limit = 2;
	var matcher = {
		$and: [{
				user_gender: conversation_JSON_data.opt_gender
			},
			{
				chatroom_opt_gender: conversation_JSON_data.user_gender
			},
			{
				connected: true
			},
			{
				conversationGroupID: {
					$ne: "-"
				}
			}
		]
	};
	if (conversation_JSON_data.type == "group") {
		limit = Math.floor(Math.random() * 10 + 5);
		matcher = {
			$and: [{
					connected: true
				},
				{
					conversationGroupID: {
						$ne: "-"
					}
				}
			]
		};
	}
	var conversationGroupIDRef = {};

	var searchquery = [{
			$geoNear: {
				near: [lat, lng],
				maxDistance: 20,
				distanceField: "distance",
				spherical: true
			}
		},
		{
			$match: matcher
		},
		{
			$lookup: {
				from: "conversationgrouplists",
				localField: "conversationGroupID",
				foreignField: "conversationGroupID",
				as: "chatroom"
			}
		},
		{
			$unwind: "$chatroom"
		},
		{
			$addFields: {
				opt_type: "$chatroom.type"
			}
		},
		{
			$match: {
				opt_type: conversation_JSON_data.type
			}
		}
	];

	console.log(JSON.stringify(searchquery, null, "    "));

	Model.UserSockets.update({
			socketID: socket.id
		}, {
			user_gender: conversation_JSON_data.user_gender,
			chatroom_status: true,
			chatroom_opt_type: conversation_JSON_data.type,
			chatroom_opt_gender: conversation_JSON_data.opt_gender,
			chatroom_user_coord: [lat.toFixed(6), lng.toFixed(6)]
		}, {
			multi: true,
			upsert: false
		})
		.then(data => {
			console.log(
				"==================================================== Start Search"
			);
			io.to(socket.id).emit("searching", true);
			return Model.UserSockets.aggregate(searchquery);
		})
		.then(data => {
			console.log(
				"==================================================== Search Chatroom"
			);
			console.log(data);
			if (data.length > 0) {
				conversationGroupIDRef.conversationGroupID =
					data[0].conversationGroupID;
				return Model.ConversationGroupList.findOne({
					$and: [{
							conversationGroupID: data[0].conversationGroupID
						},
						{
							chatroomFlag: true
						}
					]
				});
			} else {
				socket_user_list_flag = false;
				// no one with same criteria is online in the area
				var epoch = new Date().getTime() + 10000;
				var modelData = new Model.ConversationGroupList({
					conversationGroupID: conversation_JSON_data.userName + "_" + new Date().getTime(),
					title: "Public Group",
					imageURL: "",
					users: [conversation_JSON_data.userName],
					usersRemoved: [],
					usersDeletedConvGroup: [],
					creatorUserName: conversation_JSON_data.userName,
					creatorIP: "0.0.0.0",
					creatorLocation: "",
					creatorEpochTimeStamp: epoch,
					isArchived: false,
					type: conversation_JSON_data.type,
					limit: limit,
					chatroomFlag: true
				});
				return modelData.save();
			}
		})
		.then(data => {
			console.log(
				"==================================================== Search User"
			);
			console.log(data);
			conversationGroupIDRef = data;
			if (data.users.indexOf(conversation_JSON_data.userName) == -1) {
				// user not present in the conversation
				return Model.ConversationGroupList.update({
					conversationGroupID: data.conversationGroupID
				}, {
					$addToSet: {
						users: conversation_JSON_data.userName
					}
				}, {
					upsert: false,
					multi: true
				});
			} else {
				// user is present in the conversation
				return new Promise(function (resolve, reject) {
					resolve({
						conversationGroupID: data.conversationGroupID
					});
				});
			}
		})
		.then(data => {
			console.log(
				"==================================================== Update Sockets"
			);
			console.log(data);
			return Model.UserSockets.update({
				socketID: socket.id
			}, {
				conversationGroupID: conversationGroupIDRef.conversationGroupID,
				connected: true,
				chatroom_status: false
			}, {
				upsert: false,
				multi: true
			});
		})
		.then(data => {
			console.log(
				"==================================================== Get Chatroom"
			);
			console.log(data);
			return Model.ConversationGroupList.aggregate([{
					$match: {
						conversationGroupID: conversationGroupIDRef.conversationGroupID
					}
				},
				{
					$lookup: {
						from: "usersockets",
						localField: "users",
						foreignField: "userName",
						as: "sockets"
					}
				},
				{
					$lookup: {
						from: "userdetails",
						localField: "users",
						foreignField: "userName",
						as: "user_details"
					}
				}
			]);
		})
		.then(data => {
			console.log(
				"==================================================== Start Chatroom"
			);
			console.log(data);
			conversationGroupIDRef = data[0];

			var groupData = [];
			var groupList = data;
			for (var i = 0; i < groupList.length; i++) {
				var post = {};
				for (var property in groupList[i]) {
					if (groupList[i].hasOwnProperty(property)) {
						post[property] = groupList[i][property];
					}
				}
				var filters = post["user_details"];
				filters = filters.reduce((result, filter) => {
					result[filter.userName] = filter;
					return result;
				}, {});
				post["user_details"] = filters;
				post.last_message = "";
				post.last_message_time = "";
				groupData.push(post);
			}

			for (var i = 0; i < conversationGroupIDRef.sockets.length; i++) {
				console.log(
					"==================================================== CONNECTED => " +
					conversationGroupIDRef.sockets[i].userName
				);
				io
					.to(conversationGroupIDRef.sockets[i].socketID)
					.emit("result", groupData[0]);
			}
		})
		.catch(err => {
			console.log(err);
			io.to(socket.id).emit("searching", false);
		});
}

function cancelChatroomSearch(io, socket) {
	console.log(socket.id);
	var socketUser = {};
	var conversationGroupIDRef = {};
	Model.UserSockets.findOne({
			socketID: socket.id
		})
		.then(data => {
			socketUser = data;
			return Model.UserSockets.update({
				socketID: socket.id
			}, {
				chatroom_status: false,
				conversationGroupID: "-",
				connected: false
			}, {
				multi: true,
				upsert: false
			});
		})
		.then(data => {
			return Model.ConversationGroupList.aggregate([{
					$match: {
						conversationGroupID: socketUser.conversationGroupID
					}
				},
				{
					$lookup: {
						from: "usersockets",
						localField: "users",
						foreignField: "userName",
						as: "sockets"
					}
				}
			]);
		})
		.then(data => {
			if (data.length > 0) {
				conversationGroupIDRef = data[0];
				for (var i = 0; i < conversationGroupIDRef.sockets.length; i++) {
					console.log(
						"==================================================== DISCONNECTED => " +
						conversationGroupIDRef.sockets[i].userName
					);
					io.to(socketUser.conversationGroupID).emit("left", {
						userName: socketUser.userName,
						conversationGroupID: socketUser.conversationGroupID
					});
				}
			}
		})
		.catch(err => {
			console.log(err);
		});
}

function leaveChatroomSearch(io, socket) {
	console.log(socket.id);
	Model.UserSockets.update({
			socketID: socket.id
		}, {
			chatroom_status: false,
			conversationGroupID: "-",
			connected: false
		}, {
			multi: true,
			upsert: false
		})
		.then(data => {
			console.log(data);
		})
		.catch(err => {
			console.log(err);
		});
}

module.exports = function (io) {
	// Set socket.io listeners.
	io.on("connection", socket => {
		console.log("Socket ID => ", socket.id);
		console.log("Socket Options => ", socket.handshake.headers["user-agent"]);
		console.log("Socket Params => ", socket.handshake.query.userName);

		userOnline(
			io,
			socket,
			socket.handshake.query,
			socket.handshake.headers["user-agent"]
		);

		socketIDs[socket.handshake.query.userName] = socket.id;
		console.log(socketIDs);

		socket.on("enter", conversation_JSON_data => {
			console.log("SOCKET ========================================== enter");
			console.log(conversation_JSON_data);
			enterGroup(io, socket, conversation_JSON_data);
		});

		socket.on("leave", conversation_JSON_data => {
			console.log("SOCKET ========================================== leave");
			userOffline(io, socket);
		});

		socket.on("isonline", conversation_JSON_data => {
			console.log("SOCKET ========================================== leave");
			onlineUsers(io, socket, conversation_JSON_data);
		});

		socket.on("disconnecting", reason => {
			console.log("SOCKET ========================================== disconnected");
			userOffline(io, socket);
		});

		socket.on("message", conversation_JSON_data => {
			console.log("SOCKET ========================================== message");
			console.log(conversation_JSON_data);
			getMessageInChatRoom(io, socket, conversation_JSON_data);
		});

		socket.on("updatechats", (conversation_JSON_data) => {
			console.log("SOCKET ======================================== allchats");
			console.log(conversation_JSON_data);
			updateHomeChats(io, socket, conversation_JSON_data);
		});

		socket.on("data", conversation_JSON_data => {
			console.log("SOCKET ========================================== data");
			console.log(conversation_JSON_data);
			getDataInChatRoom(io, socket, conversation_JSON_data);
		});

		socket.on("status", conversation_JSON_data => {
			console.log("SOCKET ========================================== status");
			console.log(conversation_JSON_data);
			getAllUsersInGroup(io, socket, conversation_JSON_data);
		});

		socket.on("search", conversation_JSON_data => {
			console.log("SOCKET ========================================== search");
			console.log(conversation_JSON_data);
			searchChatroom(io, socket, conversation_JSON_data);
		});

		socket.on("chatoff", () => {
			console.log("SOCKET ========================================== chatoff");
			leaveChatroomSearch(io, socket);
		});

		socket.on("cancel", () => {
			console.log("SOCKET ========================================== cancel");
			cancelChatroomSearch(io, socket);
		});
	});
};
