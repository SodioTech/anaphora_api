'use strict';

var Model = require('./../Controllers/ModelLinks.js');

module.exports.logger = function (req, res, next) { // log each request to the console
    //console.log('Time:', Date.now(), req.method, req.url, res.statusCode);
    //console.log(req.headers["authorization"]);
    //console.log(req.headers["username"]);
    //next();

    Model.UserSessionLog.findOne({tokenAssigned:req.headers["authorization"]},function(error_user, data_user){
        if(error_user)
        {
            res.json({
                response:{
                    success: false,
                    message: error_text,
                    data: error_user
                }
            });
        }
        else {
            if(data_user == null){
                res.status(403).json({
                    response:{
                        success: false,
                        message: "Unauthorised Access",
                        data: data_user
                    }
                });
            }
            else{
                console.log("Request method " + req.method);
                console.log("Authorised access by user " + data_user.userName);
                req.userName = data_user.userName;
                // continue doing what we were doing and go to the route
                next();
            }
        }
    });
};
