'use strict';
var Model = require('./ModelLinks.js');

/*
Official Guide
https://socket.io/docs/rooms-and-namespaces/
*/

function getUsersInChatRoom(io, socket, eventName, conversation_JSON_data) {
    Model.ChatRoom.find({
        "conversationGroupID": conversation_JSON_data.conversationGroupID,
        "status": true
    }, {
        "userName": 1,
        "socketID": 1,
        "_id": 0
    }, function (error, data) {
        if (error) {
            console.log(error);
            io.to(conversation_JSON_data.conversationGroupID).emit('error', error);
        } else {
            console.log("============================================6")
            console.log(data);
            io.to(conversation_JSON_data.conversationGroupID).emit(eventName, data);
        }
    });
}

function enterChatRoom(io, socket, conversation_JSON_data, client_browser) {
    // check if the chatroom exists in database
    if (conversation_JSON_data != undefined) {
        Model.ChatRoom.update(conversation_JSON_data, {
            "conversationGroupID": conversation_JSON_data.conversationGroupID,
            "userName": conversation_JSON_data.userName,
            "socketID": socket.id,
            "status": true,
            "client": client_browser
        }, {
            "upsert": true
        }, function (error, data) {
            if (error) {
                console.log(error);
                io.to(conversation_JSON_data.conversationGroupID).emit('error', error);
            } else {
                socket.join(conversation_JSON_data.conversationGroupID);
                getUsersInChatRoom(io, socket, "users", conversation_JSON_data);
                io.to(conversation_JSON_data.conversationGroupID).emit('joined', conversation_JSON_data);
            }
        });
    } else {
        // io.to(conversation_JSON_data.conversationGroupID).emit('error', "Data not found");
    }
}

function leaveChatRoom(io, socket, conversation_JSON_data) {
    console.log("============================================1")
    if (conversation_JSON_data != undefined) {
        Model.ChatRoom.update(conversation_JSON_data, {
            "status": false
        }, {
            "upsert": true
        }, function (error, data) {
            if (error) {
                console.log(error);
                io.to(conversation_JSON_data.conversationGroupID).emit('error', error);
            } else {
                socket.leave(conversation_JSON_data.conversationGroupID);
                getUsersInChatRoom(io, socket, "users", conversation_JSON_data);
                io.to(conversation_JSON_data.conversationGroupID).emit('left', conversation_JSON_data);
            }
        });
    } else {
        Model.ChatRoom.update({
            "socketID": socket.id
        }, {
            "status": false
        }, {
            "upsert": true,
            "multi": true
        }, function (error, data) {
            if (error) {
                console.log(error);
            } else {
                Model.ChatRoom.aggregate([{
                        $match: {
                            "socketID": socket.id
                        }
                    },
                    {
                        $group: {
                            _id: "$socketID",
                            groups: {
                                $addToSet: "$conversationGroupID"
                            },
                            userName: {
                                $first: "$userName"
                            }
                        }
                    }
                ]).exec(function (err, data) {
                    if (error) {
                        console.log(error);
                    } else {
                        var groupList = data[0].groups;
                        console.log(groupList);

                        for (var i = 0; i < groupList.length; i++) {
                            socket.leave(groupList[i]);
                            conversation_JSON_data = {
                                userName: data[0].userName,
                                conversationGroupID: groupList[i]
                            }

                            console.log(conversation_JSON_data);
                            socket.leave(groupList[i]);
                            getUsersInChatRoom(io, socket, "users", conversation_JSON_data);
                            io.to(groupList[i]).emit('left', conversation_JSON_data);
                        }
                        
                    }
                });
            }
        });
        // io.to(conversation_JSON_data.conversationGroupID).emit('error', "Data not found");
    }
}

//[ 'Sam_1518704602093', 'codetanmoy64_1521700401320' ]

function getMessageInChatRoom(io, socket, conversation_JSON_data) {
    if (conversation_JSON_data != undefined) {
        console.log(conversation_JSON_data.conversationsID);
        Model.ConversationList.findOne({
            conversationsID: conversation_JSON_data.conversationsID
        }, function (error, data) {
            if (error || data == null) {
                console.log(error);
                io.to(conversation_JSON_data.conversationGroupID).emit('error', error);
            } else {
                console.log(data);
                io.to(conversation_JSON_data.conversationGroupID).emit('refresh', data);
            }
        });
    } else {
        // io.to(conversation_JSON_data.conversationGroupID).emit('error', "Data not found");
    }
}

function getDataInChatRoom(io, socket, conversation_JSON_data) {
    if (conversation_JSON_data != undefined) {
        io.to(conversation_JSON_data.conversationGroupID).emit('data', conversation_JSON_data);
    } else {
        // io.to(conversation_JSON_data.conversationGroupID).emit('error', "Data not found");
    }
}

module.exports = function (io) {

    // Set socket.io listeners.
    io.on('connection', (socket) => {
        console.log("Socket ID => ", socket.id);
        console.log("Socket Options => ", socket.handshake.headers['user-agent']);

        socket.on('enter', (conversation_JSON_data) => {
            console.log("SOCKET MESSAGE========================================== enter");
            console.log(conversation_JSON_data);
            enterChatRoom(io, socket, conversation_JSON_data, socket.handshake.headers['user-agent']);
        });

        socket.on('leave', (conversation_JSON_data) => {
            console.log("SOCKET ========================================== leave");
            console.log(conversation_JSON_data);
            leaveChatRoom(io, socket, conversation_JSON_data);
        });

        socket.on('disconnecting', (reason) => {
            console.log("SOCKET ========================================== disconnected");
            // console.log(socket.rooms);
            let rooms = Object.keys(socket.rooms);
            // console.log(socket.id);
            leaveChatRoom(io, socket, undefined);
        });

        socket.on('message', (conversation_JSON_data) => {
            console.log("SOCKET ========================================== message");
            console.log(conversation_JSON_data);
            getMessageInChatRoom(io, socket, conversation_JSON_data);
        });

        socket.on('data', (conversation_JSON_data) => {
            console.log("SOCKET ========================================== data");
            console.log(conversation_JSON_data);
            getDataInChatRoom(io, socket, conversation_JSON_data);
        });

        socket.on('status', (conversation_JSON_data) => {
            console.log("SOCKET ========================================== status");
            console.log(conversation_JSON_data);
            getUsersInChatRoom(io, socket, "users", conversation_JSON_data)
        });

    });
};