'use strict';
var Model = require('./../Controllers/ModelLinks.js');
const error_text = "Something Went Wrong! Please check your network or try again!";
var Notification = require('./Utility/Notification.js');

/**
 *@api {post} /api/recommend Recommend/Unrecommend
 *@apiVersion 0.0.1
 *@apiGroup Recommendation
 *@apiPermission all
 *@apiDescription Recommend/Unrecommend a user
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} userName userName of the user recommended/unrecommended
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 * {
 *    "response": {
 *        "success": true,
 *        "message": "You Recommended lucy",
 *        "data": {
 *            "__v": 0,
 *            "updatedAt": "2017-11-07T07:29:19.947Z",
 *            "createdAt": "2017-11-07T07:29:19.947Z",
 *            "userNameRecommended": "lucy",
 *            "userName": "codetanmoy64",
 *            "recommendedOn": "1510039759940",
 *            "_id": "5a0160cf40c9ad08388e3f06"
 *        }
 *    }
 *}
 **/

// Like a post
module.exports.recommend = function (req, res) {

    var epoch_milliseconds = (new Date).getTime();

    var postLike = new Model.Recommends({
        userNameRecommended: req.body.userName,
        userName: req.userName,
        recommendedOn: epoch_milliseconds
    });
    
    Model.UserCredentials.findOne({
        userName: req.body.userName
    }, function (error, data) {
        if (error) {
            res.json({
                response: {
                    success: false,
                    message: error_text,
                    data: error
                }
            });
        } else {
            if (data != null) {
                Model.Recommends.findOne({
                    $and: [{
                            userNameRecommended: req.body.userName
                        },
                        {
                            userName: req.userName
                        }
                    ]
                }, function (error, data_recommendation) {
                    if (error) {
                        res.json({
                            response: {
                                success: false,
                                message: error_text,
                                data: error
                            }
                        });
                    } else {
                        if (data_recommendation == null) {
                            postLike.save(function (error, data) {
                                if (error) {
                                    res.json({
                                        response: {
                                            success: false,
                                            message: error_text,
                                            data: error
                                        }
                                    });
                                } else {
                                  Notification.send(req.body.userName, "Recommendation", req.userName + " recommended you.", "", req.userName, data._id, function (error_notif, data_notif) {
                                        res.json({
                                            response: {
                                                success: true,
                                                message: "You Recommended " + req.body.userName,
                                                data: data
                                            }
                                        });
                                    });
                                }
                            });
                        } else {
                            Model.Recommends.remove({
                                _id: data_recommendation._id
                            }, function (error, data) {
                                if (error) {
                                    res.json({
                                        response: {
                                            success: false,
                                            message: error_text,
                                            data: error
                                        }
                                    });
                                } else {
                                    res.json({
                                        response: {
                                            success: true,
                                            message: "You un-recommended " + req.body.userName,
                                            data: data
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            } else {
                res.json({
                    response: {
                        success: false,
                        message: "User not Found!",
                        data: null
                    }
                });
            }
        }
    });
};

/**
 *@api {get} /api/recommend/:userName User Recommendations
 *@apiVersion 0.0.1
 *@apiGroup Recommendation
 *@apiPermission all
 *@apiDescription Get all Recommendations of a user
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} userName userName of the user to list recommendations of
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 * {
 *    "response": {
 *        "success": true,
 *        "message": "Found 1 Recommendations",
 *        "data": [
 *            {
 *                "_id": "5a0160cf40c9ad08388e3f06",
 *                "updatedAt": "2017-11-07T07:29:19.947Z",
 *                "createdAt": "2017-11-07T07:29:19.947Z",
 *                "userNameRecommended": "lucy",
 *                "userName": "codetanmoy64",
 *                "recommendedOn": "1510039759940",
 *                "__v": 0
 *            }
 *        ]
 *    }
 *}
 **/

module.exports.getRecommendation = function (req, res) {
    Model.Recommends.find({userNameRecommended: req.params.userName}, function (error, data) {
        if (error) {
            return res.json({
                response: {
                    success: false,
                    message: error_text,
                    data: error
                }
            });
        } else {
            return res.json({
                response: {
                    success: true,
                    message: "Found " + data.length + " Recommendations",
                    data: data
                }
            });
        }
    });
};

/**
 *@api {post} /api/recommend/my My Recommendations
 *@apiVersion 0.0.1
 *@apiGroup Recommendation
 *@apiPermission all
 *@apiDescription Get all Recommendations of a user
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} userName userName of the user to list recommendations of
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *    "response": {
 *        "success": true,
 *        "message": "You have recommended 1 users",
 *        "data": [
 *            {
 *                "_id": "5a0160cf40c9ad08388e3f06",
 *                "updatedAt": "2017-11-07T07:29:19.947Z",
 *                "createdAt": "2017-11-07T07:29:19.947Z",
 *                "userNameRecommended": "lucy",
 *                "userName": "codetanmoy64",
 *                "recommendedOn": "1510039759940",
 *                "__v": 0
 *            }
 *        ]
 *    }
 *}
 **/

module.exports.getMyRecommendations = function (req, res) {
    Model.Recommends.find({userName: req.userName}, function (error, data) {
        if (error) {
            return res.json({
                response: {
                    success: false,
                    message: error_text,
                    data: error
                }
            });
        } else {
            return res.json({
                response: {
                    success: true,
                    message: "You have recommended " + data.length + " users",
                    data: data
                }
            });
        }
    });
};
