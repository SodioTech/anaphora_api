'use strict';
var Model = require('./../Controllers/ModelLinks.js');
var Notification = require('./Utility/Notification.js');
var ConversationGroups = require("./Utility/ConversationGroups.js");
var error_text = "Something Went Wrong! Please check your network or try again!";

/**
 *@api {get} /api/connection/followers/ My Followers
 *@apiVersion 0.0.1
 *@apiGroup Follow Feature
 *@apiPermission all
 *@apiDescription People who are Following me
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "My Followers",
 *    "data": [
 *      {
 *        "updatedAt": "2017-04-12T13:51:44.772Z",
 *        "createdAt": "2017-04-12T13:51:18.031Z",
 *        "myUserName": "perma",
 *        "iAmFollowingUserName": "pierce",
 *        "followTimeStamp": "1492005078023",
 *        "followRequestAccepted": "2",
 *        "__v": 0
 *      },
 *      {
 *        "updatedAt": "2017-04-12T14:02:26.378Z",
 *        "createdAt": "2017-04-12T14:02:19.420Z",
 *        "myUserName": "perma",
 *        "iAmFollowingUserName": "perry",
 *        "followTimeStamp": "1492005739413",
 *        "followRequestAccepted": "2",
 *        "__v": 0
 *      }
 *    ]
 *  }
 *}
 **/

// Show My Followers
module.exports.myFollowers = function (req, res) {
    Model.UsersBlocked.find({
        userName: req.userName
    }, {
        blockedUser: 1,
        _id: 0
    }, function (error_3, data_3) {
        if (error_3) {
            res.json({
                response: {
                    success: false,
                    message: error_text,
                    data: error_3
                }
            });
        } else {

            console.log(data_3);

            var data_check = [];
            for (var m = 0; m < data_3.length; m++) {
                data_check.push(data_3[m]["blockedUser"]);
            }

            //     $or: [{
            //         myUserName: req.userName
            //     },
            //     {
            //         iAmFollowingUserName: req.userName
            //     }
            // ]
            Model.UserConnections.find({
                $and: [{
                        iAmFollowingUserName: req.userName // people following me
                    },
                    {
                        myUserName: { // the follower shouldn't be any blocked user
                            $nin: data_check
                        }
                    }
                ]
            }, {
                _id: 0
            }, function (error, data) {
                if (error) {
                    res.json({
                        response: {
                            success: false,
                            message: error_text,
                            data: error
                        }
                    });
                } else {
                    res.json({
                        response: {
                            success: true,
                            message: "My connections",
                            data: data
                        }
                    });
                }
            });
        }
    });
};

/**
 *@api {get} /api/connection/following/ Following
 *@apiVersion 0.0.1
 *@apiGroup Follow Feature
 *@apiPermission all
 *@apiDescription People who I'm Following
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "My Followers",
 *    "data": [
 *      {
 *        "updatedAt": "2017-04-12T13:51:44.772Z",
 *        "createdAt": "2017-04-12T13:51:18.031Z",
 *        "myUserName": "perma",
 *        "iAmFollowingUserName": "pierce",
 *        "followTimeStamp": "1492005078023",
 *        "followRequestAccepted": "2",
 *        "__v": 0
 *      },
 *      {
 *        "updatedAt": "2017-04-12T14:02:26.378Z",
 *        "createdAt": "2017-04-12T14:02:19.420Z",
 *        "myUserName": "perma",
 *        "iAmFollowingUserName": "perry",
 *        "followTimeStamp": "1492005739413",
 *        "followRequestAccepted": "2",
 *        "__v": 0
 *      }
 *    ]
 *  }
 *}
 **/

module.exports.iAmFollowing = function (req, res) {
    Model.UsersBlocked.find({
        userName: req.userName
    }, {
        blockedUser: 1,
        _id: 0
    }, function (error_3, data_3) {
        if (error_3) {
            res.json({
                response: {
                    success: false,
                    message: error_text,
                    data: error_3
                }
            });
        } else {

            console.log(data_3);

            var data_check = [];
            for (var m = 0; m < data_3.length; m++) {
                data_check.push(data_3[m]["blockedUser"]);
            }

            Model.UserConnections.find({
                $and: [{
                        myUserName: req.userName // i am following any user
                    },
                    {
                        followRequestAccepted: "2"
                    },
                    {
                        iAmFollowingUserName: { // the followed person shouldn't be any blocked user
                            $nin: data_check
                        }
                    }
                ]
            }, {
                _id: 0
            }, function (error, data) {
                if (error) {
                    res.json({
                        response: {
                            success: false,
                            message: error_text,
                            data: error
                        }
                    });
                } else {
                    res.json({
                        response: {
                            success: true,
                            message: "My connections",
                            data: data
                        }
                    });
                }
            });
        }
    });
};

/**
 *@api {post} /api/connection/send/ Follow/Unfollow
 *@apiVersion 0.0.1
 *@apiGroup Follow Feature
 *@apiPermission all
 *@apiDescription Send Connection Request to another user
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} userNameInvited user you invited
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "Saved your new connection",
 *    "data": {
 *      "__v": 0,
 *      "updatedAt": "2017-04-12T13:36:32.683Z",
 *      "createdAt": "2017-04-12T13:36:32.683Z",
 *      "myUserName": "perma",
 *      "iAmFollowingUserName": "pierce",
 *      "followTimeStamp": "1492004192682",
 *      "followRequestAccepted": "1",
 *      "_id": "58ee2d6011abad05bafb51c3"
 *    }
 *  }
 *}
 **/

// Location Logs
module.exports.connectionRequest = function (req, res) {
    Model.UserSessionLog.findOne({
        tokenAssigned: req.headers["authorization"]
    }, function (error_user, data_user) {
        if (error_user) {
            res.json({
                response: {
                    success: false,
                    message: error_text,
                    data: error_user
                }
            });
        } else {
            if (data_user == null) {
                res.status(403).json({
                    response: {
                        success: false,
                        message: "Unauthorised Access",
                        data: data_user
                    }
                });
            } else {
                Model.UserDetails.findOne({
                    userName: data_user.userName
                }, function (error, data_userdetails) {
                    if (error) {
                        res.json({
                            response: {
                                success: false,
                                message: error_text,
                                data: error
                            }
                        });
                    } else {
                        if (data_userdetails == null) {
                            res.status(404).json({
                                response: {
                                    success: false,
                                    message: "User details not found",
                                    data: data_userdetails
                                }
                            });
                        } else {
                            //console.log("Request method " + req.method);
                            console.log("User status is " + data_userdetails.status);
                            if (data_userdetails.status === "BLOCKED") {
                                res.json({
                                    response: {
                                        success: false,
                                        message: "We determined that you have violated our terms and conditions found at anaphorachat.com/terms.html. Your account has been suspended. Please contact us if you think this was a mistake at anaphora@usa.com. ",
                                        data: null

                                    }
                                });
                            } else {
                                var epoch_milliseconds = (new Date).getTime();

                                Model.UserConnections.findOne({
                                    $and: [{
                                        iAmFollowingUserName: req.body.userNameInvited
                                    }, {
                                        myUserName: req.userName
                                    }]
                                }).exec(function (error, data) {
                                    if (error) {
                                        res.json({
                                            response: {
                                                success: false,
                                                message: error_text,
                                                data: error
                                            }
                                        });
                                    } else {
                                        if (data == null) {
                                            var connections = new Model.UserConnections({
                                                myUserName: req.userName,
                                                iAmFollowingUserName: req.body.userNameInvited,
                                                followTimeStamp: epoch_milliseconds,
                                                followRequestAccepted: "2"
                                            });

                                            connections.save(function (error, data_added) {
                                                if (error) {
                                                    res.json({
                                                        response: {
                                                            success: false,
                                                            message: error_text,
                                                            data: error
                                                        }
                                                    });
                                                } else {

                                                    Model.UserConnections.find({
                                                        $and: [{
                                                                myUserName: req.body.userNameInvited
                                                            },
                                                            {
                                                                followRequestAccepted: "2"
                                                            },
                                                            {
                                                                iAmFollowingUserName: req.userName
                                                            }
                                                        ]
                                                    }, {
                                                        _id: 0
                                                    }, function (error, data) {
                                                        if (error) {
                                                            console.log(error);
                                                        } else {
                                                            if (data.length > 0) {

                                                                Model.ConversationGroupList.findOne({
                                                                    $and: [{
                                                                            users: {
                                                                                $all: [req.userName, req.body.userNameInvited]
                                                                            }
                                                                        },
                                                                        {
                                                                            type: "p2p"
                                                                        },
                                                                        {
                                                                            "chatroomFlag": false
                                                                        }
                                                                    ]
                                                                }).exec(function (error, data) {
                                                                    if (error) {
                                                                        res.json({
                                                                            response: {
                                                                                success: false,
                                                                                message: error_text,
                                                                                data: error
                                                                            }
                                                                        });
                                                                    } else {
                                                                        console.log(data);
                                                                        var message = `${req.userName} followed ${req.body.userNameInvited} back. Say hi!`;
                                                                        var epoch_milliseconds = new Date().getTime();

                                                                        if (data != null) {
                                                                            console.log("found group");
                                                                            var modelData = new Model.ConversationList({
                                                                                conversationsID: req.userName + "_" + epoch_milliseconds,
                                                                                conversationGroupID: data.conversationGroupID,
                                                                                users: data.users,
                                                                                usersDeletedConv: [],
                                                                                usersConvNotRead: data.users,
                                                                                sender: "SYSTEM",
                                                                                message_text: message,
                                                                                message_type: "text",
                                                                                epochTimeStamp: epoch_milliseconds
                                                                            });

                                                                            modelData.save(function (error, data) {

                                                                            });
                                                                            ///////////////////////////

                                                                        } else {
                                                                            //start a new chat here.
                                                                            console.log("group not found");
                                                                            req.body.type = "p2p";
                                                                            ConversationGroups.createGroup(
                                                                                req, [req.userName, req.body.userNameInvited],
                                                                                function (error, data) {
                                                                                    if (error) {
                                                                                        res.json({
                                                                                            response: {
                                                                                                success: false,
                                                                                                message: error_text,
                                                                                                data: error
                                                                                            }
                                                                                        });
                                                                                    } else {

                                                                                        //New chat from ${req.userName}.`;
                                                                                        ConversationGroups.showOtherData(data, function (err, data) {
                                                                                            if (error) {
                                                                                                res.json({
                                                                                                    response: {
                                                                                                        success: false,
                                                                                                        message: error_text,
                                                                                                        data: error
                                                                                                    }
                                                                                                });
                                                                                            } else {
                                                                                                var modelData = new Model.ConversationList({
                                                                                                    conversationsID: req.userName + "_" + epoch_milliseconds,
                                                                                                    conversationGroupID: data.conversationGroupID,
                                                                                                    users: data.users,
                                                                                                    usersDeletedConv: [],
                                                                                                    usersConvNotRead: data.users,
                                                                                                    sender: req.userName,
                                                                                                    message_text: message,
                                                                                                    message_type: "text",
                                                                                                    epochTimeStamp: epoch_milliseconds
                                                                                                });

                                                                                                modelData.save(function (error, data) {

                                                                                                });
                                                                                                Notification.send(
                                                                                                    req.body.userNameInvited,
                                                                                                    "New Conversation",
                                                                                                    message,
                                                                                                    "",
                                                                                                    req.userName,
                                                                                                    data._id,
                                                                                                    function (error_notif, data_notif) {

                                                                                                    }
                                                                                                );
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                });
                                                                            /////////////////////
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });

                                                    Notification.send(req.body.userNameInvited, "New Follower", `${req.userName} followed you.`, "", req.userName, "", function (error_notif, data_notif) {
                                                        res.json({
                                                            response: {
                                                                success: true,
                                                                message: "Followed " + req.body.userNameInvited + "!",
                                                                data: data_added
                                                            }
                                                        });
                                                    });
                                                }
                                            });
                                        } else {
                                            Model.UserConnections.remove({
                                                _id: data["_id"]
                                            }).exec(function (error, data_delete) {
                                                if (error) {
                                                    res.json({
                                                        response: {
                                                            success: false,
                                                            message: error_text,
                                                            data: error
                                                        }
                                                    });
                                                } else {
                                                    res.json({
                                                        response: {
                                                            success: true,
                                                            message: "Unfollowed " + req.body.userNameInvited + "!",
                                                            data: data
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    }
                });
            }
        }
    });
};

/**
 *@api {post} /api/connection/block/ Block/Unblock a user
 *@apiVersion 0.0.1
 *@apiGroup Follow Feature
 *@apiPermission all
 *@apiDescription Block/Unblock a user
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} blockUserName Username of the person who you want to block
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "You have blocked pierce from your connections!",
 *    "data": {
 *      "__v": 0,
 *      "updatedAt": "2017-04-12T13:20:45.616Z",
 *      "createdAt": "2017-04-12T13:20:45.616Z",
 *      "userName": "perma",
 *      "blockedUser": "pierce",
 *      "followTimeStamp": "1492003245609",
 *      "_id": "58ee29ad7d0a6b0554c2cf49"
 *    }
 *  }
 *}
 **/

// Location Logs
module.exports.blockConnection = function (req, res) {
    var epoch_milliseconds = (new Date).getTime();

    var connections = new Model.UsersBlocked({
        userName: req.userName,
        blockedUser: req.body.blockUserName,
        followTimeStamp: epoch_milliseconds
    });

    Model.UserCredentials.findOne({
        userName: req.body.blockUserName
    }, function (error, data) {
        if (error) {
            res.json({
                response: {
                    success: false,
                    message: error_text,
                    data: error
                }
            });
        } else {
            if (data != null) {
                Model.UsersBlocked.findOne({
                    $and: [{
                            blockedUser: req.body.blockUserName
                        },
                        {
                            userName: req.userName
                        },
                    ]
                }, function (error, data_find) {
                    if (error) {
                        res.json({
                            response: {
                                success: false,
                                message: error_text,
                                data: error
                            }
                        });
                    } else {
                        if (data_find == null) {
                            connections.save(function (error, data) {
                                if (error) {
                                    res.json({
                                        response: {
                                            success: false,
                                            message: error_text,
                                            data: error
                                        }
                                    });
                                } else {
                                    res.json({
                                        response: {
                                            success: true,
                                            message: "You have blocked " + req.body.blockUserName + " from your connections!",
                                            data: data
                                        }
                                    });
                                }
                            });
                        } else {
                            Model.UsersBlocked.remove({
                                _id: data_find._id
                            }, function (error, data) {
                                if (error) {
                                    res.json({
                                        response: {
                                            success: false,
                                            message: error_text,
                                            data: error
                                        }
                                    });
                                } else {
                                    res.json({
                                        response: {
                                            success: true,
                                            message: "You unblocked " + req.body.blockUserName,
                                            data: data
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            } else {
                res.json({
                    response: {
                        success: false,
                        message: "User Not Found!",
                        data: null
                    }
                });
            }
        }
    });
};