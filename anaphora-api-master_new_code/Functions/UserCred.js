'use strict';
const moment = require('moment');
var path = require('path');
var cryptoLib = require('./Utility/Encrypt.js');
var Model = require('./../Controllers/ModelLinks.js');
var Notification = require('./Utility/Notification.js');
var error_text = "Something Went Wrong! Please check your network or try again!";

/**
 *@api {post} /api/auth/signup/ User Registration
 *@apiVersion 0.0.1
 *@apiGroup Authentication
 *@apiPermission all
 *@apiDescription Register a user
 *@apiSampleRequest off
 *
 *@apiParam {String} profilePic Profile picture URL as received from the file upload API
 *@apiParam {String} name Full name
 *@apiParam {String} userName Unique username
 *@apiParam {String} email Email
 *@apiParam {String} password Password set
 *@apiParam {String} gender Gender of user, eg: Male, Female, Don't want to disclose
 *@apiParam {String} phNo Phone Number
 *@apiParam {String} lat_lng latitude,longitude
 *@apiParam {String} location Name of location
 *@apiParam {String} bio User Bio
 *@apiParam {String} hobbies Hobbies of User
 *@apiParam {String} dob Date of birth dd-mm-yyyy
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *@apiErrorExample Response (Duplicate Username):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "You're already registered with the same username or phone number! Please enter a different number to register or Login with your current account.",
 *    "data": null
 *  }
 *}
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "You've successfully logged in",
 *    "data": {
 *      "_id": "58dd24276414ba04993e8d04",
 *      "updatedAt": "2017-03-30T15:28:39.627Z",
 *      "createdAt": "2017-03-30T15:28:39.627Z",
 *      "userName": "mike",
 *      "name": "Mike",
 *      "profilePic": "/profilepic/prof_1490640748402.jpg",
 *      "email": "micheal.perry@yahoo.com",
 *      "dob": "08-05-1967",
 *      "gender": "",
 *      "phNo": "",
 *      "age": "",
 *      "location": "",
 *      "lat_lng": [],
 *      "hobbies": "",
 *      "bio": "",
 *      "timeZone": "",
 *      "private_mode": false,
 *      "__v": 0
 *    },
 *    "apiKey": "/90oGH9h83hUMeIJXachVw=="
 *  }
 *}
 **/

// User registration
module.exports.userRegistration = function (req, res, next) {
    if (req.body.userName != undefined) {

        var latLng = req.body.lat_lng.split(",").map(Number);

        var dob = req.body.dob;
        const age = moment().diff(moment(dob), 'years');

        var userdetails = new Model.UserDetails({
            userName: req.body.userName,
            name: req.body.name,
            profilePic: req.body.profilePic,
            email: req.body.email,
            dob: req.body.dob,
            gender: req.body.gender,
            phNo: req.body.phNo,
            age: age,
            location: req.body.location,
            hobbies: req.body.hobbies,
            bio: req.body.bio,
            private_mode: false,
            timeZone: "",
            role: "user",
            status: "ACTIVE",
            homeTown: req.body.homeTown,
            deviceToken: req.body.deviceToken,
        });

        if (req.body.lat_lng) userdetails['lat_lng'] = req.body.lat_lng.split(",").map(Number);

        var userCreds = new Model.UserCredentials({
            userName: req.body.userName,
            password: cryptoLib.encrypt(req.body.userName, req.body.password)
        });

        userdetails.save(function (error, data) {
            if (error) {
                if (error.code == 11000) {
                    res.json({
                        response: {
                            success: false,
                            message: "You're already registered with the same username or phone number! Please enter a different number to register or Login with your current account.",
                            data: error
                        }
                    });
                } else {
                    res.json({
                        response: {
                            success: false,
                            message: error.message || error_text,
                            data: error
                        }
                    });
                }
            } else {
                userCreds.save(function (error_cred, data_cred) {
                    if (error_cred) {
                        res.json({
                            response: {
                                success: false,
                                message: error_cred.message || error_text,
                                data: error_cred
                            }
                        });
                    } else {
                        next();
                    }
                });
            }
        });
    } else {
        res.json({
            response: {
                success: false,
                message: error_text,
                data: null
            }
        });
    }
};

/**
 *@api {post} /api/auth/signin/ User Login
 *@apiVersion 0.0.1
 *@apiGroup Authentication
 *@apiPermission all
 *@apiDescription Login a user to anaphora app
 *@apiSampleRequest off
 *
 *@apiParam {String} userName Unique username
 *@apiParam {String} password Password set
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "You've successfully logged in",
 *    "data": {
 *      "_id": "58d961ad653ba20a9f33e447",
 *      "updatedAt": "2017-03-27T19:02:05.361Z",
 *      "createdAt": "2017-03-27T19:02:05.361Z",
 *      "userName": "perry",
 *      "name": "Perry",
 *      "profilePic": "/profilepic/prof_1490640748402.jpg",
 *      "email": "micheal.perry@yahoo.com",
 *      "dob": "",
 *      "gender": "",
 *      "phNo": "+919856734684",
 *      "age": "",
 *      "location": "Kolkata",
 *      "lat_lng": [
 *        22.595456,
 *        88.3838633
 *      ],
 *      "hobbies": "",
 *      "bio": "",
 *      "timeZone": "+05:30",
 *      "private_mode": false,
 *      "__v": 0
 *    },
 *    "apiKey": "baJlFZkO5PrJV0xJzQp6Wg=="
 *  }
 *}
 **/

// Login
module.exports.userLogin = function (req, res) {
console.log(req.body);
    if (req.body.userName != undefined) {
//        console.log("hii");
        var epoch_milliseconds = (new Date).getTime();
        var apiKey = cryptoLib.encrypt(epoch_milliseconds + "", req.body.userName + "_" + req.body.password);
  //  console.log(apikey);
        var encryptedPassword = cryptoLib.encrypt(req.body.userName, req.body.password);
        console.log("password of " + req.body.userName + " is " + req.body.password);

        var sessionLog = new Model.UserSessionLog({
            userName: req.body.userName,
            loginTimeStamp: epoch_milliseconds,
            tokenAssigned: apiKey
        });

        var checker = {
            userName: req.body.userName
        };

        Model.UserCredentials.findOne(checker, function (error, data) {
            if (error) {
                res.json({
                    response: {
                        success: false,
                        message: "Sorry! You're not registered with us. Open an account and login again!",
                        data: error
                    }
                });
            } else {
                if (data != null) {
                    if (data.password.localeCompare(encryptedPassword) == 0) {
                        sessionLog.save(function (error_session, data_session) {
                            if (error_session) {
                                res.json({
                                    response: {
                                        success: false,
                                        message: error_text + " Session log error.",
                                        data: error_session
                                    }
                                });
                            } else {
                                Model.UserDetails.findOne(checker, async function (error_user, data_user) {
                                    if (error_user) {
                                        res.json({
                                            response: {
                                                success: false,
                                                message: error_text + " Can't find user data.",
                                                data: error_user
                                            }
                                        });
                                    } else {
                                        if (data_user != null) {
                                            if (req.body.deviceToken) {
                                                const { deviceToken } = req.body;
                                                await Model.UserDetails.findOneAndUpdate(checker, { deviceToken });
                                            }
                                            if (data_user.status === "BLOCKED") {
                                                res.json({
                                                    response: {
                                                        success: false,
                                                        message: "We determined that you have violated our terms and conditions found at anaphorachat.com/terms.html. Your account has been suspended. Please contact us if you think this was a mistake at anaphora@usa.com. ",
                                                        data: null

                                                    }
                                                });
                                            } else if (!data_user.status || data_user.status === "" || data_user.status === null) {
                                                //Update the user status to ACTIVE
                                                Model.UserDetails.update({
                                                    userName: req.body.userName
                                                }, {
                                                    $set: {
                                                        status: "ACTIVE"
                                                    }
                                                }, {
                                                    w: 1
                                                }, function (error_statususer, data_statususer) {

                                                    if (error_statususer) {
                                                        res.json({
                                                            response: {
                                                                success: false,
                                                                message: error_text,
                                                                data: error_statususer
                                                            }
                                                        });
                                                    } else {

                                                        Model.UserDetails.findOne(checker, async function (error, data) {
                                                            if (error) {
                                                                res.json({
                                                                    response: {
                                                                        success: false,
                                                                        message: error_text + " Can't find user data.",
                                                                        data: error
                                                                    }
                                                                });
                                                            } else {
                                                                res.json({
                                                                    response: {
                                                                        success: true,
                                                                        message: "You've successfully logged in",
                                                                        data: data,
                                                                        apiKey: apiKey
                                                                    }
                                                                });
                                                            }
                                                        })
                                                    }
                                                });
                                            } else if (data_user.status === "ACTIVE") {
                                                res.json({
                                                    response: {
                                                        success: true,
                                                        message: "You've successfully logged in",
                                                        data: data_user,
                                                        apiKey: apiKey
                                                    }
                                                });
                                            }
                                        } else {
                                            res.json({
                                                response: {
                                                    success: false,
                                                    message: "Can't find user data.",
                                                    data: data_user
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    } else {
                        res.json({
                            response: {
                                success: false,
                                message: "You've entered the wrong password!",
                                data: null
                            }
                        });
                    }
                } else {
                    res.json({
                        response: {
                            success: false,
                            message: "You've entered the wrong password!",
                            data: null
                        }
                    });
                }
            }
        });
    } else {
        res.json({
            response: {
                success: false,
                message: error_text,
                data: null
            }
        });
    }
};

/**
 *@api {put} /api/auth/reset/ Profile Password Reset
 *@apiVersion 0.0.1
 *@apiGroup Forgot Password
 *@apiPermission all
 *@apiDescription Reset Password Profile from the anaphora app
 *@apiSampleRequest off
 *
 *@apiParam {String} userName Username
 *@apiParam {String} password Password
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiErrorExample Response (Password doesn't match):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Password's don't match",
 *    "data": null
 *  }
 *}
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "We've updated your details successfully!",
 *    "data": { "ok" : 1, "nModified" : 1, "n" : 1 }
 *  }
 *}
 **/

// Update user details
module.exports.resetPassword = function (req, res) {
    var query = {
        userName: req.body.userName
    };

    var update = {
        password: cryptoLib.encrypt(req.body.userName, req.body.password)
    };

    Model.UserCredentials.findOneAndUpdate(query, update, function (error, data) {
        if (error) {
            res.json({
                response: {
                    success: false,
                    message: error_text,
                    data: error
                }
            });
        } else {
            res.json({
                response: {
                    success: true,
                    message: "We've updated your details successfully!",
                    data: data
                }
            });
        }
    });
};

/**
 *@api {get} /api/auth/email/:email/ Find user
 *@apiVersion 0.0.1
 *@apiGroup Forgot Password
 *@apiPermission all
 *@apiDescription Find user's username from email
 *@apiSampleRequest off
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiErrorExample Response (User Not Found):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "User Not Found",
 *    "data": null
 *  }
 *}
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": 'User Found!',
 *    "data": perry
 *  }
 *}
 **/

// Update user details
module.exports.getUsername = function (req, res) {
    Model.UserDetails.findOne({
        email: req.params.email
    }, function (error_user, data_user) {
        if (error_user) {
            res.json({
                response: {
                    success: false,
                    message: error_text,
                    data: error_user
                }
            });
        } else {
            if (data_user == null) {
                res.json({
                    response: {
                        success: false,
                        message: "User Not Found",
                        data: data_user
                    }
                });
            } else {
                res.json({
                    response: {
                        success: true,
                        message: 'User Found!',
                        data: data_user.userName
                    }
                });
            }
        }
    });
};

/**
 *@api {put} /api/profile/update/ Profile Update
 *@apiVersion 0.0.1
 *@apiGroup Profile
 *@apiPermission user
 *@apiDescription Update Profile from the anaphora app
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} name Full Name
 *@apiParam {String} profilePic Profile picture URL as received from the file upload API
 *@apiParam {String} email Email
 *@apiParam {String} dob Date of birth dd-mm-yyyy
 *@apiParam {String} gender Gender
 *@apiParam {String} phNo Phone Number
 *@apiParam {String} location City and Street
 *@apiParam {String} lat_lng GPS location of the user. For eg: 22.595456,88.3838633
 *@apiParam {String} hobbies Hobbies
 *@apiParam {String} bio Short Biography of user
 *@apiParam {String} private_mode true/false
 *@apiParam {String} timeZone Time Zone of the user. For eg: +05:30
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "We've updated your details successfully!",
 *    "data": { "ok" : 1, "nModified" : 1, "n" : 1 }
 *  }
 *}
 **/

// Update user details
module.exports.userProfileUpdate = function (req, res, next) {
    var query = {
        userName: req.userName
    }

    var dob = req.body.dob;
    const age = moment().diff(moment(dob), 'years');

    var latLng = req.body.lat_lng.split(",").map(Number);

    var update = {
        name: req.body.name,
        profilePic: req.body.profilePic,
        email: req.body.email,
        dob: req.body.dob,
        gender: req.body.gender,
        phNo: req.body.phNo,
        age,
        location: req.body.location,
        lat_lng: latLng,
        hobbies: req.body.hobbies,
        bio: req.body.bio,
        private_mode: req.body.private_mode == "true" ? true : false,
        timeZone: req.body.timeZone,
        homeTown: req.body.homeTown,
    };

    Model.UserDetails.findOneAndUpdate(query, update, function (error, data) {
        if (error) {
            res.json({
                response: {
                    success: false,
                    message: error_text,
                    data: error
                }
            });
        } else {
            if (data == null) {
                res.json({
                    response: {
                        success: false,
                        message: "User Not Found",
                        data: data
                    }
                });
            } else {
                next();
            }
            // res.json({
            //     response:{
            //         success: true,
            //         message: "We've updated your details successfully!",
            //         data: data
            //     }
            // });
        }
    });
};

/**
 *@api {get} /api/profile/details/:userName Profile Details
 *@apiVersion 0.0.1
 *@apiGroup Profile
 *@apiPermission user
 *@apiDescription Fetch user profile details in anaphora App
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} userName userName 
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": error explanation
 *  }
 *}
 *@apiErrorExample Response (User Not Found):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "User Not Found",
 *    "data": null
 *  }
 *}
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "User Personal Details fetched successfully!",
 *    "data": {
 *      "_id": "58d961ad653ba20a9f33e447",
 *      "updatedAt": "2017-03-27T19:02:05.361Z",
 *      "createdAt": "2017-03-27T19:02:05.361Z",
 *      "userName": "perry",
 *      "name": "Perry",
 *      "profilePic": "/profilepic/prof_1490640748402.jpg",
 *      "email": "micheal.perry@yahoo.com",
 *      "dob": "",
 *      "gender": "",
 *      "phNo": "+919856734684",
 *      "age": "",
 *      "location": "Kolkata",
 *      "lat_lng": [
 *        22.595456,
 *        88.3838633
 *      ],
 *      "hobbies": "",
 *      "bio": "",
 *      "timeZone": "+05:30",
 *      "private_mode": false,
 *      "__v": 0
 *    }
 *  }
 *}
 **/

// Get user personal details
module.exports.userGetProfileDetails = function (req, res) {
    var query = {
        userName: req.userName
    };
    if(req.params.userName != undefined){
        query = {
            userName: req.params.userName
        };
    }
    console.log(query);
    Model.UserDetails.findOne( query, function (error_user, data_user) {
        if (error_user) {
            res.json({
                response: {
                    success: false,
                    message: error_text,
                    data: error_user
                }
            });
        } else {
            if (data_user == null) {
                res.json({
                    response: {
                        success: false,
                        message: "User Not Found",
                        data: data_user
                    }
                });
            } else {
                res.json({
                    response: {
                        success: true,
                        message: 'User details saved!',
                        data: data_user
                    }
                });
            }
        }
    });
};

/**
 *@api {get} /api/auth/photo/:userName Profile Photo
 *@apiVersion 0.0.1
 *@apiGroup Profile
 *@apiPermission user
 *@apiDescription Fetch user profile details in Blubyrd App
 *@apiSampleRequest off
 *
 *@apiExample Example usage:
 *curl -i http://52.11.247.42:3001/api/auth/photo/perry
 *
 *@apiParam {String} userName userName
 **/

// Get user personal details
module.exports.userGetProfileImage = function (req, res) {

    if (req.params.userName != undefined) {
        Model.UserDetails.findOne({
            userName: req.params.userName
        }, function (error_user, data_user) {
            if (error_user) {
                res.json({
                    response: {
                        success: false,
                        message: error_text,
                        data: error_user
                    }
                });
            } else {
                if (data_user == null) {
                    res.sendFile("/profilepic/placeholder-user-photo.png", {
                        root: path.join(__dirname, '../uploads')
                    });
                } else {
                    res.sendFile(data_user.profilePic, {
                        root: path.join(__dirname, '../uploads')
                    });
                }
            }
        });
    } else {
        res.sendFile("/profilepic/placeholder-user-photo.png", {
            root: path.join(__dirname, '../uploads')
        });
    }
};

//=====================FOR ADMIN =====================================


/**
 *@api {post} /api/profile/usersDetails Get All users Details by Admin
 *@apiVersion 0.0.1
 *@apiGroup Profile
 *@apiPermission user
 *@apiDescription Fetch all users profile details in anaphora App
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *@apiParam {String} name admin Name 
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": error explanation
 *  }
 *}
 *@apiErrorExample Response (User Not Found):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Users Not Found",
 *    "data": null
 *  }
 *}
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "User Personal Details fetched successfully!",
 *    "data": [{
 *      "_id": "58d961ad653ba20a9f33e447",
 *      "updatedAt": "2017-03-27T19:02:05.361Z",
 *      "createdAt": "2017-03-27T19:02:05.361Z",
 *      "userName": "perry",
 *      "name": "Perry",
 *      "profilePic": "/profilepic/prof_1490640748402.jpg",
 *      "email": "micheal.perry@yahoo.com",
 *      "dob": "",
 *      "gender": "",
 *      "phNo": "+919856734684",
 *      "age": "",
 *      "role": "user",
 *      "location": "Kolkata",
 *      "lat_lng": [
 *        22.595456,
 *        88.3838633
 *      ],
 *      "hobbies": "",
 *      "bio": "",
 *      "timeZone": "+05:30",
 *      "private_mode": false,
 *      "__v": 0
 *    }]
 *  }
 *}
 **/

// Get all users details
module.exports.getAllUsersDetails = function (req, res) {
    var adminquery = {
        name: req.body.name
    };

    console.log(adminquery);

    Model.Admin.findOne(adminquery, function (error_admin, data_admin) {

        if (error_admin) {
            res.json({
                response: {
                    success: false,
                    message: error_text,
                    data: error_admin
                }
            });
        } else {
            if (data_admin == null) {
                res.json({
                    response: {
                        success: false,
                        message: "Admin details Not Found",
                        data: data_admin
                    }
                });
            } else {
                if (data_admin.role === "ADMIN") {
                    var userquery = {};

                    console.log(userquery);
                    Model.UserDetails.find(userquery, function (error_user, data_user) {
                        if (error_user) {
                            res.json({
                                response: {
                                    success: false,
                                    message: error_text,
                                    data: error_user
                                }
                            });
                        } else {
                            if (data_user.length) {
                                res.json({
                                    response: {
                                        success: true,
                                        message: 'Users details found!',
                                        data: data_user
                                    }
                                });
                            } else {
                                res.json({
                                    response: {
                                        success: false,
                                        message: "Users Not Found",
                                        data: data_user
                                    }
                                });
                            }
                        }
                    });
                } else {
                    res.json({
                        response: {
                            success: false,
                            message: "You are not admin"
                        }
                    });
                }

            }
        }
    });
};


/**
 *@api {post} /api/auth/adminLogin/ Admin Login
 *@apiVersion 0.0.1
 *@apiGroup Authentication
 *@apiPermission all
 *@apiDescription Login as a Admin to anaphora app
 *@apiSampleRequest off
 *
 *@apiParam {String} name Unique Admin Name
 *@apiParam {String} password Password set
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "You've successfully logged in",
 *    "data": {
 *      "_id": "58d961ad653ba20a9f33e447",
 *      "updatedAt": "2017-03-27T19:02:05.361Z",
 *      "createdAt": "2017-03-27T19:02:05.361Z",
 *      "name": "admin",
 *      "email": "admin.anaphora@yahoo.com",
 *      "phone": "+919856734684",
 *      "role": "ADMIN",
 *      "__v": 0
 *    },
 *    "apiKey": "baJlFZkO5PrJV0xJzQp6Wg=="
 *  }
 *}
 **/

// Admin Login
module.exports.adminLogin = function (req, res) {
    console.log(req.body);
    if (req.body.name != undefined) {
        //        console.log("hii");
        var epoch_milliseconds = (new Date).getTime();
        var apiKey = cryptoLib.encrypt(epoch_milliseconds + "", req.body.name + "_" + req.body.password);
        //  console.log(apikey);
        var encryptedPassword = cryptoLib.encrypt(req.body.name, req.body.password);
        console.log("Admin password of " + req.body.name + " is " + req.body.password);
        console.log(encryptedPassword)
        var sessionLog = new Model.UserSessionLog({
            userName: req.body.name,
            loginTimeStamp: epoch_milliseconds,
            tokenAssigned: apiKey
        });

        var redentialsChecker = {
            userName: req.body.name
        };

        Model.UserCredentials.findOne(redentialsChecker, function (error, data) {
            if (error) {
                res.json({
                    response: {
                        success: false,
                        message: "Sorry! You're not Admin.",
                        data: error
                    }
                });
            } else {
                if (data != null) {
                    console.log(data)
                    console.log(data.password.localeCompare(encryptedPassword))
                    if (data.password.localeCompare(encryptedPassword) == 0) {
                        sessionLog.save(function (error_session, data_session) {
                            if (error_session) {
                                res.json({
                                    response: {
                                        success: false,
                                        message: error_text + " Session log error.",
                                        data: error_session
                                    }
                                });
                            } else {
                                var checker = {
                                    name: req.body.name
                                };
                                Model.Admin.findOne(checker, async function (error_admin, data_admin) {
                                    if (error_admin) {
                                        res.json({
                                            response: {
                                                success: false,
                                                message: error_text + " Can't find admin data.",
                                                data: error_admin
                                            }
                                        });
                                    } else {
                                        if (data_admin != null) {
                                            if (req.body.deviceToken) {
                                                const {
                                                    deviceToken
                                                } = req.body;
                                                await Model.Admin.findOneAndUpdate(checker, {
                                                    deviceToken
                                                });
                                            }
                                            res.json({
                                                response: {
                                                    success: true,
                                                    message: "You've successfully logged in",
                                                    data: data_admin,
                                                    apiKey: apiKey
                                                }
                                            });
                                        } else {
                                            res.json({
                                                response: {
                                                    success: false,
                                                    message: "Can't find admin data.",
                                                    data: data_admin
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    } else {
                        res.json({
                            response: {
                                success: false,
                                message: "You've entered the wrong password!",
                                data: null
                            }
                        });
                    }
                } else {
                    res.json({
                        response: {
                            success: false,
                            message: "You've entered the wrong password!",
                            data: null
                        }
                    });
                }
            }
        });
    } else {
        res.json({
            response: {
                success: false,
                message: error_text,
                data: null
            }
        });
    }
};

/**
 *@api {put} /api/profile/BlockOrUnblockUser/ Block Or Unblock User by Admin
 *@apiVersion 0.0.1
 *@apiGroup Profile
 *@apiPermission user
 *@apiDescription Update status (Block/Unblock) user from the anaphora app
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} userName user name
 *@apiParam {String} status user status (BLOCKED/ACTIVE) By default all users status will be ACTIVE
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *@apiSuccessExample Response (if Blocked):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "The user is blocked"
 *  }
 *}

 @apiSuccessExample Response (if Un-Blocked):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "The user is un-blocked"
 *  }
 *}
 **/
// Update user details profile block or unblock
module.exports.userBlockOrUnblock = function (req, res, next) {
    var userFindQuery = {
        userName: req.body.userName
    }
    var reqStatus = req.body.status;

    reqStatus = reqStatus.toUpperCase();
    console.log(reqStatus)

    Model.UserDetails.findOne(userFindQuery, function (error_user, data_user) {

        if (error_user) {
            res.json({
                response: {
                    success: false,
                    message: error_text,
                    data: error_user
                }
            });
        } else {
            if (data_user != null) {

                Model.UserDetails.update({
                    userName: req.body.userName
                }, {
                    $set: {
                        status: reqStatus
                    }
                }, {
                    w: 1
                }, function (error_user, data_user) {

                    if (error_user) {
                        res.json({
                            response: {
                                success: false,
                                message: error_text,
                                data: error_user
                            }
                        });
                    } else {

                        if (reqStatus === "BLOCKED") {
                            var message = "The user is blocked"

                        } else if (reqStatus === "ACTIVE") {
                            var message = "The user is un-blocked"
                        }

                        if (data_user != null) {
                            res.json({
                                response: {
                                    success: true,
                                    message: message
                                }
                            });
                        } else {

                            res.json({
                                response: {
                                    success: false,
                                    message: "User status not updated"

                                }
                            });
                        }
                    }
                });
            } else {
                res.json({
                    response: {
                        success: false,
                        message: "User Not Found",
                        data: data_user
                    }
                });
            }
        }
    });

}
