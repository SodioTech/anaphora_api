'use strict';
var Model = require('./../Controllers/ModelLinks.js');
var error_text = "Something Went Wrong! Please check your network or try again!";
var geocoder = require('geocoder');

/**
 *@api {post} /api/history/location/log/ Location Log
 *@apiVersion 0.0.1
 *@apiGroup Location History
 *@apiPermission all
 *@apiDescription log location of the user everytime this API get\'s called
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} lat latitude of 2D index of location
 *@apiParam {String} lng longitude of 2D index of location
 *@apiParam {String} locationName location
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *    "response": {
 *        "success": true,
 *        "message": "Location log updated successfully",
 *        "data": {
 *            "_id": "596f819fd71c5515914b6740",
 *            "updatedAt": "2017-07-19T15:58:30.316Z",
 *            "createdAt": "2017-07-19T15:58:23.092Z",
 *            "userName": "codetanmoy32",
 *            "userLocation": [
 *                22.4994,
 *                88.3713
 *            ],
 *            "locationName": "188, Raja Subodh Chandra Mallick Rd, Poddar Nagar, Jadavpur, Kolkata, West Bengal 700032, India",
 *            "logTimeStamp": "1500479909925",
 *            "__v": 0
 *        }
 *    }
 *}
 **/

// Location Logs
module.exports.logUserLocationHistory = function (req, res) {

    var epoch_milliseconds = (new Date).getTime();

    var lat = parseFloat(req.body.lat).toFixed(6);
    var lng = parseFloat(req.body.lng).toFixed(6);

    // geocoder.reverseGeocode(lat, lng, function (err, g_data) {
    //     if (err) {
    //         console.log(err);
    //         res.json({
    //             response: {
    //                 success: false,
    //                 message: "Invalid Location",
    //                 data: error
    //             }
    //         });
    //     } else {
    //         console.log(g_data);
    //         var geo_data = g_data;


    //     }
    // });

    var logDetails = new Model.LocationHistory({
        locationHistoryLoggedID: "location_" + epoch_milliseconds,
        userName: req.userName,
        userLocation: [lat, lng],
        locationName: req.body.locationName,
        logTimeStamp: epoch_milliseconds
    });

    logDetails.save(function (error, data) {
        if (error) {
            res.json({
                response: {
                    success: false,
                    message: error_text,
                    data: error
                }
            });
        } else {
            var logUpdate = new Model.LocationCurrent({
                userName: req.userName,
                userLocation: [lat, lng],
                locationName: req.body.locationName,
                logTimeStamp: epoch_milliseconds
            });

            logUpdate.save(function (error, data) {
                if (error) {
                    if (error.code == 11000) {
                        var query = {
                            userName: req.userName
                        };

                        //locationName: geo_data.results[0].formatted_address

                        var update = {
                            userLocation: [lat, lng],
                            locationName: req.body.locationName,
                            logTimeStamp: epoch_milliseconds
                        };

                        Model.LocationCurrent.findOneAndUpdate(query, update, function (error, data) {
                            if (error) {
                                res.json({
                                    response: {
                                        success: false,
                                        message: error_text,
                                        data: error
                                    }
                                });
                            } else {
                                var query = {
                                    userName: req.userName
                                };

                                var update = {
                                    location: req.body.locationName,
                                    lat_lng: [lat, lng]
                                };

                                Model.UserDetails.findOneAndUpdate(query, update, function (error, data) {
                                    if (error) {
                                        res.json({
                                            response: {
                                                success: false,
                                                message: error_text,
                                                data: error
                                            }
                                        });
                                    } else {
                                        res.json({
                                            response: {
                                                success: true,
                                                message: "Location log updated successfully",
                                                data: data
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                        res.json({
                            response: {
                                success: false,
                                message: error_text,
                                data: error
                            }
                        });
                    }
                } else {
                    res.json({
                        response: {
                            success: true,
                            message: "Location logged successfully",
                            data: data
                        }
                    });
                }
            });

        }
    });
};
