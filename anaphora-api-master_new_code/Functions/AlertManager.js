'use strict';
var Model = require('./../Controllers/ModelLinks.js');
var error_text = "Something Went Wrong! Please check your network or try again!";

/**
 *@api {post} /api/notifications Notification List
 *@apiVersion 0.0.1
 *@apiGroup Notifications
 *@apiPermission all
 *@apiDescription Show User's Notification List
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *    "response": {
 *        "success": true,
 *        "message": "All Alerts!",
 *        "data": [
 *            {
 *                "_id": "59f1eb55f829c568e054424a",
 *                "updatedAt": "2017-10-26T14:04:05.478Z",
 *                "createdAt": "2017-10-26T14:04:05.478Z",
 *                "alertID": "alert_1509026645471",
 *                "userNameReceiver": "lucy",
 *                "title": "Resumed Conversation",
 *                "message": "codetanmoy64 has texted you again after a long time!",
 *                "imageUrl": "",
 *                "related_user": "codetanmoy64",
 *                "related_data": "59edf1d4aea2dc30458db2c2",
 *                "epochTimeStampSent": "1509026645471",
 *                "epochTimeStampRead": "",
 *                "readStatus": "false",
 *                "__v": 0
 *            },
 *            {
 *                "_id" : ObjectId("59f1e25af28b9068446212bd"),
 *                "updatedAt" : ISODate("2017-10-26T13:25:46.223Z"),
 *                "createdAt" : ISODate("2017-10-26T13:25:46.223Z"),
 *                "alertID" : "alert_1509024346222",
 *                "userNameReceiver" : "lucy",
 *                "title" : "Post",
 *                "message" : "codetanmoy64 liked your Post!",
 *                "imageUrl" : "",
 *                "related_user" : "codetanmoy64",
 *                "related_data" : "POST1507703866545",
 *                "epochTimeStampSent" : "1509024346222",
 *                "epochTimeStampRead" : "",
 *                "readStatus" : "false",
 *                "__v" : 0
 *            }
 *        ]
 *    }
 *}
 **/

// Create a Post
module.exports.getAllAlerts = function (req, res) {
    Model.Alerts.find({
        userNameReceiver: req.userName
    }, function (error, data) {
        if (error) {
            res.json({
                response: {
                    success: false,
                    message: error_text,
                    data: error
                }
            });
        } else {
            res.json({
                response: {
                    success: true,
                    message: "All Alerts!",
                    data: data
                }
            });
        }
    });

    // Model.Alerts.aggregate([{
    //     $group: {
    //         _id: "$title",
    //         alerts: {
    //             $push: '$$ROOT'
    //         }
    //     }
    // }]).exec(function (error, data) {
    //     if (error) {
    //         res.json({
    //             response: {
    //                 success: false,
    //                 message: error_text,
    //                 data: error
    //             }
    //         });
    //     } else {
    //         res.json({
    //             response: {
    //                 success: true,
    //                 message: "All Alerts!",
    //                 data: data
    //             }
    //         });
    //     }
    // });
};