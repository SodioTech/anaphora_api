"use strict";
var Model = require("./../Controllers/ModelLinks.js");
var Notification = require("./Utility/Notification.js");
var error_text =
	"Something Went Wrong! Please check your network or try again!";

/**
 *@api {get} /api/feed/my/:userName/:fromtime My Feed
 *@apiVersion 0.0.1
 *@apiGroup Feed
 *@apiPermission all
 *@apiDescription fetch my own post feed
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} fromtime from timestamp in epoch miliseconds
 *@apiParam {String} userName username of the current user
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 * {
 *    "response": {
 *        "success": true,
 *        "message": "My Posts fetched successfully!",
 *        "data": [
 *            {
 *                "_id": "59650be8d474d46bee0d465d",
 *                "updatedAt": "2017-07-11T17:33:28.442Z",
 *                "createdAt": "2017-07-11T17:33:28.442Z",
 *                "postID": "POST1499794408338",
 *                "userName": "codetanmoy64",
 *                "message": "test #test",
 *                "imageUrl": "/profilepic/prof_1499794405055.jpg",
 *                "logTimeStamp": "1499794408338",
 *                "userLocation": "London,United Kingdom",
 *                "reported": "0",
 *                "user": {
 *                    "_id": "59418b075ebac10a8ab34119",
 *                    "updatedAt": "2017-10-24T13:18:10.082Z",
 *                    "createdAt": "2017-06-14T19:14:15.510Z",
 *                    "userName": "codetanmoy64",
 *                    "name": "Tanmoy Khanra",
 *                    "profilePic": "/profilepic/prof_1508841209969.jpg",
 *                    "email": "tanmoykhanra@hotmail.com",
 *                    "dob": "15-06-1990",
 *                    "gender": "Male",
 *                    "phNo": "9234567876",
 *                    "age": "27",
 *                    "location": "",
 *                    "lat_lng": [
 *                        22.496267,
 *                        88.361191
 *                    ],
 *                    "hobbies": "Photography",
 *                    "bio": "Who am I ......?",
 *                    "timeZone": "GMT 5:30",
 *                    "__v": 0,
 *                    "private_mode": false
 *                },
 *                "likes": [],
 *                    "comments": [
 *                    {
 *                        "_id": "59661f6561ff724a1ae213ed",
 *                        "updatedAt": "2017-07-12T13:08:53.711Z",
 *                        "createdAt": "2017-07-12T13:08:53.711Z",
 *                        "postID": "POST1499794408338",
 *                        "userName": "lucy",
 *                        "message": "test comment ",
 *                        "logTimeStamp": "1499864933649",
 *                        "__v": 0
 *                    },
 *                    {
 *                        "_id": "5966203761ff724a1ae213ee",
 *                        "updatedAt": "2017-07-12T13:12:23.788Z",
 *                        "createdAt": "2017-07-12T13:12:23.788Z",
 *                        "postID": "POST1499794408338",
 *                        "userName": "lucy",
 *                        "message": "test post 123",
 *                        "logTimeStamp": "1499865143788",
 *                        "__v": 0
 *                    },
 *                    {
 *                        "_id": "59662cf30b29a104652c176a",
 *                        "updatedAt": "2017-07-12T14:06:43.484Z",
 *                        "createdAt": "2017-07-12T14:06:43.484Z",
 *                        "postID": "POST1499794408338",
 *                        "userName": "lucy",
 *                        "message": "ho ho",
 *                        "logTimeStamp": "1499868403417",
 *                        "__v": 0
 *                    },
 *                    {
 *                        "_id": "596659500b29a104652c176c",
 *                        "updatedAt": "2017-07-12T17:16:00.260Z",
 *                        "createdAt": "2017-07-12T17:16:00.260Z",
 *                        "postID": "POST1499794408338",
 *                        "userName": "lucy",
 *                        "message": "nice",
 *                        "logTimeStamp": "1499879760259",
 *                        "__v": 0
 *                    }
 *                ]
 *            }
 *        ]
 *    }
 *}
 **/

// Get my posts
module.exports.myFeed = function (req, res) {
	Model.Posts.aggregate([{
			$match: {
				userName: req.params.userName,
				logTimeStamp: {
					$lt: req.params.fromtime
				}
			}
		}, {
			$lookup: {
				from: "userdetails",
				localField: "userName",
				foreignField: "userName",
				as: "user"
			}
		},
		{
			$sort: {
				_id: -1
			}
		},
		{
			$project: {
				_id: 1,
				updatedAt: 1,
				createdAt: 1,
				postID: 1,
				userName: 1,
				message: 1,
				imageUrl: 1,
				userLocation: 1,
				reported: 1,
				logTimeStamp: 1,
				user: {
					$arrayElemAt: ["$user", 0]
				}
			}
		},
		{
			$lookup: {
				from: "likes",
				localField: "postID",
				foreignField: "postID",
				as: "likes"
			}
		},
		{
			$lookup: {
				from: "comments",
				localField: "postID",
				foreignField: "postID",
				as: "comments"
			}
		},
		{
			$limit: 10
		}
	]).exec(function (error_post, data_post) {
		if (error_post) {
			res.json({
				response: {
					success: false,
					message: error_text,
					data: error_post
				}
			});
		} else {
			res.json({
				response: {
					success: true,
					message: "My Posts fetched successfully!",
					data: data_post
				}
			});
		}
	});
};

/**
 *@api {get} /api/feed/connections/:fromtime Connections Feed
 *@apiVersion 0.0.1
 *@apiGroup Feed
 *@apiPermission all
 *@apiDescription fetch my connections\' post feed
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} fromtime from timestamp in epoch miliseconds
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *    "response": {
 *        "success": true,
 *        "message": "Posts of my connections fetched successfully!",
 *        "data": [
 *            {
 *                "_id": "596666e40b29a104652c176e",
 *                "updatedAt": "2017-07-12T18:13:56.009Z",
 *                "createdAt": "2017-07-12T18:13:56.009Z",
 *                "postID": "POST1499883235947",
 *                "userName": "faheelkamran",
 *                "message": "Europe!",
 *                "imageUrl": "/profilepic/prof_1499883234806.jpg",
 *                "logTimeStamp": "1499883235947",
 *                "userLocation": "Gwynn Oak,United States",
 *                "reported": "0",
 *                "user": {
 *                    "_id": "5941a37e5ebac10a8ab34131",
 *                    "updatedAt": "2017-10-14T02:26:05.100Z",
 *                    "createdAt": "2017-06-14T20:58:38.415Z",
 *                    "userName": "faheelkamran",
 *                    "name": "Faheel",
 *                    "profilePic": "/profilepic/prof_1507615216716.jpg",
 *                    "email": "faheelkamran@icloud.com",
 *                    "dob": "07-12-1999",
 *                    "gender": "Male",
 *                    "phNo": "2406727699",
 *                    "age": "18",
 *                    "location": "2603 Royal Oak Ave, Gwynn Oak, MD 21207, USA",
 *                    "lat_lng": [
 *                        39.324617,
 *                        -76.7106
 *                    ],
 *                    "hobbies": "Fun",
 *                    "bio": "life is bigger than us ",
 *                    "timeZone": null,
 *                    "__v": 0,
 *                    "private_mode": false
 *                },
 *                "likes": [
 *                    {
 *                        "_id": "59c1bf2514fc9945edf65c51",
 *                        "updatedAt": "2017-09-20T01:06:45.984Z",
 *                        "createdAt": "2017-09-20T01:06:45.984Z",
 *                        "postID": "POST1499883235947",
 *                        "userName": "codetanmoy64",
 *                        "likedOn": "1505869605982",
 *                        "__v": 0
 *                    }
 *                ],
 *                "comments": [
 *                    {
 *                        "_id": "596691670b29a104652c1771",
 *                        "updatedAt": "2017-07-12T21:15:19.758Z",
 *                        "createdAt": "2017-07-12T21:15:19.758Z",
 *                        "postID": "POST1499883235947",
 *                        "userName": "codetanmoy64",
 *                        "message": "wow !!!!",
 *                        "logTimeStamp": "1499894119757",
 *                        "__v": 0
 *                    }
 *                ]
 *            },
 *            {
 *                "_id": "5966399f0b29a104652c176b",
 *                "updatedAt": "2017-07-12T15:00:47.739Z",
 *                "createdAt": "2017-07-12T15:00:47.739Z",
 *                "postID": "POST1499871647738",
 *                "userName": "lucy",
 *                "message": "banana",
 *                "imageUrl": "/profilepic/prof_1499871644889.jpg",
 *                "logTimeStamp": "1499871647738",
 *                "userLocation": "Kolkata,India",
 *                "reported": "0",
 *                "user": {
 *                    "_id": "5941a0335ebac10a8ab3412d",
 *                    "updatedAt": "2017-06-14T20:45:05.805Z",
 *                    "createdAt": "2017-06-14T20:44:35.958Z",
 *                    "userName": "lucy",
 *                    "name": "popo lucy",
 *                    "profilePic": "/profilepic/prof_1497473102817.jpg",
 *                    "email": "tanmoykhanra@lolo.com",
 *                    "dob": "15-06-2017",
 *                    "gender": "Male",
 *                    "phNo": "5678906543",
 *                    "age": "0",
 *                    "location": "Kolkata,India",
 *                    "lat_lng": [
 *                        22.496328225149,
 *                        88.3611998194166
 *                    ],
 *                    "hobbies": "bio",
 *                    "bio": "test",
 *                    "timeZone": null,
 *                    "__v": 0,
 *                    "private_mode": false
 *                },
 *                "likes": [
 *                    {
 *                        "_id": "59c0fa6220196747d9c9b30b",
 *                        "updatedAt": "2017-09-19T11:07:14.513Z",
 *                        "createdAt": "2017-09-19T11:07:14.513Z",
 *                        "postID": "POST1499871647738",
 *                        "userName": "codetanmoy64",
 *                        "likedOn": "1505819234511",
 *                        "__v": 0
 *                    }
 *                ],
 *                "comments": []
 *            },
 *            {
 *                "_id": "59660c2661ff724a1ae213ec",
 *                "updatedAt": "2017-07-12T11:46:46.866Z",
 *                "createdAt": "2017-07-12T11:46:46.866Z",
 *                "postID": "POST1499860006800",
 *                "userName": "lucy",
 *                "message": "high to jump jump to high",
 *                "imageUrl": "/profilepic/prof_1499860001539.jpg",
 *                "logTimeStamp": "1499860006800",
 *                "userLocation": "Kolkata,India",
 *                "reported": "0",
 *                "user": {
 *                    "_id": "5941a0335ebac10a8ab3412d",
 *                    "updatedAt": "2017-06-14T20:45:05.805Z",
 *                    "createdAt": "2017-06-14T20:44:35.958Z",
 *                    "userName": "lucy",
 *                    "name": "popo lucy",
 *                    "profilePic": "/profilepic/prof_1497473102817.jpg",
 *                    "email": "tanmoykhanra@lolo.com",
 *                    "dob": "15-06-2017",
 *                    "gender": "Male",
 *                    "phNo": "5678906543",
 *                    "age": "0",
 *                    "location": "Kolkata,India",
 *                    "lat_lng": [
 *                        22.496328225149,
 *                        88.3611998194166
 *                    ],
 *                    "hobbies": "bio",
 *                    "bio": "test",
 *                    "timeZone": null,
 *                    "__v": 0,
 *                    "private_mode": false
 *                },
 *                "likes": [
 *                    {
 *                        "_id": "59c0fa5a20196747d9c9b30a",
 *                        "updatedAt": "2017-09-19T11:07:06.311Z",
 *                        "createdAt": "2017-09-19T11:07:06.311Z",
 *                        "postID": "POST1499860006800",
 *                        "userName": "codetanmoy64",
 *                        "likedOn": "1505819226232",
 *                        "__v": 0
 *                    },
 *                    {
 *                        "_id": "59ddbbe07999c16cef246aa1",
 *                        "updatedAt": "2017-10-11T06:36:16.907Z",
 *                        "createdAt": "2017-10-11T06:36:16.907Z",
 *                        "postID": "POST1499860006800",
 *                        "userName": "faheelkamran",
 *                        "likedOn": "1507703776744",
 *                        "__v": 0
 *                    }
 *                ],
 *                "comments": [
 *                    {
 *                        "_id": "5966673b0b29a104652c176f",
 *                        "updatedAt": "2017-07-12T18:15:23.266Z",
 *                        "createdAt": "2017-07-12T18:15:23.266Z",
 *                        "postID": "POST1499860006800",
 *                        "userName": "faheelkamran",
 *                        "message": "Wow",
 *                        "logTimeStamp": "1499883323266",
 *                        "__v": 0
 *                    },
 *                    {
 *                        "_id": "59c0fa3020196747d9c9b308",
 *                        "updatedAt": "2017-09-19T11:06:24.553Z",
 *                        "createdAt": "2017-09-19T11:06:24.553Z",
 *                        "postID": "POST1499860006800",
 *                        "userName": "codetanmoy64",
 *                        "message": "nice",
 *                        "logTimeStamp": "1505819184553",
 *                        "__v": 0
 *                    },
 *                    {
 *                        "_id": "59c0fa5020196747d9c9b309",
 *                        "updatedAt": "2017-09-19T11:06:56.529Z",
 *                        "createdAt": "2017-09-19T11:06:56.529Z",
 *                        "postID": "POST1499860006800",
 *                        "userName": "codetanmoy64",
 *                        "message": "yo",
 *                        "logTimeStamp": "1505819216529",
 *                        "__v": 0
 *                    },
 *                    {
 *                        "_id": "59e5a75261fce42949159528",
 *                        "updatedAt": "2017-10-17T06:46:42.054Z",
 *                        "createdAt": "2017-10-17T06:46:42.054Z",
 *                        "postID": "POST1499860006800",
 *                        "userName": "codetanmoy64",
 *                        "message": "@testing ",
 *                        "logTimeStamp": "1508222802053",
 *                        "__v": 0
 *                    }
 *                ]
 *            }
 *        ]
 *    }
 *}
 **/

// Get my connections' posts
module.exports.myConnectionFeed = function (req, res) {
	Model.UsersBlocked.aggregate([{
			$match: {
				userName: req.userName
			}
		},
		{
			$group: {
				_id: "$userName",
				blockedUsers: {
					$push: "$blockedUser"
				}
			}
		}
	]).exec(function (error_3, data_3) {
		if (error_3) {
			res.json({
				response: {
					success: false,
					message: error_text,
					data: error_3
				}
			});
		} else {
		    console.log(data_3);
		    var data_check = [];
		    if(data_3.length > 0) data_check = data_3[0].blockedUsers;

			Model.UserConnections.aggregate([{
						$match: {
							$and: [{
									myUserName: req.userName
								},
								{
									followRequestAccepted: "2"
								},
								{
									iAmFollowingUserName: {
										$nin: data_check
									}
								}
							]
						}
					},
					{
						$group: {
							_id: "$followRequestAccepted",
							connections: {
								$push: "$iAmFollowingUserName"
							}
						}
					}
				],
				function (error, data) {
					if (error) {
						res.json({
							response: {
								success: false,
								message: error_text,
								data: error
							}
						});
					} else {
					    var connections = [];
					    if(data.length > 0) connections = data[0].connections;
						console.log("My Connections", connections);

						Model.Posts.aggregate([{
								$match: {
									userName: {
										$in: connections
									},
									logTimeStamp: {
										$lt: req.params.fromtime
									}
								}
							}, {
								$lookup: {
									from: "userdetails",
									localField: "userName",
									foreignField: "userName",
									as: "user"
								}
							},
							{
								$sort: {
									_id: -1
								}
							},
							{
								$project: {
									_id: 1,
									updatedAt: 1,
									createdAt: 1,
									postID: 1,
									userName: 1,
									message: 1,
									imageUrl: 1,
									userLocation: 1,
									reported: 1,
									logTimeStamp: 1,
									user: {
										$arrayElemAt: ["$user", 0]
									}
								}
							},
							{
								$lookup: {
									from: "likes",
									localField: "postID",
									foreignField: "postID",
									as: "likes"
								}
							},
							{
								$lookup: {
									from: "comments",
									localField: "postID",
									foreignField: "postID",
									as: "comments"
								}
							},
							{
								$limit: 10
							}
						]).exec(function (error_user, data_user) {
							if (error_user) {
								res.json({
									response: {
										success: false,
										message: error_text,
										data: error_user
									}
								});
							} else {
								res.json({
									response: {
										success: true,
										message: "Posts of my connections fetched successfully!",
										data: data_user
									}
								});
							}
						});
					}
				}
			);
		}
	});
};

/**
 *@api {post} /api/feed/search/ Search Feeds & People
 *@apiVersion 0.0.1
 *@apiGroup Feed
 *@apiPermission all
 *@apiDescription Search through all public feeds & people
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} search_text search text that user types
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 * {
 *    "response": {
 *        "success": true,
 *        "message": "Posts of my connections fetched successfully!",
 *        "data": {
 *            "posts": [
 *                {
 *                    "_id": "5972ee439ff04b7038801480",
 *                    "updatedAt": "2017-07-22T06:18:43.349Z",
 *                    "createdAt": "2017-07-22T06:18:43.349Z",
 *                    "postID": "POST1500704323347",
 *                    "userName": "lucy",
 *                    "message": "#way to",
 *                    "imageUrl": "/profilepic/prof_1500704318581.jpg",
 *                    "logTimeStamp": "1500704323347",
 *                    "userLocation": "Kolkata,India",
 *                    "reported": "0",
 *                    "user": {
 *                        "_id": "5941a0335ebac10a8ab3412d",
 *                        "updatedAt": "2017-11-01T14:28:12.832Z",
 *                        "createdAt": "2017-06-14T20:44:35.958Z",
 *                        "userName": "lucy",
 *                        "name": "popo lucy",
 *                        "profilePic": "/profilepic/prof_1497473102817.jpg",
 *                        "email": "tanmoykhanra@lolo.com",
 *                        "dob": "15-06-2017",
 *                        "gender": "Male",
 *                        "phNo": "5678906543",
 *                        "age": "0",
 *                        "location": "",
 *                        "lat_lng": [
 *                            22.496267,
 *                            88.361191
 *                        ],
 *                        "hobbies": "bio",
 *                        "bio": "test",
 *                        "timeZone": null,
 *                        "__v": 0,
 *                        "private_mode": false
 *                    },
 *                    "likes": [
 *                        {
 *                            "_id": "59c1beca14fc9945edf65c50",
 *                            "updatedAt": "2017-09-20T01:05:14.583Z",
 *                            "createdAt": "2017-09-20T01:05:14.583Z",
 *                            "postID": "POST1500704323347",
 *                            "userName": "codetanmoy64",
 *                            "likedOn": "1505869514463",
 *                            "__v": 0
 *                        }
 *                    ],
 *                    "comments": [
 *                        {
 *                            "_id": "59f1d7e3f5e69723c3912462",
 *                            "updatedAt": "2017-10-26T12:41:07.602Z",
 *                            "createdAt": "2017-10-26T12:41:07.602Z",
 *                            "postID": "POST1500704323347",
 *                            "userName": "codetanmoy64",
 *                            "message": "test comments ",
 *                            "logTimeStamp": "1509021667520",
 *                            "__v": 0
 *                        }
 *                    ]
 *                },
 *                {
 *                    "_id": "5966399f0b29a104652c176b",
 *                    "updatedAt": "2017-07-12T15:00:47.739Z",
 *                    "createdAt": "2017-07-12T15:00:47.739Z",
 *                    "postID": "POST1499871647738",
 *                    "userName": "lucy",
 *                    "message": "banana",
 *                    "imageUrl": "/profilepic/prof_1499871644889.jpg",
 *                    "logTimeStamp": "1499871647738",
 *                    "userLocation": "Kolkata,India",
 *                    "reported": "0",
 *                    "user": {
 *                        "_id": "5941a0335ebac10a8ab3412d",
 *                        "updatedAt": "2017-11-01T14:28:12.832Z",
 *                        "createdAt": "2017-06-14T20:44:35.958Z",
 *                        "userName": "lucy",
 *                        "name": "popo lucy",
 *                        "profilePic": "/profilepic/prof_1497473102817.jpg",
 *                        "email": "tanmoykhanra@lolo.com",
 *                        "dob": "15-06-2017",
 *                        "gender": "Male",
 *                        "phNo": "5678906543",
 *                        "age": "0",
 *                        "location": "",
 *                        "lat_lng": [
 *                            22.496267,
 *                            88.361191
 *                        ],
 *                        "hobbies": "bio",
 *                        "bio": "test",
 *                        "timeZone": null,
 *                        "__v": 0,
 *                        "private_mode": false
 *                    },
 *                    "likes": [
 *                        {
 *                            "_id": "59c0fa6220196747d9c9b30b",
 *                            "updatedAt": "2017-09-19T11:07:14.513Z",
 *                            "createdAt": "2017-09-19T11:07:14.513Z",
 *                            "postID": "POST1499871647738",
 *                            "userName": "codetanmoy64",
 *                            "likedOn": "1505819234511",
 *                            "__v": 0
 *                        }
 *                    ],
 *                    "comments": [
 *                        {
 *                            "_id": "59f0aa8307ee5453d8bda08f",
 *                            "updatedAt": "2017-10-25T15:15:15.403Z",
 *                            "createdAt": "2017-10-25T15:15:15.403Z",
 *                            "postID": "POST1499871647738",
 *                            "userName": "codetanmoy64",
 *                            "message": "good test",
 *                            "logTimeStamp": "1508944515402",
 *                            "__v": 0
 *                        }
 *                    ]
 *                }
 *            ],
 *            "people": [
 *                {
 *                    "_id": "594057658bd9132e2680a50d",
 *                    "updatedAt": "2017-06-13T21:21:41.334Z",
 *                    "createdAt": "2017-06-13T21:21:41.334Z",
 *                    "userName": "codetanmoy",
 *                    "profilePic": "/profilepic/prof_1497388896994.jpg",
 *                    "email": "tanmoykhanra@live.com",
 *                    "dob": "",
 *                    "gender": "Male",
 *                    "phNo": "9007011860",
 *                    "age": "",
 *                    "location": "",
 *                    "lat_lng": [
 *                        0,
 *                        0
 *                    ],
 *                    "hobbies": "",
 *                    "bio": "",
 *                    "timeZone": "",
 *                    "__v": 0,
 *                    "private_mode": false
 *                },
 *                {
 *                    "_id": "59405ab18bd9132e2680a513",
 *                    "updatedAt": "2017-09-07T14:05:57.597Z",
 *                    "createdAt": "2017-06-13T21:35:45.786Z",
 *                    "userName": "codetanmoy32",
 *                    "profilePic": "/profilepic/prof_1497389740612.jpg",
 *                    "email": "tanmoykhnara@live.com",
 *                    "dob": "05-08-1987",
 *                    "gender": "Male",
 *                    "phNo": "9735362926",
 *                    "age": "30",
 *                    "location": "Kolkata,India",
 *                    "lat_lng": [
 *                        22.4962665421229,
 *                        88.3611914142966
 *                    ],
 *                    "hobbies": "",
 *                    "bio": "who am I ?",
 *                    "timeZone": null,
 *                    "__v": 0,
 *                    "name": "Naina Poddar",
 *                    "private_mode": false
 *                }
 *            ]
 *        }
 *    }
 *}
 **/

// Search through public feeds
module.exports.searchPublicFeed = function (req, res) {
	Model.UsersBlocked.find({
			userName: req.userName
		}, {
			blockedUser: 1,
			_id: 0
		},
		function (error_3, data_3) {
			if (error_3) {
				res.json({
					response: {
						success: false,
						message: error_text,
						data: error_3
					}
				});
			} else {
				var data_check = [];
				for (var m = 0; m < data_3.length; m++) {
					data_check.push(data_3[m]["blockedUser"]);
				}

				Model.UserDetails.find({
						$and: [{
								userName: {
									$nin: data_check
								}
							},
							{
								$or: [{
										userName: {
											$regex: req.body.search_text,
											$options: "i"
										}
									},
									{
										email: {
											$regex: req.body.search_text,
											$options: "i"
										}
									},
									{
										name: {
											$regex: req.body.search_text,
											$options: "i"
										}
									}
								]
							}
						]
					},
					function (error, data_users_searched) {
						if (error) {
							res.json({
								response: {
									success: false,
									message: error_text,
									data: error
								}
							});
						} else {
							Model.Posts.aggregate([{
									$lookup: {
										from: "userdetails",
										localField: "userName",
										foreignField: "userName",
										as: "user"
									}
								},
								{
									$sort: {
										_id: -1
									}
								},
								{
									$project: {
										_id: 1,
										updatedAt: 1,
										createdAt: 1,
										postID: 1,
										userName: 1,
										message: 1,
										imageUrl: 1,
										userLocation: 1,
										reported: 1,
										logTimeStamp: 1,
										user: {
											$arrayElemAt: ["$user", 0]
										}
									}
								},
								{
									$match: {
										$and: [{
												"user.private_mode": false
											},
											{
												userName: {
													$nin: data_check
												}
											},
											{
												$or: [{
														message: {
															$regex: req.body.search_text,
															$options: "i"
														}
													},
													{
														userName: {
															$regex: req.body.search_text,
															$options: "i"
														}
													}
												]
											}
										]
									}
								},
								{
									$lookup: {
										from: "likes",
										localField: "postID",
										foreignField: "postID",
										as: "likes"
									}
								},
								{
									$lookup: {
										from: "comments",
										localField: "postID",
										foreignField: "postID",
										as: "comments"
									}
								}
							]).exec(function (error, data_posts) {
								if (error) {
									res.json({
										response: {
											success: false,
											message: error_text,
											data: error
										}
									});
								} else {
									res.json({
										response: {
											success: true,
											message: "Posts of my connections fetched successfully!",
											data: {
												posts: data_posts,
												people: data_users_searched
											}
										}
									});
								}
							});
						}
					}
				);
			}
		}
	);
};

/**
 *@api {get} /api/feed/details/:postID Fetch Details of a Post
 *@apiVersion 0.0.1
 *@apiGroup Feed
 *@apiPermission all
 *@apiDescription Fetch Details of anyone's post by specifying the postID of the post
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} postID postID of the post to get details
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *{
 *    "response": {
 *        "success": true,
 *        "message": "The Post fetched successfully!",
 *        "data": {
 *            "__v": 0,
 *            "reported": "0",
 *            "userLocation": "Kolkata,India",
 *            "logTimeStamp": "1500704323347",
 *            "imageUrl": "/profilepic/prof_1500704318581.jpg",
 *            "message": "#way to",
 *            "userName": "lucy",
 *            "postID": "POST1500704323347",
 *            "createdAt": "2017-07-22T06:18:43.349Z",
 *            "updatedAt": "2017-07-22T06:18:43.349Z",
 *            "_id": "5972ee439ff04b7038801480",
 *            "likes": [
 *                {
 *                    "_id": "59c1beca14fc9945edf65c50",
 *                    "updatedAt": "2017-09-20T01:05:14.583Z",
 *                    "createdAt": "2017-09-20T01:05:14.583Z",
 *                    "postID": "POST1500704323347",
 *                    "userName": "codetanmoy64",
 *                    "likedOn": "1505869514463",
 *                    "__v": 0
 *                }
 *            ],
 *            "comments": [
 *                {
 *                    "_id": "596659500b29a104652c176c",
 *                    "updatedAt": "2017-07-12T17:16:00.260Z",
 *                    "createdAt": "2017-07-12T17:16:00.260Z",
 *                    "postID": "POST1499794408338",
 *                    "userName": "lucy",
 *                    "message": "nice",
 *                    "logTimeStamp": "1499879760259",
 *                    "__v": 0
 *                }
 *            ]
 *        }
 *    }
 *}
 **/

// Get my posts
module.exports.getFeed = function (req, res) {
	Model.Posts.aggregate([{
			$match: {
				postID: req.params.postID
			}
		},
		{
			$lookup: {
				from: "likes",
				localField: "postID",
				foreignField: "postID",
				as: "likes"
			}
		},
		{
			$lookup: {
				from: "comments",
				localField: "postID",
				foreignField: "postID",
				as: "comments"
			}
		}
	]).exec(function (error_post, data_post) {
		if (error_post) {
			res.json({
				response: {
					success: false,
					message: error_text,
					data: error_post
				}
			});
		} else {
			res.json({
				response: {
					success: true,
					message: "The Post fetched successfully!",
					data: data_post[0]
				}
			});
		}
	});
};
