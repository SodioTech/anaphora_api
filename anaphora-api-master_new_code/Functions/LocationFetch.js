'use strict';
var Model = require('./../Controllers/ModelLinks.js');
var error_text = "Something Went Wrong! Please check your network or try again!";
var geocoder = require('geocoder');

/**
 *@api {get} /api/explore/:lat/:lng/:distance/:offset/:limit Find Users in Area
 *@apiVersion 0.0.1
 *@apiGroup Explore
 *@apiPermission all
 *@apiDescription Fetch users available to chat around a location
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} lat latitude of 2D index of location
 *@apiParam {String} lng longitude of 2D index of location
 *@apiParam {String} distance radius of the location searched in
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *    "response": {
 *        "success": true,
 *        "message": "Users around you",
 *        "data": [
 *            {
 *                "__v": 0,
 *                "logTimeStamp": "1500479944176",
 *                "locationName": "188, Raja Subodh Chandra Mallick Rd, Poddar Nagar, Jadavpur, Kolkata, West Bengal 700032, India",
 *                "userLocation": [
 *                    22.4994,
 *                    88.3713
 *                ],
 *                "userName": "codetanmoy32",
 *                "createdAt": "2017-07-19T15:58:23.092Z",
 *                "updatedAt": "2017-07-19T15:59:04.483Z",
 *                "_id": "596f819fd71c5515914b6740",
 *                "user_details": {
 *                    "_id": "59405ab18bd9132e2680a513",
 *                    "updatedAt": "2017-06-14T12:35:55.500Z",
 *                    "createdAt": "2017-06-13T21:35:45.786Z",
 *                    "userName": "codetanmoy32",
 *                    "profilePic": "/profilepic/prof_1497389740612.jpg",
 *                    "email": "tanmoykhnara@live.com",
 *                    "dob": "05-08-1987",
 *                    "gender": "Male",
 *                    "phNo": "9735362926",
 *                    "age": "30",
 *                    "location": "",
 *                    "lat_lng": [
 *                        22.4646464,
 *                        83.164364345
 *                    ],
 *                    "hobbies": "",
 *                    "bio": "",
 *                    "timeZone": "",
 *                    "__v": 0,
 *                    "name": "Naina Poddar"
 *                },
 *                "user_feed": [],
 *                "connection": true
 *            },
 *            {
 *                "locationName": "Nabin Krishna Ghoshal Road, Ruby Park, Kasbah North, Kolkata, Hāora, West Bengal, 700016, India",
 *                "logTimeStamp": "1500480090.897239",
 *                "userLocation": [
 *                    22.51290667,
 *                    88.37512264
 *                ],
 *                "userName": "perry",
 *                "_id": "596f825bfc52ff1638012b86",
 *                "user_details": {
 *                    "_id": "59418bbc5ebac10a8ab3411d",
 *                    "updatedAt": "2017-06-14T19:17:16.688Z",
 *                    "createdAt": "2017-06-14T19:17:16.688Z",
 *                    "userName": "perry",
 *                    "name": "Naina Poddar",
 *                    "profilePic": "/profilepic/prof_1497389740612.jpg",
 *                    "email": "tanmoykhnara@live.com",
 *                    "dob": "05-08-1987",
 *                    "gender": "Male",
 *                    "phNo": "97453675768",
 *                    "age": "30",
 *                    "location": "",
 *                    "lat_lng": [
 *                        22.4646464,
 *                        83.164364345
 *                    ],
 *                    "hobbies": "",
 *                    "bio": "",
 *                    "timeZone": "",
 *                    "__v": 0
 *                },
 *                "user_feed": ["/profilepic/prof_1499883234806.jpg", "/profilepic/prof_1499883256767.jpg"],
 *                "connection": true
 *            }
 *        ]
 *    }
 *}
 **/

// Location Searches
module.exports.searchAroundUser = function (req, res) {

	console.log(req.params);

	if (req.params.lat == undefined && req.params.lng == undefined && req.params.distance == undefined && req.params.offset == undefined && req.params.limit == undefined) {
		res.json({
			response: {
				success: false,
				message: "Invalid parameters!",
				data: req.params
			}
		});
	} else {

		var epoch_milliseconds = (new Date).getTime();
		var lat = parseFloat(req.params.lat).toFixed(6);
		var lng = parseFloat(req.params.lng).toFixed(6);

		// geocoder.reverseGeocode(lat, lng, function (err, geo_data) {
		// 	if (err) {
		// 		console.log(err);
		// 		res.json({
		// 			response: {
		// 				success: false,
		// 				message: "Invalid Location",
		// 				data: error
		// 			}
		// 		});
		// 	} else {
		// 		//console.log(geo_data);

		// 	}
		// });

		var logDetails = new Model.LocationSearches({
			locationSearchLoggedID: "location_" + epoch_milliseconds,
			userName: req.userName,
			userLocation: [lat, lng],
			locationName: "",
			logTimeStamp: epoch_milliseconds
		});

		// Save log search request of user
		logDetails.save(function (error, data) {
			if (error) {
				res.json({
					response: {
						success: false,
						message: error_text,
						data: error
					}
				});
			} else {

				var distance = parseInt(req.params.distance) / 111.128;

				var query = {
					userLocation: {
						$near: [lat, lng],
						$maxDistance: distance
					}
				};

				// find users in that area
				Model.LocationCurrent.find(query).skip(parseFloat(req.params.offset)).limit(parseFloat(req.params.limit)).exec(function (error, data) {
					if (error) {
						res.json({
							response: {
								success: false,
								message: error_text,
								data: error
							}
						});
					} else {
						var search_list_data = data;

						var user_userName = [];
						var search_list = {};

						for (var k = 0; k < search_list_data.length; k++) {
							if (user_userName.indexOf(search_list_data[k]["userName"]) == -1) {
								user_userName.push(search_list_data[k]["userName"]);
							}
						}

						console.log("LocationCurrent Database");
						console.log(user_userName);

						// get details of users in the area
						Model.UserDetails.find({
							userName: {
								$in: user_userName
							}
						}, function (error, user_data) {
							if (error) {
								res.json({
									response: {
										success: false,
										message: error_text,
										data: error
									}
								});
							} else {
								var user_name_list = [];
								var user_data_all = {};
								for (var k = 0; k < user_data.length; k++) {
									user_name_list.push(user_data[k]["userName"]);
									user_data_all[user_data[k]["userName"]] = user_data[k];
								}

								console.log("UserDetails Database");
								console.log(user_name_list);

								Model.UsersBlocked.find({
									userName: req.userName
								}, {
									blockedUser: 1,
									_id: 0
								}, function (error_3, data_3) {
									if (error_3) {
										res.json({
											response: {
												success: false,
												message: error_text,
												data: error_3
											}
										});
									} else {

										console.log(data_3);

										var data_check = [];
										for (var m = 0; m < data_3.length; m++) {
											data_check.push(data_3[m]["blockedUser"]);
										}

										Model.UserConnections.find({
											$and: [{
													$or: [{
															myUserName: req.userName
														},
														{
															iAmFollowingUserName: req.userName
														}
													]
												},
												{
													followRequestAccepted: "2"
												},
												{
													myUserName: {
														$nin: data_check
													}
												},
												{
													iAmFollowingUserName: {
														$nin: data_check
													}
												}
											]
										}, {
											_id: 0
										}, function (error, data) {
											if (error) {
												res.json({
													response: {
														success: false,
														message: error_text,
														data: error
													}
												});
											} else {

												var data_conn = [];
												for (var m = 0; m < data.length; m++) {
													data_conn.push(data[m]["iAmFollowingUserName"]);
												}

												// find Posts of the users generated
												Model.Posts.find({
													userName: {
														$in: user_userName
													}
												}, function (error, post_data) {
													if (error) {
														res.json({
															response: {
																success: false,
																message: error_text,
																data: error
															}
														});
													} else {
														var post_user_name_list = [];
														var post_user_feed_data = {};
														for (var k = 0; k < post_data.length; k++) {
															if (post_user_name_list.indexOf(post_data[k]["userName"]) == -1) {
																post_user_name_list.push(post_data[k]["userName"]);
																post_user_feed_data[post_data[k]["userName"]] = [];
															}
															post_user_feed_data[post_data[k]["userName"]].push(post_data[k]["imageUrl"]);
														}

														console.log("Posts Database");
														console.log(post_user_name_list);

														console.log("Processing Search List =======");

														var search_list = [];

														for (var k = 0; k < search_list_data.length; k++) {
															console.log(search_list_data[k]["userName"]);
															var search_data = {};
															for (var property in search_list_data[k]["_doc"]) {
																if (search_list_data[k]["_doc"].hasOwnProperty(property)) {
																	search_data[property] = search_list_data[k]["_doc"][property];
																}
															}
															//console.log(user_data_all[search_list_data[k]["userName"]]);
															search_data["user_details"] = user_data_all[search_list_data[k]["userName"]];
															//console.log(post_user_feed_data[search_list_data[k]["userName"]]);
															if (post_user_feed_data[search_list_data[k]["userName"]] == undefined) {
																search_data["user_feed"] = [];
															} else {
																search_data["user_feed"] = post_user_feed_data[search_list_data[k]["userName"]];
															}
															search_data["connection"] = false;
															if (data_conn.indexOf(search_data["userName"]) != -1){
																search_data["connection"] = true;
															}
															search_list.push(search_data);
														}

														res.json({
															response: {
																success: true,
																message: "Users around you",
																data: search_list
															}
														});
													}
												});
											}
										});
									}
								});
							}
						});
					}
				});
			}
		});
	}
};