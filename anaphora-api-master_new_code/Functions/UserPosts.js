"use strict";
var Model = require("./../Controllers/ModelLinks.js");
var Notification = require("./Utility/Notification.js");
var error_text =
	"Something Went Wrong! Please check your network or try again!";

/**
 *@api {post} /api/post/add/ Post Add
 *@apiVersion 0.0.1
 *@apiGroup Post
 *@apiPermission all
 *@apiDescription Add User Post to feed
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} message Message to post
 *@apiParam {String} imageUrl part url of the uploaded image
 *@apiParam {String} userLocation Street Address, city and country, eg: Russel Street, Kolkata, India
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "Saved your post",
 *    "data": {
 *      "__v": 0,
 *      "updatedAt": "2017-04-11T22:30:01.803Z",
 *      "createdAt": "2017-04-11T22:30:01.803Z",
 *      "postID": "POST1491949801794",
 *      "userName": "perma",
 *      "message": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur a molestie nisi, vel ullamcorper quam. In in nulla dolor. Pellentesque nulla felis, pellentesque sit amet vestibulum ut, dapibus quis odio.",
 *      "imageUrl": "/feedpic/2354353654.jpg",
 *      "logTimeStamp": "1491949801794",
 *      "userLocation": "Bhawanipur, Kolkata",
 *      "reported": "0",
 *      "_id": "58ed58e9e33ede07306cc402"
 *    }
 *  }
 *}
 **/

// Create a Post
module.exports.addPostToFeed = function (req, res) {

	Model.UserSessionLog.findOne({
		tokenAssigned: req.headers["authorization"]
	}, function (error_user, data_user) {
		if (error_user) {
			res.json({
				response: {
					success: false,
					message: error_text,
					data: error_user
				}
			});
		} else {
			if (data_user == null) {
				res.status(403).json({
					response: {
						success: false,
						message: "Unauthorised Access",
						data: data_user
					}
				});
			} else {
				Model.UserDetails.findOne({
					userName: data_user.userName
				}, function (error, data_userdetails) {
					if (error) {
						res.json({
							response: {
								success: false,
								message: error_text,
								data: error
							}
						});
					} else {
						if (data_userdetails == null) {
							res.status(404).json({
								response: {
									success: false,
									message: "User details not found",
									data: data_userdetails
								}
							});
						} else {
							//console.log("Request method " + req.method);
							console.log("User status is " + data_userdetails.status);
							if (data_userdetails.status === "BLOCKED") {
								res.json({
									response: {
										success: false,
										message: "We determined that you have violated our terms and conditions found at anaphorachat.com/terms.html. Your account has been suspended. Please contact us if you think this was a mistake at anaphora@usa.com. ",
										data: null

									}
								});
							} else {
								var epoch_milliseconds = new Date().getTime();

								var post = new Model.Posts({
									postID: "POST" + epoch_milliseconds,
									userName: req.userName,
									message: req.body.message,
									imageUrl: req.body.imageUrl,
									logTimeStamp: epoch_milliseconds,
									userLocation: req.body.userLocation,
									reported: "0"
								});

								post.save(function (error, data) {
									if (error) {
										res.json({
											response: {
												success: false,
												message: error_text,
												data: error
											}
										});
									} else {
										res.json({
											response: {
												success: true,
												message: "Saved your post",
												data: data
											}
										});
									}
								});
							}
						}
					}
				});
			}
		}
	});
};

/**
 *@api {post} /api/post/report/ Post Report
 *@apiVersion 0.0.1
 *@apiGroup Post
 *@apiPermission all
 *@apiDescription Report other's posts if you find it offensive
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} post_id _id of the post
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "Post Reported",
 *    "data": { "ok" : 1, "nModified" : 1, "n" : 1 }
 *  }
 *}
 **/

// report post
module.exports.reportPost = function (req, res) {
	var epoch_milliseconds = new Date().getTime();

	console.log(req.body.post_id);

	var query = {
		postID: req.body.post_id
	};

	var update = {
		reported: "1"
	};

	Model.Posts.update(query, update, function (error, data) {
		if (error) {
			res.json({
				response: {
					success: false,
					message: error_text,
					data: error
				}
			});
		} else {
			res.json({
				response: {
					success: true,
					message: "Post Reported",
					data: data
				}
			});
		}
	});
	ß;
};

/**
 *@api {post} /api/post/delete/ Delete Post
 *@apiVersion 0.0.1
 *@apiGroup Post
 *@apiPermission all
 *@apiDescription Delete your own post
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} post_id _id of the post
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "Post Deleted",
 *    "data": { "ok" : 1, "nModified" : 1, "n" : 1 }
 *  }
 *}
 **/

// Delete post
module.exports.deletePost = function (req, res) {
	var epoch_milliseconds = new Date().getTime();

	console.log(req.body.post_id);

	Model.Posts.findOne({
		postID: req.body.post_id
	}).exec(function (error, data) {
		if (error) {
			res.json({
				response: {
					success: false,
					message: error_text,
					data: error
				}
			});
		} else {
			if (data != null) {
				if (data.userName == req.userName) {
					Model.Posts.findByIdAndRemove({
						_id: data._id
					}).exec(function (
						error,
						data
					) {
						if (error) {
							res.json({
								response: {
									success: false,
									message: error_text,
									data: error
								}
							});
						} else {
							res.json({
								response: {
									success: true,
									message: "Post Deleted",
									data: data
								}
							});
						}
					});
				} else {
					res.json({
						response: {
							success: false,
							message: "You're not Authorised to Delete this Post!",
							data: null
						}
					});
				}
			} else {
				res.json({
					response: {
						success: false,
						message: "Post Not Found",
						data: null
					}
				});
			}
		}
	});
	var query = {
		_id: req.body.post_id
	};
};

/**
 *@api {post} /api/post/like/ Like-Unlike Post
 *@apiVersion 0.0.1
 *@apiGroup Post
 *@apiPermission all
 *@apiDescription Like a user\'s post or unlike it
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} postID _id of the post
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "You liked a post",
 *    "data": {
 *      "__v": 0,
 *      "updatedAt": "2017-04-11T22:44:19.607Z",
 *      "createdAt": "2017-04-11T22:44:19.607Z",
 *      "postID": "58ed58e9e33ede07306cc402",
 *      "userName": "pierce",
 *      "likedOn": "1491950659600",
 *      "_id": "58ed5c432c0a5f086ab0c462"
 *    }
 *  }
 *}
 **/

// Like a post
module.exports.likePost = function (req, res) {
	Model.UserSessionLog.findOne({
		tokenAssigned: req.headers["authorization"]
	}, function (error_user, data_user) {
		if (error_user) {
			res.json({
				response: {
					success: false,
					message: error_text,
					data: error_user
				}
			});
		} else {
			if (data_user == null) {
				res.status(403).json({
					response: {
						success: false,
						message: "Unauthorised Access",
						data: data_user
					}
				});
			} else {
				Model.UserDetails.findOne({
					userName: data_user.userName
				}, function (error, data_userdetails) {
					if (error) {
						res.json({
							response: {
								success: false,
								message: error_text,
								data: error
							}
						});
					} else {
						if (data_userdetails == null) {
							res.status(404).json({
								response: {
									success: false,
									message: "User details not found",
									data: data_userdetails
								}
							});
						} else {
							//console.log("Request method " + req.method);
							console.log("User status is " + data_userdetails.status);
							if (data_userdetails.status === "BLOCKED") {
								res.json({
									response: {
										success: false,
										message: "We determined that you have violated our terms and conditions found at anaphorachat.com/terms.html. Your account has been suspended. Please contact us if you think this was a mistake at anaphora@usa.com. ",
										data: null

									}
								});
							} else {
								var epoch_milliseconds = new Date().getTime();

								var postLike = new Model.Likes({
									postID: req.body.postID,
									userName: req.userName,
									likedOn: epoch_milliseconds
								});

								Model.Posts.findOne({
										postID: req.body.postID
									},
									function (error_post, data_post) {
										if (error_post) {
											res.json({
												response: {
													success: false,
													message: error_text,
													data: error_post
												}
											});
										} else {
											if (data_post != null) {
												Model.Likes.findOne({
														postID: req.body.postID,
														userName: req.userName
													},
													function (error_user, data_user) {
														if (error_user) {
															res.json({
																response: {
																	success: false,
																	message: error_text,
																	data: error_user
																}
															});
														} else {
															if (data_user == null) {
																postLike.save(function (error, data) {
																	if (error) {
																		res.json({
																			response: {
																				success: false,
																				message: error_text,
																				data: error
																			}
																		});
																	} else {
																		if (data_post.userName == req.userName) {
																			res.json({
																				response: {
																					success: true,
																					message: "You liked a post",
																					data: data
																				}
																			});
																		} else {
																			Notification.send(
																				data_post.userName,
																				"Post",
																				req.userName + " liked your photo.",
																				"",
																				req.userName,
																				data_post.postID,
																				function (error_notif, data_notif) {
																					res.json({
																						response: {
																							success: true,
																							message: "You liked a post",
																							data: data
																						}
																					});
																				}
																			);
																		}
																	}
																});
															} else {
																Model.Likes.remove({
																		_id: data_user._id
																	},
																	function (error, data) {
																		if (error) {
																			res.json({
																				response: {
																					success: false,
																					message: error_text,
																					data: error
																				}
																			});
																		} else {
																			res.json({
																				response: {
																					success: true,
																					message: "You unliked a post!",
																					data: data
																				}
																			});
																		}
																	}
																);
															}
														}
													}
												);
											} else {
												res.json({
													response: {
														success: false,
														message: "Post not Found!",
														data: null
													}
												});
											}
										}
									}
								);
							}
						}
					}
				});
			}
		}
	});


};

/**
 *@api {post} /api/post/comment/add/ Comment on a post
 *@apiVersion 0.0.1
 *@apiGroup Post
 *@apiPermission all
 *@apiDescription Comment Post
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} postID _id of the post
 *@apiParam {String} message message of the post
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "You commented on a post",
 *    "data": {
 *      "__v": 0,
 *      "updatedAt": "2017-04-11T22:54:03.725Z",
 *      "createdAt": "2017-04-11T22:54:03.725Z",
 *      "postID": "58ed58e9e33ede07306cc402",
 *      "userName": "perry",
 *      "message": "Sed varius id mi tempor vulputate",
 *      "logTimeStamp": "1491951243718",
 *      "_id": "58ed5e8b18dca808b6fdf7db"
 *    }
 *  }
 *}
 **/

// Comment on a post
module.exports.addCommentToPost = function (req, res) {
	Model.UserSessionLog.findOne({
		tokenAssigned: req.headers["authorization"]
	}, function (error_user, data_user) {
		if (error_user) {
			res.json({
				response: {
					success: false,
					message: error_text,
					data: error_user
				}
			});
		} else {
			if (data_user == null) {
				res.status(403).json({
					response: {
						success: false,
						message: "Unauthorised Access",
						data: data_user
					}
				});
			} else {
				Model.UserDetails.findOne({
					userName: data_user.userName
				}, function (error, data_userdetails) {
					if (error) {
						res.json({
							response: {
								success: false,
								message: error_text,
								data: error
							}
						});
					} else {
						if (data_userdetails == null) {
							res.status(404).json({
								response: {
									success: false,
									message: "User details not found",
									data: data_userdetails
								}
							});
						} else {
							//console.log("Request method " + req.method);
							console.log("User status is " + data_userdetails.status);
							if (data_userdetails.status === "BLOCKED") {
								res.json({
									response: {
										success: false,
										message: "We determined that you have violated our terms and conditions found at anaphorachat.com/terms.html. Your account has been suspended. Please contact us if you think this was a mistake at anaphora@usa.com. ",
										data: null

									}
								});
							} else {
								var epoch_milliseconds = new Date().getTime();

								var postComment = new Model.Comments({
									postID: req.body.postID,
									userName: req.userName,
									message: req.body.message,
									logTimeStamp: epoch_milliseconds
								});

								Model.Posts.findOne({
										postID: req.body.postID
									},
									function (error_post, data_post) {
										if (error_post) {
											res.json({
												response: {
													success: false,
													message: error_text,
													data: error_post
												}
											});
										} else {
											if (data_post != null) {
												postComment.save(function (error, data) {
													if (error) {
														res.json({
															response: {
																success: false,
																message: error_text,
																data: error
															}
														});
													} else {
														Notification.send(
															data_post.userName,
															"Comment",
															req.userName + " commented on your photo.",
															"",
															req.userName,
															data_post.postID,
															function (error_notif, data_notif) {
																res.json({
																	response: {
																		success: true,
																		message: "You commented on a post",
																		data: data
																	}
																});
															}
														);
													}
												});
											} else {
												res.json({
													response: {
														success: false,
														message: "Post not Found!",
														data: null
													}
												});
											}
										}
									}
								);
							}
						}
					}
				});
			}
		}
	});

};

/**
 *@api {post} /api/post/comment/remove/ Delete comment
 *@apiVersion 0.0.1
 *@apiGroup Post
 *@apiPermission all
 *@apiDescription Delete a comment from a post
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} _id _id of the comment
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "Post Deleted",
 *    "data": { "ok" : 1, "nModified" : 1, "n" : 1 }
 *  }
 *}
 **/

// Delete a comment on a post
module.exports.removeCommentFromPost = function (req, res) {
	var epoch_milliseconds = new Date().getTime();

	Model.Comments.remove({
			_id: req.body._id
		},
		function (error, data) {
			if (error) {
				res.json({
					response: {
						success: false,
						message: error_text,
						data: error
					}
				});
			} else {
				res.json({
					response: {
						success: true,
						message: "Deleted your comment on a post!",
						data: data
					}
				});
			}
		}
	);
};