var Model = require('./../../Controllers/ModelLinks.js');

module.exports.send = function (receiver, title, message, imageUrl, related_user, related_data, callback) {

    var epoch_milliseconds = (new Date).getTime();

    var modelData = new Model.Alerts({
        alertID: "alert_" + epoch_milliseconds,
        userNameReceiver: receiver,
        title: title,
        message: message,
        imageUrl: imageUrl,
        related_user: related_user,
        related_data: related_data,
        epochTimeStampSent: epoch_milliseconds,
        epochTimeStampRead: "",
        readStatus: false
    });

    modelData.save(function (error, data) {
        if (error) {
            callback(error, null);
        } else {
            callback(false, data);
        }
    });
};

module.exports.sendmultiple = function (count, receiver, title, message, imageUrl, related_user, related_data, callback) {
    if (count == receiver.length) {
        callback(false, "Done");
    } else {
        module.exports.send(receiver[count], title, message, imageUrl, related_user, related_data, function (error, data) {
            if (error) {
                callback(error, null);
            } else {
                module.exports.sendmultiple(count + 1, receiver, title, message, imageUrl, related_user, related_data, callback);
            }
        });
    }
};