'use strict';
var http = require("http");
var https = require("https");
var request = require('request');
var Model = require('./../../Controllers/ModelLinks.js');

module.exports.createGroup = function (req, requestUsers, callback) {

    var epoch_milliseconds = (new Date).getTime();
    var requestType = req.body.type;
    var ip = req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress;

    var modelData = new Model.ConversationGroupList({
        conversationGroupID: req.userName + "_" + epoch_milliseconds,
        title: "Group of " + requestUsers.length + " people",
        imageURL: "",
        users: requestUsers,
        usersRemoved: [],
        usersDeletedConvGroup: [],
        creatorUserName: req.userName,
        creatorIP: ip,
        creatorLocation: "",
        creatorEpochTimeStamp: epoch_milliseconds,
        isArchived: false,
        type: requestType,
        chatroomFlag: false
    });

    modelData.save(function (error, data) {
        if (error) {
            callback(error, null);
        } else {
            callback(false, data);
        }
    });
};

module.exports.addUser = function (userList, _id, callback) {
    var epoch_milliseconds = (new Date).getTime();

    var query = {
        _id: _id
    };
    var updated = {
        $addToSet: {
            users: {
                $each: userList
            }
        }
    };

    Model.ConversationGroupList.update(query, updated, {
        upsert: false,
        multi: true
    }).exec(function (error, data) {
        if (error) {
            callback(error, null);
        } else {
            callback(false, data);
        }
    });
};

module.exports.showOtherData = function (data, callback) {
    var groupList = [data];
    var groupData = {};

    Model.UserDetails.find({
        userName: {
            $in: data.users
        }
    }).exec(function (error, data) {
        if (error) {
            callback(error, null);
        } else {

            for (var i = 0; i < groupList.length; i++) {

                var post = {};

                for (var property in groupList[i]["_doc"]) {
                    if (groupList[i]["_doc"].hasOwnProperty(property)) {
                        console.log(post[property]);
                        post[property] = groupList[i]["_doc"][property];
                    }
                }

                post.last_message = "";
                post.last_message_time = "";

                var filters = data;
                filters = filters.reduce((result, filter) => {
                    result[filter.userName] = filter;
                    return result;
                }, {});
                post.user_details = filters;

                console.log(post.user_details);

                groupData = post;
            }

            callback(false, groupData);
        }
    });
};