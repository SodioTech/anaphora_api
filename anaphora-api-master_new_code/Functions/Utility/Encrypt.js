'use strict';
var crypto = require('crypto');

//function for encription and decription api_key
module.exports.encrypt = function (text, key) {
    var cipher = crypto.createCipher('aes-256-cbc', key);
    var encryptedPassword = cipher.update(text, 'utf8', 'base64');
    encryptedPassword += cipher.final('base64');
    return encryptedPassword;
};

module.exports.decrypt = function (text, key) {
    var decipher = crypto.createDecipher('aes-256-cbc', key);
    var decryptedPassword = decipher.update(text, 'base64', 'utf8');
    decryptedPassword += decipher.final('utf8');
    return decryptedPassword;
};
