'use strict';
var formidable = require('formidable');

//save picture and details
module.exports.fileUpload = function (req, res, next){

    // Variable Initialisation
    var picType = "";
    var location = "";
    var location_check = req.params.type;
    var path = "";
    var fullpath = "";
    //var fileType = "";

    // Form Initialisation and parsing
    var form = new formidable.IncomingForm();
    form.type = 'urlencoded';
    form.parse(req);

    // File Handling Events
    form.on('fileBegin', function(field, file) {

        //fileType = file.type.split('/').pop();
        //console.log(fileType);

        //epoch time by server
        var epoch_time = (new Date).getTime();

        switch (location_check) {
            case "c":
                location =  process.cwd() + '/uploads/convopic/';
                picType = "conv_";
                fullpath = "/convopic/";
                break;
            case "f":
                location =  process.cwd() + '/uploads/feedpic/';
                picType = "feed_";
                fullpath = "/feedpic/";
                break;
            case "p":
                location =  process.cwd() + '/uploads/profilepic/';
                picType = "prof_";
                fullpath = "/profilepic/";
                break;
            case "s":
                location =  process.cwd() + '/uploads/snapsharepic/';
                picType = "snap_";
                fullpath = "/snapsharepic/";
                break;
            default:
                location =  process.cwd() + '/uploads/nocategorypic/';
                picType = "ncat_";
                fullpath = "/nocategorypic/";
                break;
        }

        file.name = picType + epoch_time + ".jpg";// + fileType;
        file.path =  location + file.name;
        path = file.name;
        console.log(file.path);
    });

    form.on('error', function(err) {
        console.log(err);
        req.resume();
        // res.json({
        //     response:{
        //         success: false,
        //         message: "File Upload Error",
        //         error: err
        //     }
        // });
    });

    form.on('aborted', function(err) {
        res.json({
            response:{
                success: false,
                message: "File Upload Aborted",
                error: err
            }
        });
    });

    form.on('end', function() {
        console.log(fullpath + path);
        res.json({
            response:{
                success: true,
                message: "File Upload Successfully",
                data: fullpath + path
            }
        });
    });
};
