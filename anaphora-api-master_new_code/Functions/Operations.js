"use strict";
var http = require("http");
var https = require("https");
var request = require("request");
var Model = require("./../Controllers/ModelLinks.js");
var ConversationGroups = require("./Utility/ConversationGroups.js");
var Notification = require("./Utility/Notification.js");
const error_text =
	"Something Went Wrong! Please check your network or try again!";

/**
 *@api {post} /api/chat/create/ Create a P2P Chat
 *@apiVersion 0.0.1
 *@apiGroup Chat
 *@apiPermission all
 *@apiDescription Create a chat group by a user
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} type p2p
 *@apiParam {String} userName specify a userName
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "New Conversation saved Successfully",
 *    "data": {
 *      "__v": 0,
 *      "updatedAt": "2017-04-12T16:05:27.936Z",
 *      "createdAt": "2017-04-12T16:05:27.936Z",
 *      "conversationGroupID": "perma_1492013126295",
 *      "title": "",
 *      "imageURL": "",
 *      "creatorUserName": "perma",
 *      "creatorIP": "45.124.5.20",
 *      "creatorLocation": "{\"continent_code\":\"AS\",\"country_code\":\"IN\",\"country_code3\":\"IND\",\"country_name\":\"India\",\"region\":\"West Bengal\",\"city\":\"Kolkata\",\"postal_code\":\"\",\"latitude\":22.569700241089,\"longitude\":88.369697570801,\"dma_code\":0,\"area_code\":0,\"ip\":\"45.124.5.20\"}",
 *      "creatorEpochTimeStamp": "1492013126295",
 *      "isArchived": false,
 *      "type": "single",
 *      "_id": "58ee5047745e9c0b5e012023",
 *      "usersDeletedConvGroup": [],
 *      "usersRemoved": [],
 *      "users": [
 *        "perry",
 *        "perma"
 *      ]
 *    }
 *  }
 *}
 **/

// Create Conversation Group
module.exports.createConversationGroup = function (req, res) {
	Model.ConversationGroupList.findOne({
		$and: [{
				users: {
					$all: [req.userName, req.body.userName]
				}
			},
			{
				type: "p2p"
			},
			{
				"chatroomFlag": false
			}
		]
	}).exec(function (error, data) {
		if (error) {
			res.json({
				response: {
					success: false,
					message: error_text,
					data: error
				}
			});
		} else {
			console.log(data);
			if (data != null) {
				ConversationGroups.showOtherData(data, function (err, data) {
					if (error) {
						res.json({
							response: {
								success: false,
								message: error_text,
								data: error
							}
						});
					} else {
						// Notification.send(
						// 	req.body.userName,
						// 	"Resumed Conversation",
						// 	req.userName,
						// 	"",
						// 	req.userName,
						// 	data._id,
						// 	function (error_notif, data_notif) {
						// 		res.json({
						// 			response: {
						// 				success: true,
						// 				message: "Resume Conversation!",
						// 				data: data
						// 			}
						// 		});
						// 	}
						// );
						return res.json({
							response: {
								success: true,
								message: "Resume Conversation!",
								data: data
							}
						});
					}
				});
			} else {
				ConversationGroups.createGroup(
					req, [req.userName, req.body.userName],
					function (error, data) {
						if (error) {
							res.json({
								response: {
									success: false,
									message: error_text,
									data: error
								}
							});
						} else {
							var message = `New chat from ${req.userName}.`;
							// req.userName + " has started a conversation with you!";
							if (req.body.type == "anon") {
								var message = "Someone has started a conversation with you!";
							}
							ConversationGroups.showOtherData(data, function (err, data) {
								if (error) {
									res.json({
										response: {
											success: false,
											message: error_text,
											data: error
										}
									});
								} else {
									Notification.send(
										req.body.userName,
										"New Conversation",
										message,
										"",
										req.userName,
										data._id,
										function (error_notif, data_notif) {
											res.json({
												response: {
													success: true,
													message: "New Conversation saved Successfully",
													data: data
												}
											});
										}
									);
								}
							});
						}
					}
				);
			}
		}
	});
};

/**
 *@api {post} /api/chat/random/ Create a Random Group Chat
 *@apiVersion 0.0.1
 *@apiGroup Chat
 *@apiPermission all
 *@apiDescription Create a Random chat group by a user
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} type p2p/anon/group
 *@apiParam {String} user_gender gender of user
 *@apiParam {String} chatroom_opt_gender gender of user's choice for chatroom
 *@apiParam {String} lat latitude of user
 *@apiParam {String} lng longitude of user
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "New Conversation saved Successfully",
 *    "data": {
 *      "__v": 0,
 *      "updatedAt": "2017-04-12T16:05:27.936Z",
 *      "createdAt": "2017-04-12T16:05:27.936Z",
 *      "conversationGroupID": "perma_1492013126295",
 *      "title": "",
 *      "imageURL": "",
 *      "creatorUserName": "perma",
 *      "creatorIP": "45.124.5.20",
 *      "creatorLocation": "{\"continent_code\":\"AS\",\"country_code\":\"IN\",\"country_code3\":\"IND\",\"country_name\":\"India\",\"region\":\"West Bengal\",\"city\":\"Kolkata\",\"postal_code\":\"\",\"latitude\":22.569700241089,\"longitude\":88.369697570801,\"dma_code\":0,\"area_code\":0,\"ip\":\"45.124.5.20\"}",
 *      "creatorEpochTimeStamp": "1492013126295",
 *      "isArchived": false,
 *      "type": "single",
 *      "_id": "58ee5047745e9c0b5e012023",
 *      "usersDeletedConvGroup": [],
 *      "usersRemoved": [],
 *      "users": [
 *        "perry",
 *        "perma"
 *      ]
 *    }
 *  }
 *}
 **/

// Create Conversation Group
module.exports.createRandomConversationGroup = function (req, res, next) {
	var distance = 100 / 111.128;
	var lat = parseFloat(req.body.lat);
	var lng = parseFloat(req.body.lng);
	var limit = 1;
	limit = Math.floor(Math.random() * 10 + 5);

	Model.UserDetails.findOne({
		userName: req.userName
	}).exec(function (error, data) {
		if (error) {
			res.json({
				response: {
					success: false,
					message: error_text,
					data: error
				}
			});
		} else {
			var age = parseFloat(data.age);
			var ageStart = age - 5;
			var ageEnd = age + 5;

			var searchquery = [{
					$geoNear: {
						near: [lat, lng],
						maxDistance: 20,
						distanceField: "distance",
						spherical: true
					}
				},
				{
					$match: {
						$and: [{
								user_gender: req.body.chatroom_opt_gender
							},
							{
								chatroom_opt_gender: req.body.user_gender
							},
							{
								userName: {
									$nin: [req.userName]
								}
							},
							{
								chatroom_status: true
							}
						]
					}
				},
				{
					$sample: {
						size: limit
					}
				}
			];

			console.log(JSON.stringify(searchquery, null, '    '));

			Model.UserSockets.aggregate(searchquery).exec(function (error, data_live) {
				if (error) {
					res.json({
						response: {
							success: false,
							message: error_text,
							data: error
						}
					});
				} else {
					console.log(
						"================================================================"
					);
					console.log(data_live);
					console.log(
						"================================================================"
					);
					if (data_live.length < 1) {
						res.json({
							response: {
								success: false,
								message: "There aren’t enough users to connect!",
								data: {}
							}
						});
					} else {
						var userList = [];
						if (data_live.length < 1) {
							res.json({
								response: {
									success: false,
									message: "There aren’t enough users online at this time!",
									data: {}
								}
							});
						} else {
							userList = [];
							for (var i = 0; i < data_live.length; i++) {
								if (
									userList.indexOf(data_live[i]["userName"]) == -1 &&
									data_live[i]["userName"] != req.userName
								) {
									userList.push(data_live[i]["userName"]);
								}
							}

							if (req.body.type == "group") {
								userList.push(req.userName);
							} else {
								var user2 =
									userList[Math.floor(Math.random() * userList.length + 0)];
								req.body.userName = user2;
								userList = [req.userName, user2];
							}

							console.log(userList);

							if (req.body.type == "p2p") {
								next();
							} else {

								Model.ConversationGroupList.findOne({
									$and: [{
											type: req.body.type
										},
										{
											users: {
												$all: [req.userName, req.body.userName]
											}
										}
									]
								}).exec(function (error, data) {
									if (error) {
										return res.json({
											response: {
												success: false,
												message: error_text,
												data: error
											}
										});
									} else {
										console.log(data);
										console.log(userList);

										if (data != null) {
											Model.ConversationList.update({
													conversationGroupID: data.conversationGroupID
												}, {
													usersDeletedConv: userList
												}, {
													upsert: false,
													multi: true
												})
												.exec(function (error, data_list) {
													if (error) {
														return res.json({
															response: {
																success: false,
																message: error_text,
																data: error
															}
														});
													} else {
														ConversationGroups.showOtherData(data, function (err, data) {
															if (error) {
																return res.json({
																	response: {
																		success: false,
																		message: error_text,
																		data: error
																	}
																});
															} else {
																// Notification.send(
																// 	req.body.userName,
																// 	"Resumed Conversation",
																// 	req.userName,
																// 	"",
																// 	req.userName,
																// 	data._id,
																// 	function (error_notif, data_notif) {
																// 		return res.json({
																// 			response: {
																// 				success: true,
																// 				message: "Resume Conversation!",
																// 				data: data
																// 			}
																// 		});
																// 	}
																// );
																return res.json({
																	response: {
																		success: true,
																		message: "Resume Conversation!",
																		data: data
																	}
																});
															}
														});
													}
												});
										} else {
											ConversationGroups.createGroup(req, userList, function (
												error,
												data_conv
											) {
												if (error) {
													return res.json({
														response: {
															success: false,
															message: "Couldn't create Chat Room",
															data: error
														}
													});
												} else {
													console.log(data_conv);
													ConversationGroups.addUser(
														userList,
														data_conv._id,
														function (error, data) {
															if (error) {
																return res.json({
																	response: {
																		success: false,
																		message: "Couldn't add any user!",
																		data: error
																	}
																});
															} else {
																Model.ConversationGroupList.aggregate(
																	[{
																			$match: {
																				_id: data_conv._id
																			}
																		},
																		{
																			$lookup: {
																				from: "userdetails",
																				localField: "users",
																				foreignField: "userName",
																				as: "user_details"
																			}
																		}
																	],
																	function (error, data) {
																		if (error) {
																			return res.json({
																				response: {
																					success: false,
																					message: "Couldn't Load Chat Room",
																					data: error
																				}
																			});
																		} else {
																			var groupData = [];
																			var groupList = data;

																			for (var i = 0; i < groupList.length; i++) {
																				var post = {};

																				for (var property in groupList[i]) {
																					if (groupList[i].hasOwnProperty(property)) {
																						post[property] = groupList[i][property];
																					}
																				}

																				post.last_message = "";
																				post.last_message_time = "";

																				var filters = post["user_details"];
																				filters = filters.reduce((result, filter) => {
																					result[filter.userName] = filter;
																					return result;
																				}, {});
																				post["user_details"] = filters;

																				var users = post["users"];

																				groupData.push(post);
																			}

																			Notification.sendmultiple(
																				0,
																				userList,
																				"New Group Conversation",
																				req.userName +
																				" has added you to a group conversation!",
																				"",
																				req.userName,
																				data_conv._id,
																				function (error_notif, data_notif) {
																					return res.json({
																						response: {
																							success: true,
																							message: "Chat Room Loaded",
																							data: groupData[0]
																						}
																					});
																				}
																			);
																		}
																	}
																);
															}
														}
													);
												}
											});
										}
									}
								});
							}
						}
					}
				}
			});
		}
	});
};

// Create Conversation Group
// module.exports.createRandomConversationGroup = function (req, res, next) {
//   var distance = 100 / 111.128;
//   var lat = parseFloat(req.body.lat);
//   var lng = parseFloat(req.body.lng);
//   var limit = 1;
//   limit = Math.floor(Math.random() * 10 + 5);
//   // if (req.body.type != undefined) {
//   //   if (req.body.type == "group") {
//   //     limit = Math.floor(Math.random() * 10 + 5);
//   //   }
//   // }

//   Model.UserDetails.findOne({
//     userName: req.userName
//   }).exec(function (error, data) {
//     if (error) {
//       res.json({
//         response: {
//           success: false,
//           message: error_text,
//           data: error
//         }
//       });
//     } else {
//       var age = parseFloat(data.age);
//       var ageStart = age - 5;
//       var ageEnd = age + 5;

//       console.log([lat, lng]);

//       Model.UserDetails.aggregate([{
//           $geoNear: {
//             near: [lat, lng],
//             maxDistance: 20,
//             distanceField: "distance",
//             spherical: false
//           }
//         },
//         {
//           $match: {
//             gender: req.body.gender,
//             userName: {
//               $nin: [req.userName]
//             }
//           }
//         },
//         // {
//         //   $lookup: {
//         //     from: "chatrooms",
//         //     localField: "userName",
//         //     foreignField: "userName",
//         //     as: "sockets"
//         //   }
//         // },
//         // {
//         //   $match: {
//         //     sockets: { $exists: true, $ne: [] }
//         //   }
//         // },
//         {
//           $sample: {
//             size: limit
//           }
//         }
//       ]).exec(function (error, data) {
//         if (error) {
//           res.json({
//             response: {
//               success: false,
//               message: error_text,
//               data: error
//             }
//           });
//         } else {
//           console.log(data);
//           if (data.length < 1) {
//             res.json({
//               response: {
//                 success: false,
//                 message: "There aren’t enough users to connect!",
//                 data: {}
//               }
//             });
//           } else {
//             var userList = []; //[req.userName];
//             for (var i = 0; i < data.length; i++) {
//               userList.push(data[i]["userName"]);
//             }
//             console.log(userList);

//             Model.UserSockets.find({
//               $and: [{
//                   userName: {
//                     $in: userList
//                   }
//                 },
//                 {
//                   status: true
//                 }
//               ]
//             }).exec(function (err, data_live) {
//               if (error) {
//                 res.json({
//                   response: {
//                     success: false,
//                     message: error_text,
//                     data: error
//                   }
//                 });
//               } else {
//                 if (data_live.length < 1) {
//                   res.json({
//                     response: {
//                       success: false,
//                       message: "There aren’t enough users online at this time!",
//                       data: {}
//                     }
//                   });
//                 } else {

//                   userList = [];
//                   for (var i = 0; i < data_live.length; i++) {
//                     if (userList.indexOf(data_live[i]["userName"]) == -1 && data_live[i]["userName"] != req.userName) {
//                       userList.push(data_live[i]["userName"]);
//                     }
//                   }

//                   if (req.body.type == "group") {
//                     userList.push(req.userName);
//                   } else {
//                     var user2 = userList[Math.floor(Math.random() * userList.length + 0)]
//                     req.body.userName = user2;
//                     userList = [req.userName, user2];
//                   }

//                   console.log(userList);

//                   if (req.body.type == "p2p") {
//                     next();
//                   } else {
//                     ConversationGroups.createGroup(req, userList, function (
//                       error,
//                       data_conv
//                     ) {
//                       if (error) {
//                         res.json({
//                           response: {
//                             success: false,
//                             message: "Couldn't create Chat Room",
//                             data: error
//                           }
//                         });
//                       } else {
//                         console.log(data_conv);
//                         ConversationGroups.addUser(
//                           userList,
//                           data_conv._id,
//                           function (error, data) {
//                             if (error) {
//                               return res.json({
//                                 response: {
//                                   success: false,
//                                   message: "Couldn't add any user!",
//                                   data: error
//                                 }
//                               });
//                             } else {
//                               Model.ConversationGroupList.aggregate(
//                                 [{
//                                     $match: {
//                                       _id: data_conv._id
//                                     }
//                                   },
//                                   {
//                                     $lookup: {
//                                       from: "userdetails",
//                                       localField: "users",
//                                       foreignField: "userName",
//                                       as: "user_details"
//                                     }
//                                   }
//                                 ],
//                                 function (error, data) {
//                                   if (error) {
//                                     return res.json({
//                                       response: {
//                                         success: false,
//                                         message: "Couldn't Load Chat Room",
//                                         data: error
//                                       }
//                                     });
//                                   } else {
//                                     var groupData = [];
//                                     var groupList = data;

//                                     for (var i = 0; i < groupList.length; i++) {
//                                       var post = {};

//                                       for (var property in groupList[i]) {
//                                         if (
//                                           groupList[i].hasOwnProperty(property)
//                                         ) {
//                                           post[property] = groupList[i][property];
//                                         }
//                                       }

//                                       post.last_message = "";
//                                       post.last_message_time = "";

//                                       var filters = post["user_details"];
//                                       filters = filters.reduce(
//                                         (result, filter) => {
//                                           result[filter.userName] = filter;
//                                           return result;
//                                         }, {}
//                                       );
//                                       post["user_details"] = filters;

//                                       var users = post["users"];

//                                       groupData.push(post);
//                                     }

//                                     Notification.sendmultiple(
//                                       0,
//                                       userList,
//                                       "New Group Conversation",
//                                       req.userName +
//                                       " has added you to a group conversation!",
//                                       "",
//                                       req.userName,
//                                       data_conv._id,
//                                       function (error_notif, data_notif) {
//                                         return res.json({
//                                           response: {
//                                             success: true,
//                                             message: "Chat Room Loaded",
//                                             data: groupData[0]
//                                           }
//                                         });
//                                       }
//                                     );
//                                   }
//                                 }
//                               );
//                             }
//                           }
//                         );
//                       }
//                     });
//                   }
//                 }
//               }
//             });
//           }
//         }
//       });
//     }
//   });
// };

/**
 *@api {get} /api/chat/ Get chat groups
 *@apiVersion 0.0.1
 *@apiGroup Chat
 *@apiPermission all
 *@apiDescription Get all chat groups of a user
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *    "response": {
 *        "success": true,
 *        "message": "All Conversations",
 *        "data": [
 *            {
 *                "users": [
 *                    "toto",
 *                    "lucy",
 *                    "codetanmoy64",
 *                    "codetanmoy32"
 *                ],
 *                "usersRemoved": [],
 *                "usersDeletedConvGroup": [],
 *                "__v": 0,
 *                "type": "group",
 *                "isArchived": false,
 *                "creatorEpochTimeStamp": "1505870391211",
 *                "creatorLocation": "",
 *                "creatorIP": "::ffff:47.15.11.159",
 *                "creatorUserName": "codetanmoy64",
 *                "imageURL": "",
 *                "title": "",
 *                "conversationGroupID": "codetanmoy64_1505870391211",
 *                "createdAt": "2017-09-20T01:19:51.374Z",
 *                "updatedAt": "2017-09-20T01:19:51.665Z",
 *                "_id": "59c1c2374243594bdbeec17a",
 *                "last_message": {
 *                    "createdAt": "2017-10-23T13:46:20.209Z",
 *                    "sender": "lucy",
 *                    "message_text": "Hi"
 *                }
 *                "user_details": [
 *                 {
 *                     "_id": "59405ab18bd9132e2680a513",
 *                     "updatedAt": "2017-09-07T14:05:57.597Z",
 *                     "createdAt": "2017-06-13T21:35:45.786Z",
 *                     "userName": "codetanmoy32",
 *                     "profilePic": "/profilepic/prof_1497389740612.jpg",
 *                     "email": "tanmoykhnara@live.com",
 *                     "dob": "05-08-1987",
 *                     "gender": "Male",
 *                     "phNo": "9735362926",
 *                     "age": "30",
 *                     "location": "Kolkata,India",
 *                     "lat_lng": [
 *                         22.4962665421229,
 *                         88.3611914142966
 *                     ],
 *                     "hobbies": "",
 *                     "bio": "who am I ?",
 *                     "timeZone": null,
 *                     "__v": 0,
 *                     "name": "Naina Poddar",
 *                     "private_mode": false
 *                 },
 *                 {
 *                     "_id": "5941a0335ebac10a8ab3412d",
 *                     "updatedAt": "2017-06-14T20:45:05.805Z",
 *                     "createdAt": "2017-06-14T20:44:35.958Z",
 *                     "userName": "lucy",
 *                     "name": "popo lucy",
 *                     "profilePic": "/profilepic/prof_1497473102817.jpg",
 *                     "email": "tanmoykhanra@lolo.com",
 *                     "dob": "15-06-2017",
 *                     "gender": "Male",
 *                     "phNo": "5678906543",
 *                     "age": "0",
 *                     "location": "Kolkata,India",
 *                     "lat_lng": [
 *                         22.496328225149,
 *                         88.3611998194166
 *                     ],
 *                     "hobbies": "bio",
 *                     "bio": "test",
 *                     "timeZone": null,
 *                     "__v": 0,
 *                     "private_mode": false
 *                 }
 *              ]
 *            },
 *            {
 *                "users": [
 *                    "codetanmoy64",
 *                    "lucy"
 *                ],
 *                "usersRemoved": [],
 *                "usersDeletedConvGroup": [],
 *                "__v": 0,
 *                "type": "p2p",
 *                "isArchived": false,
 *                "creatorEpochTimeStamp": "1508766164053",
 *                "creatorLocation": "",
 *                "creatorIP": "::ffff:103.87.143.57",
 *                "creatorUserName": "codetanmoy64",
 *                "imageURL": "",
 *                "title": "",
 *                "conversationGroupID": "codetanmoy64_1508766164053",
 *                "createdAt": "2017-10-23T13:42:44.177Z",
 *                "updatedAt": "2017-10-23T13:42:44.177Z",
 *                "_id": "59edf1d4aea2dc30458db2c2",
 *                "last_message": {
 *                    "createdAt": "2017-10-23T13:46:20.209Z",
 *                    "sender": "codetanmoy64",
 *                    "message_text": "Hello how are u"
 *                }
 *                "user_details": [
 *                 {
 *                     "_id": "59405ab18bd9132e2680a513",
 *                     "updatedAt": "2017-09-07T14:05:57.597Z",
 *                     "createdAt": "2017-06-13T21:35:45.786Z",
 *                     "userName": "codetanmoy32",
 *                     "profilePic": "/profilepic/prof_1497389740612.jpg",
 *                     "email": "tanmoykhnara@live.com",
 *                     "dob": "05-08-1987",
 *                     "gender": "Male",
 *                     "phNo": "9735362926",
 *                     "age": "30",
 *                     "location": "Kolkata,India",
 *                     "lat_lng": [
 *                         22.4962665421229,
 *                         88.3611914142966
 *                     ],
 *                     "hobbies": "",
 *                     "bio": "who am I ?",
 *                     "timeZone": null,
 *                     "__v": 0,
 *                     "name": "Naina Poddar",
 *                     "private_mode": false
 *                 },
 *                 {
 *                     "_id": "5941a0335ebac10a8ab3412d",
 *                     "updatedAt": "2017-06-14T20:45:05.805Z",
 *                     "createdAt": "2017-06-14T20:44:35.958Z",
 *                     "userName": "lucy",
 *                     "name": "popo lucy",
 *                     "profilePic": "/profilepic/prof_1497473102817.jpg",
 *                     "email": "tanmoykhanra@lolo.com",
 *                     "dob": "15-06-2017",
 *                     "gender": "Male",
 *                     "phNo": "5678906543",
 *                     "age": "0",
 *                     "location": "Kolkata,India",
 *                     "lat_lng": [
 *                         22.496328225149,
 *                         88.3611998194166
 *                     ],
 *                     "hobbies": "bio",
 *                     "bio": "test",
 *                     "timeZone": null,
 *                     "__v": 0,
 *                     "private_mode": false
 *                 }
 *              ]
 *            }
 *        ]
 *    }
 *}
 **/

// Get All types of Conversation Groups where the user is present
module.exports.getAllConversationGroups = function (req, res) {
	Model.ConversationGroupList.aggregate([{
		$match: {
			users: req.userName,
			isArchived: false,
			usersDeletedConvGroup: {
				$nin: [req.userName]
			},
			type: "p2p",
			chatroomFlag: false
		}
	}]).exec(function (error, data) {
		if (error) {
			return res.json({
				response: {
					success: false,
					message: error_text,
					data: error
				}
			});
		} else {
			var grouplist = [];
			for (var i = 0; i < data.length; i++) {
				grouplist.push(data[i].conversationGroupID);
			}

			Model.ConversationList.aggregate([{
					$match: {
						users: {
							$in: [req.userName]
						},
						conversationGroupID: {
							$in: grouplist
						}
					}
				},
				{
					$sort: {
						epochTimeStamp: -1
					}
				},
				{
					$lookup: {
						from: "userdetails",
						localField: "users",
						foreignField: "userName",
						as: "user"
					}
				},
				{
					$lookup: {
						from: "conversationgrouplists",
						localField: "conversationGroupID",
						foreignField: "conversationGroupID",
						as: "group"
					}
				},
				{
					$group: {
						_id: "$conversationGroupID",
						last: {
							$first: "$$ROOT"
						}
					}
				},
				{
					$sort: {
						last: -1
					}
				}
			]).exec(function (error_last, data_last) {
				if (error) {
					return res.json({
						response: {
							success: false,
							message: error_text,
							data: error
						}
					});
				} else {
					//   console.log(data_last);
					var groupListArray = [];
					for (var i = 0; i < data_last.length; i++) {
						var group_item_final = {};
						var group_item = data_last[i];
						var group_item_data = group_item.last.group[0];
						var group_item_user = group_item.last.user;

						for (var property in group_item_data) {
							if (group_item_data.hasOwnProperty(property)) {
								group_item_final[property] = group_item_data[property];
							}
						}

						group_item_final.last_message = group_item.last.message_text;
						group_item_final.last_message_time = group_item.last.epochTimeStamp;

						var group_users = {};

						for (var k = 0; k < group_item_user.length; k++) {
							group_users[group_item_user[k].userName] = group_item_user[k];
						}

						group_item_final.user_details = group_users;

						groupListArray.push(group_item_final);
					}

					return res.json({
						response: {
							success: true,
							message: "All Conversations",
							data: groupListArray
						}
					});
				}
			});
		}
	});
};

// module.exports.getAllConversationGroupsOld = function(req, res) {
//   Model.ConversationGroupList.aggregate([
//     {
//       $match: {
//         users: req.userName,
//         isArchived: false,
//         usersDeletedConvGroup: {
//           $nin: [req.userName]
//         },
//         type: {
//           $nin: ["group"]
//         }
//       }
//     },
//     {
//       $lookup: {
//         from: "userdetails",
//         localField: "users",
//         foreignField: "userName",
//         as: "user_details"
//       }
//     }
//   ]).exec(function(error, data) {
//     if (error) {
//       return res.json({
//         response: {
//           success: false,
//           message: error_text,
//           data: error
//         }
//       });
//     } else {
//       var groupList = data;
//       Model.ConversationList.aggregate([
//         {
//           $sort: {
//             updatedAt: -1
//           }
//         },
//         {
//           $group: {
//             _id: "$conversationGroupID",
//             message: {
//               $first: "$$ROOT"
//             }
//           }
//         },
//         {
//           $project: {
//             _id: 1,
//             "message.sender": 1,
//             "message.message_text": 1,
//             "message.updatedAt": 1
//           }
//         }
//       ]).exec(function(error_last, data_last) {
//         if (error) {
//           return res.json({
//             response: {
//               success: false,
//               message: error_text,
//               data: error
//             }
//           });
//         } else {
//           console.log(data_last);
//           // console.log(groupList);
//           var messageList = {};
//           for (var i = 0; i < data_last.length; i++) {
//             messageList[data_last[i]._id] = data_last[i].message;
//             // console.log(data_last[i]._id);
//             // console.log(data_last[i].message.message_text);
//           }
//           var groupListModified = [];
//           for (var i = 0; i < groupList.length; i++) {
//             var post = {};
//             for (var property in groupList[i]) {
//               if (groupList[i].hasOwnProperty(property)) {
//                 post[property] = groupList[i][property];
//               }
//             }
//             var createdAt = new Date(post["createdAt"]).valueOf().toString();
//             post.last_message = "";
//             post.last_message_time = new Date(post["createdAt"])
//               .valueOf()
//               .toString();
//             var filters = post["user_details"];
//             filters = filters.reduce((result, filter) => {
//               result[filter.userName] = filter;
//               return result;
//             }, {});
//             post["user_details"] = filters;
//             var conversationGroupID = post["conversationGroupID"].toString();
//             var lastMessageObj = messageList[conversationGroupID];
//             var users = post["users"];
//             /*
// 						for (var j = 0; j < users; j++) {
// 							post["user_details"][users[j]] = {
// 								"userName": "lucy",
// 								"name": "popo lucy",
// 								"profilePic": "/profilepic/prof_1497473102817.jpg",
// 								"email": "tanmoykhanra@lolo.com",
// 								"dob": "15-06-2017",
// 								"gender": "Male",
// 								"phNo": "5678906543",
// 								"age": "0",
// 								"location": "Kolkata,India",
// 								"lat_lng": [
// 									22.496328225149,
// 									88.3611998194166
// 								],
// 								"hobbies": "bio",
// 								"bio": "test",
// 								"timeZone": null,
// 								"private_mode": false
// 							};
// 						}*/
//             //console.log(messageList[conversationGroupID]);
//             if (
//               lastMessageObj != undefined &&
//               lastMessageObj.message_text != undefined
//             ) {
//               post["last_message"] = lastMessageObj.message_text;
//               post["last_message_time"] = new Date(lastMessageObj.updatedAt)
//                 .valueOf()
//                 .toString();
//               groupListModified.push(post);
//             }
//           }
//           return res.json({
//             response: {
//               success: true,
//               message: "All Conversations",
//               data: groupListModified
//             }
//           });
//         }
//       });
//     }
//   });
// };

/**
*@api {get} /api/chat/find/:search_text Find in chat groups
*@apiVersion 0.0.1
*@apiGroup Chat
*@apiPermission all
*@apiDescription Find from chat groups of a user
*@apiSampleRequest off
*
*@apiHeader Authorization {String} API key sent while login.
*
*@apiParam {String} search_text Enter Search text
*
*@apiErrorExample Response (Network Error):
*HTTP/1.1 200 OK
*{
* "response": {
*    "success": false,
*    "message": "Something Went Wrong! Please check your network or try again!",
*    "data": null
*  }
*}
*
*@apiSuccessExample Response (example):
*HTTP/1.1 200 OK
{
	"response": {
	"success": true,
	"message": "All Conversations",
	"data": [
		{
		"_id": "58ee521e1a092d0b89d52b22",
		"updatedAt": "2017-04-12T16:13:18.855Z",
		"createdAt": "2017-04-12T16:13:18.855Z",
		"conversationGroupID": "perma_1492013598219",
		"title": "",
		"imageURL": "",
		"creatorUserName": "perma",
		"creatorIP": "45.124.5.20",
		"creatorLocation": "{\"continent_code\":\"AS\",\"country_code\":\"IN\",\"country_code3\":\"IND\",\"country_name\":\"India\",\"region\":\"West Bengal\",\"city\":\"Kolkata\",\"postal_code\":\"\",\"latitude\":22.569700241089,\"longitude\":88.369697570801,\"dma_code\":0,\"area_code\":0,\"ip\":\"45.124.5.20\"}",
		"creatorEpochTimeStamp": "1492013598219",
		"isArchived": false,
		"type": "single",
		"__v": 0,
		"usersDeletedConvGroup": [],
		"usersRemoved": [],
		"users": [
			"perry",
			"perma"
		]
		},
		{
		"_id": "58ee5327a4fce00bc6a40aa7",
		"updatedAt": "2017-04-12T16:17:43.632Z",
		"createdAt": "2017-04-12T16:17:43.632Z",
		"conversationGroupID": "perma_1492013862961",
		"title": "",
		"imageURL": "",
		"creatorUserName": "perma",
		"creatorIP": "45.124.5.20",
		"creatorLocation": "{\"continent_code\":\"AS\",\"country_code\":\"IN\",\"country_code3\":\"IND\",\"country_name\":\"India\",\"region\":\"West Bengal\",\"city\":\"Kolkata\",\"postal_code\":\"\",\"latitude\":22.569700241089,\"longitude\":88.369697570801,\"dma_code\":0,\"area_code\":0,\"ip\":\"45.124.5.20\"}",
		"creatorEpochTimeStamp": "1492013862961",
		"isArchived": false,
		"type": "single",
		"__v": 0,
		"usersDeletedConvGroup": [],
		"usersRemoved": [],
		"users": [
			"perry",
			"perma"
		]
		}
	]
	}
}
**/

module.exports.findInAllConversationGroups = function (req, res) {
	var query = {
		$and: [{
				users: {
					$regex: req.params.search_text
				}
			},
			{
				users: {
					$regex: req.userName
				}
			},
			{
				usersDeletedConvGroup: {
					$nin: [req.userName]
				}
			}
		]
	};

	console.log(query);
	Model.ConversationGroupList.find(query, function (error, data) {
		if (error) {
			return res.json({
				response: {
					success: false,
					message: error_text,
					data: error
				}
			});
		} else {
			return res.json({
				response: {
					success: true,
					message: "Found " + data.length + " Conversations",
					data: data
				}
			});
		}
	});
};

/**
 *@api {post} /api/chat/update/ Update a chat group
 *@apiVersion 0.0.1
 *@apiGroup Chat
 *@apiPermission all
 *@apiDescription Update a chat group by a user
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} _id id of conversation group
 *@apiParam {String} imageURL image url of conversation group
 *@apiParam {String} title if group, then update a title for the conversation
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "Conversation Group Deleted",
 *    "data": {
 *      "_id": "58ee4fef9d93270b49874680",
 *      "updatedAt": "2017-04-12T17:00:34.348Z",
 *      "createdAt": "2017-04-12T16:03:59.921Z",
 *      "conversationGroupID": "perma_1492013039279",
 *      "title": "Friends",
 *      "imageURL": "",
 *      "creatorUserName": "perma",
 *      "creatorIP": "45.124.5.20",
 *      "creatorLocation": "[object Object]",
 *      "creatorEpochTimeStamp": "1492013039279",
 *      "isArchived": false,
 *      "type": "single",
 *      "__v": 0,
 *      "usersDeletedConvGroup": [
 *        "perma"
 *      ],
 *      "usersRemoved": [],
 *      "users": [
 *        "perry",
 *        "perma"
 *      ]
 *    }
 *  }
 *}
 **/

// Update Title and Image link
module.exports.updateConversationGroup = function (req, res) {
	var epoch_milliseconds = new Date().getTime();

	var query = {
		_id: req.body._id
	};
	var updated = {
		title: req.body.title,
		imageURL: req.body.imageURL
	};

	Model.ConversationGroupList.update(query, updated, {
		upsert: false,
		multi: true
	}), exec(function (error, data) {
		if (error) {
			return res.json({
				response: {
					success: false,
					message: error_text,
					data: error
				}
			});
		} else {
			return res.json({
				response: {
					success: true,
					message: "Conversation Group Updated",
					data: data
				}
			});
		}
	});
};

/**
 *@api {post} /api/chat/delete/:_id Delete a chat group
 *@apiVersion 0.0.1
 *@apiGroup Chat
 *@apiPermission all
 *@apiDescription Create a chat group by a user
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} _id id of conversation group
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "Conversation Group Deleted",
 *    "data": {
 *      "ok": 1,
 *      "nModified": 1,
 *      "n": 1
 *    }
 *  }
 *}
 **/

// Soft Delete Conversation Group
module.exports.deleteConversationGroup = function (req, res) {
	var epoch_milliseconds = new Date().getTime();

	var query = {
		_id: req.body._id
	};
	var updated = {
		$addToSet: {
			usersDeletedConvGroup: {
				$each: [req.userName]
			}
		}
	}; //{ $push: { "usersDeletedConvGroup": req.userName } };

	Model.ConversationGroupList.findByIdAndUpdate(req.body._id, updated, function (
		error,
		data
	) {
		if (error) {
			return res.json({
				response: {
					success: false,
					message: error_text,
					data: error
				}
			});
		} else {
			return res.json({
				response: {
					success: true,
					message: "Conversation Group Deleted",
					data: data
				}
			});
		}
	});
};

/**
 *@api {post} /api/chat/user/add Add user to Group
 *@apiVersion 0.0.1
 *@apiGroup Chat
 *@apiPermission all
 *@apiDescription Add a user to a chat group by a user
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} _id id of conversation group
 *@apiParam {String} users users to add for eg: perry,nickel or monty
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "Users added to Conversation Group",
 *    "data": {
 *      "ok": 1,
 *      "nModified": 1,
 *      "n": 1
 *    }
 *  }
 *}
 **/

// Add a user to the Conversation Group
module.exports.addUserToConversationGroup = function (req, res) {
	var userList = req.body.users.split(",");

	ConversationGroups.addUser(userList, req.body._id, function (error, data) {
		if (error) {
			return res.json({
				response: {
					success: false,
					message: error_text,
					data: error
				}
			});
		} else {
			return res.json({
				response: {
					success: true,
					message: "Users added to Conversation Group",
					data: data
				}
			});
		}
	});
};

/**
 *@api {post} /api/chat/user/remove Remove user to Group
 *@apiVersion 0.0.1
 *@apiGroup Chat
 *@apiPermission all
 *@apiDescription Remove a user to a chat group by a user
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} _id id of conversation group
 *@apiParam {String} users users to remove for eg: perry,nickel or monty
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "Users removed from Conversation Group",
 *    "data": {
 *      "ok": 1,
 *      "nModified": 1,
 *      "n": 1
 *    }
 *  }
 *}
 **/

// Remove a user from the Conversation Group
module.exports.removeUserFromConversationGroup = function (req, res) {
	var epoch_milliseconds = new Date().getTime();

	var userList = req.body.users.split(",");

	var query = {
		_id: req.body._id
	};
	var updated = {
		$pull: {
			users: {
				$in: userList
			}
		}
	};

	Model.ConversationGroupList.update(query, updated, {
		upsert: false,
		multi: true
	}).exec(function (error, data) {
		if (error) {
			return res.json({
				response: {
					success: false,
					message: error_text,
					data: error
				}
			});
		} else {
			return res.json({
				response: {
					success: true,
					message: "Users removed from Conversation Group",
					data: data
				}
			});
		}
	});
};

/**
 *@api {post} /api/chat/message/send Send Message
 *@apiVersion 0.0.1
 *@apiGroup Chat
 *@apiPermission all
 *@apiDescription Send message to a chat group by a user
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} conversationGroupID conversationGroupID of chat group
 *@apiParam {String} message_text message text
 *@apiParam {String} message_file file link
 *@apiParam {String} message_type message type
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "New message sent Successfully",
 *    "data": {
 *      "__v": 0,
 *      "updatedAt": "2017-04-12T18:52:52.687Z",
 *      "createdAt": "2017-04-12T18:52:52.687Z",
 *      "conversationsID": "perma_1492023172670",
 *      "conversationGroupID": "58ee50105097e30b50db23b1",
 *      "sender": "perma",
 *      "message_text": "",
 *      "message_file": "/profilepic/image456456467657.jpg",
 *      "message_type": "image",
 *      "epochTimeStamp": "1492023172670",
 *      "_id": "58ee7784647f380f443e2155",
 *      "usersConvNotRead": [
 *        "perma",
 *        "pierce"
 *      ],
 *      "usersDeletedConv": [],
 *      "users": [
 *        "perma",
 *        "pierce"
 *      ]
 *    }
 *  }
 *}
 **/

// Send a message to Conversation Group
module.exports.sendMessageToConversationGroup = function (req, res) {
	console.log("Am in send message")
	Model.UserSessionLog.findOne({
		tokenAssigned: req.headers["authorization"]
	}, function (error_user, data_user) {
		if (error_user) {
			res.json({
				response: {
					success: false,
					message: error_text,
					data: error_user
				}
			});
		} else {
			if (data_user == null) {
				res.status(403).json({
					response: {
						success: false,
						message: "Unauthorised Access",
						data: data_user
					}
				});
			} else {
				Model.UserDetails.findOne({
					userName: data_user.userName
				}, function (error, data_userdetails) {
					if (error) {
						res.json({
							response: {
								success: false,
								message: error_text,
								data: error
							}
						});
					} else {
						if (data_userdetails == null) {
							res.status(404).json({
								response: {
									success: false,
									message: "User details not found",
									data: data_userdetails
								}
							});
						} else {
							//console.log("Request method " + req.method);
							console.log("User status is " + data_userdetails.status);
							if (data_userdetails.status === "BLOCKED") {
								res.json({
									response: {
										success: false,
										message: "We determined that you have violated our terms and conditions found at anaphorachat.com/terms.html. Your account has been suspended. Please contact us if you think this was a mistake at anaphora@usa.com. ",
										data: null

									}
								});
							} else {

								var epoch_milliseconds = new Date().getTime();

								Model.ConversationGroupList.findOneAndUpdate({
										conversationGroupID: req.body.conversationGroupID
									}, {
										hasUnreadMessages: true,
										lastMessageBy: req.userName
									}, {
										new: true
									},
									function (error, data) {
										if (error) {
											return res.json({
												response: {
													success: false,
													message: error_text,
													data: error
												}
											});
										} else {
											if (data != null) {
												var modelData = new Model.ConversationList({
													conversationsID: req.userName + "_" + epoch_milliseconds,
													conversationGroupID: req.body.conversationGroupID,
													users: data.users,
													usersDeletedConv: [],
													usersConvNotRead: data.users,
													sender: req.userName,
													message_text: req.body.message_text,
													message_file: req.body.message_file,
													message_type: req.body.message_type,
													epochTimeStamp: epoch_milliseconds
												});

												modelData.save(function (error, data) {
													if (error) {
														return res.json({
															response: {
																success: false,
																message: error_text,
																data: error
															}
														});
													} else {
														return res.json({
															response: {
																success: true,
																message: "New message sent Successfully",
																data: data
															}
														});
													}
												});
											} else {
												return res.json({
													response: {
														success: false,
														message: "Conversation Group not found!",
														data: null
													}
												});
											}
										}
									}
								);
							}
						}
					}
				});
			}
		}
	});
};

/**
 *@api {post} /api/chat/message/ Get all messages
 *@apiVersion 0.0.1
 *@apiGroup Chat
 *@apiPermission all
 *@apiDescription Get all messages received in a chat group
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} conversationGroupID conversationGroupID of conversation group
 *@apiParam {String} fromTime time of start
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "All Messages",
 *    "data": [
 *      {
 *        "_id": "58ee7784647f380f443e2155",
 *        "updatedAt": "2017-04-12T18:52:52.687Z",
 *        "createdAt": "2017-04-12T18:52:52.687Z",
 *        "conversationsID": "perma_1492023172670",
 *        "conversationGroupID": "58ee50105097e30b50db23b1",
 *        "sender": "perma",
 *        "message_text": "",
 *        "message_file": "/profilepic/image456456467657.jpg",
 *        "message_type": "image",
 *        "epochTimeStamp": "1492023172670",
 *        "__v": 0,
 *        "usersConvNotRead": [
 *          "perma",
 *          "pierce"
 *        ],
 *        "usersDeletedConv": [],
 *        "users": [
 *          "perma",
 *          "pierce"
 *        ]
 *      },
 *      {
 *        "_id": "58ee7a10d5b26e0fe42dc2f2",
 *        "updatedAt": "2017-04-12T19:12:21.832Z",
 *        "createdAt": "2017-04-12T19:03:44.803Z",
 *        "conversationsID": "pierce_1492023824789",
 *        "conversationGroupID": "58ee50105097e30b50db23b1",
 *        "sender": "pierce",
 *        "message_text": "",
 *        "message_file": "/profilepic/image456456467657.jpg",
 *        "message_type": "image",
 *        "epochTimeStamp": "1492023824789",
 *        "__v": 0,
 *        "usersConvNotRead": [
 *          "perma",
 *          "pierce"
 *        ],
 *        "usersDeletedConv": [
 *          "pierce"
 *        ],
 *        "users": [
 *          "perma",
 *          "pierce"
 *        ]
 *      }
 *    ]
 *  }
 *}
 **/

// List all messages of a Conversation Group which a user can see
module.exports.getAllMessagesConversationGroup = function (req, res) {
	Model.ConversationList.find({
			conversationGroupID: req.body.conversationGroupID,
			epochTimeStamp: {
				$lte: req.body.fromTime
			},
			users: {
				$in: [req.userName]
			},
			usersDeletedConv: {
				$nin: [req.userName]
			}
		})
		.sort({
			_id: -1
		})
		.limit(30)
		.exec(function (error, data) {
			if (error) {
				return res.json({
					response: {
						success: false,
						message: error_text,
						data: error
					}
				});
			} else {
				Model.ConversationGroupList.findOneAndUpdate({
						conversationGroupID: req.body.conversationGroupID,
						hasUnreadMessages: true,
						lastMessageBy: {
							$ne: req.userName
						},
					}, {
						hasUnreadMessages: false,
						lastMessageBy: null,
					})
					.then(() => res.json({
						response: {
							success: true,
							message: "All Messages",
							data: data
						}
					}))
					.catch((err) => res.json({
						response: {
							success: true,
							message: error_text,
							data: err,
						}
					}))
				// return res.json({
				// 	response: {
				// 		success: true,
				// 		message: "All Messages",
				// 		data: data
				// 	}
				// });
			}
		});
};

/**
 *@api {post} /api/chat/message/read Mark Read Message
 *@apiVersion 0.0.1
 *@apiGroup Chat
 *@apiPermission all
 *@apiDescription Mark Read to a message received by a chat group
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} _id id of conversation group
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "Message Read!",
 *    "data": {
 *      "ok": 1,
 *      "nModified": 0,
 *      "n": 0
 *    }
 *  }
 *}
 **/

// User has read a message from Conversation Group
module.exports.readMessagesByUserConversationGroup = function (req, res) {
	var epoch_milliseconds = new Date().getTime();

	var query = {
		_id: req.body._id
	};
	var updated = {
		$pull: {
			usersConvNotRead: {
				$in: [req.userName]
			}
		}
	};

	Model.ConversationList.update(query, updated, {
		upsert: false,
		multi: true
	}).exec(function (error, data) {
		if (error) {
			return res.json({
				response: {
					success: false,
					message: error_text,
					data: error
				}
			});
		} else {
			return res.json({
				response: {
					success: true,
					message: "Message Read!",
					data: data
				}
			});
		}
	});
};

/**
 *@api {post} /api/chat/message/delete Delete message
 *@apiVersion 0.0.1
 *@apiGroup Chat
 *@apiPermission all
 *@apiDescription Delete message from a chat group
 *@apiSampleRequest off
 *
 *@apiHeader Authorization {String} API key sent while login.
 *
 *@apiParam {String} _id id of message object
 *
 *@apiErrorExample Response (Network Error):
 *HTTP/1.1 200 OK
 *{
 * "response": {
 *    "success": false,
 *    "message": "Something Went Wrong! Please check your network or try again!",
 *    "data": null
 *  }
 *}
 *
 *@apiSuccessExample Response (example):
 *HTTP/1.1 200 OK
 *{
 *  "response": {
 *    "success": true,
 *    "message": "Message Deleted",
 *    "data": {
 *      "ok": 1,
 *      "nModified": 1,
 *      "n": 1
 *    }
 *  }
 *}
 **/

// Soft Delete a message
module.exports.deleteMessageFromConversationGroup = function (req, res) {
	var epoch_milliseconds = new Date().getTime();

	var query = {
		_id: req.body._id
	};
	var updated = {
		$addToSet: {
			usersDeletedConv: {
				$each: [req.userName]
			}
		}
	}; //{ $push: { usersDeletedConvGroup: req.userName } };

	Model.ConversationList.update(query, updated, {
		upsert: false,
		multi: true
	}).exec(function (error, data) {
		if (error) {
			return res.json({
				response: {
					success: false,
					message: error_text,
					data: error
				}
			});
		} else {
			return res.json({
				response: {
					success: true,
					message: "Message Deleted",
					data: data
				}
			});
		}
	});
};