/*jslint node:true */
"use strict";

//packages
const http = require("http");
const path = require("path");
const express = require("express");
const expressListRoutes = require("express-list-routes");
const cookieParser = require("cookie-parser");
const mongoose = require("mongoose");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const session = require("express-session");
const basicAuth = require("basic-auth");
const passport = require("passport");
const passportLocal = require("passport-local").Strategy;

//create application


var cors = require('cors');

var app = express();
app.use(cors());

// Routed Files
const socketEvents = require("./Controllers/SocketEvents");
const router = require("./Controllers/Router");
const appConfig = require("./Config/appConfig");
const dbConfig = require("./Config/db");
const User = require('./Models/UserDetails');
const ConversationGroupList = require('./Models/ConversationGroupList');

//database connection
mongoose.connect(dbConfig.url, function(error) {
  if (error) {
    console.log(error);
  } else {
    console.log("Connected to Database");
  }
});

let server = app.listen(appConfig.port);
const io = require("socket.io").listen(server);
app.io = io;
socketEvents(io);

//access static folders
app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/uploads"));

//call the middleware
app.use(
  bodyParser.urlencoded({
    extended: true,
    defer: true
  })
);
app.use(bodyParser.json());
app.use(cookieParser());
app.use(
  session({
    secret: "tre56766677",
    resave: true,
    saveUninitialized: true
  })
);

// setting view engine to 'ejs' and views folder to ./views
app.set("view engine", "ejs");
app.set("views", "./Views");

//log all request to console
app.use(morgan("dev"));

// configure our app to handle CORS requests
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
		res.header(
			"Access-Control-Allow-Headers",
			"Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials, Access-Control-Allow-Origin, x-client-key, x-client-token, x-client-secret"
		);
		res.header("Access-Control-Allow-Headers", "Content-Type");
		res.header(
			"Access-Control-Allow-Methods",
			"PUT, GET, POST, DELETE, OPTIONS, PATCH"
		);
		res.header("Access-Control-Allow-Credentials", "true");
		next();
});


const apn = require('apn');
const apnProvider = new apn.Provider({
  token: {
    key: 'apns.p8',
    keyId: 'BF4J9NNA7N',
    teamId: '5CC3PJAUE8',
  },
  production: true,
});

app.post('/alerts', async (req, res) => {
  let deviceToken;
  const { hiddenSecret, receiverId, alert: body, _id, name: subtitle } = req.body;
  const response = {
    success: false,
    message: '',
    data: null,
  };
  if (hiddenSecret && hiddenSecret === 'arshadiosappforfaheel') {
    if (!receiverId || !body || !_id) {
      response.message = 'All fields are mandatory!';
      return res.send({ response });
    }

    // const options = { cert: `${__dirname}/cert.pem`, key: `${__dirname}/key.pem` };
    // const apnConnection = new apn.Provider(options);
    const notification = new apn.Notification();

    try {
      const user = await User.findOne({ userName: receiverId });
      if (!user) throw new Error('No user found!');
      if (!user.deviceToken) throw new Error('No device token found!');

      let badge = user.badgeCount;

      if (!user.badgeCount || user.badgeCount <= 0) {
        user.badgeCount = 1;
        badge = user.badgeCount;
        user.badgeSentBy.push(_id);
        await user.save();
      } else if (user.badgeCount > 0 && user.badgeSentBy && !user.badgeSentBy.includes(_id)) {
        user.badgeCount += 1;
        badge = user.badgeCount;
        user.badgeSentBy.push(_id);
        await user.save();
      }

      deviceToken = user.deviceToken;
      notification.topic = 'com.acco.app';
      notification.alert = body;
      notification.sound = 'ping.aiff';
      notification.badge = badge;
      notification.aps = {
        topic: 'com.acco.app',
        alert: { subtitle, body },
        sound: 'ping.aiff',
        badge: badge || 1,
        'content-available': 1,
        _id,
      };
    } catch (error) {
      response.message = error.message;
      response.data = { error };
      return res.send({ response });
    }

    apnProvider.send(notification, deviceToken)
      .then((result) => {
        response.success = true;
        response.message = 'Notifications sent successfully!';
        response.data = { result };
        return res.send({ response })
      })
      .catch((error) => {
        response.message = error.message;
        response.data = { error };
        return res.send({ response });
      });
  } else {
    response.message = 'You are not authorized!';
    return res.send({ response });
  }
});

app.post('/update-badge-count', (req, res) => {
  const { id, otherId, hiddenSecret } = req.body;
  if (hiddenSecret !== 'arshadiosappforfaheel') return res.redirect({ status: false, message: 'You are not authorized!' });
  if (!id || !otherId) return res.redirect({ status: false, message: 'All fields are mandatory!' });
  return User.findById(id)
    .then((user) => {
      if (user.badgeSentBy.includes(otherId)) {
        user.badgeSentBy = user.badgeSentBy.filter(x => x !== otherId);
        user.badgeCount = user.badgeSentBy.length;
      }
      return user.save();
    })
    .then(user => res.send({ status: true, badgeCount: user.badgeCount }))
    .catch((err) => res.send({ status: false, message: err.message }));
});

// Import routes to be served
router(app);

console.log(
  "Server running at http://localhost:" + (process.env.PORT || appConfig.port)
);

// necessary for testing
//module.exports = server;
