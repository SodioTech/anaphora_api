import os
import re


#clientid 934242183916-3b1fphj2ilpu8uu36b6pg74ponfokkhc.apps.googleusercontent.com
#client secret 1q4WtuDTk-i3qOpF6Lf6ywth
import argparse, commands, time

parser = argparse.ArgumentParser(description='Generate Schema Documentation')
parser.add_argument('--router', help='Router File Path')
parser.add_argument('--model', help='Model File Path')
parser.add_argument('--routers', help='Router File Paths with delimiter \',\'')

args = parser.parse_args()
fileObject = open ( args.router , "r")
fileContent = fileObject.readlines()

API_BASES = {}
for line in fileContent:
    if "/api/" in line:
        lineSplit = re.split("[, : ' \" { } ( ) . ;]", line)
        lineWords = filter(None, lineSplit)
        API_BASES[lineWords[4][1:]+".js"] = lineWords[2]

print API_BASES

docObjectForModel = open("readme", "w")
docObjectForModel.write("Document Generated on " + time.strftime("%a, %d %b %Y %H:%M", time.gmtime()) + "\n\n")
docObjectForModel.write(os.popen("python genDocumentation.py -h").read())
docObjectForModel.write("\n\nModels\n==================\n\n")

path = args.model
fileList = os.listdir(path)

for files in fileList:
    print path + "/" + files
    fileObject = open ( path + "/" + files , "r")
    fileContent = fileObject.readlines()

    #copy variables from file
    ModelName = ""
    Variables = []

    for line in fileContent:
        lineSplit = re.split("[, : ' \" { } ( ) . ; \] \[]", line)
        lineWords = filter(None, lineSplit)

        try:
            index_element = lineWords.index("model")
            ModelName = lineWords[index_element + 1] + " (" + path + "/" + files + ")"
        except ValueError:
            pass

        try:
            index_element = lineWords.index("type")
            Variables.append([lineWords[0], lineWords[index_element + 1]])
        except ValueError:
            pass

    docObjectForModel.write(ModelName + "\n" + ("-"*len(ModelName)))
    for l in Variables:
        docObjectForModel.write("\n" + l[0]+ " : "+ l[1])
    docObjectForModel.write("\n\n")

    fileObject.close()

# docObjectForModel.close()
# docObjectForModel = open("API_Documentation.txt", "w")
# docObjectForModel.write("Document Generated on " + time.strftime("%a, %d %b %Y %H:%M", time.gmtime()) + "\n\n")
docObjectForModel.write("\nRouters\n==================\n\n")

path = args.routers
fileList = []
for pathGiven in path.split(","):
    for fileName in os.listdir(pathGiven):
        if fileName[0] != ".":
            fileList.append(pathGiven+"/"+fileName)

methods = ["post", "put", "get", "delete"]
methodsparams = ["body", "body", "params", "params"]

count = 1

for files in fileList:
    print files
    fileObject = open ( files , "r")
    fileContent = fileObject.readlines()

    #copy variables from file
    APIlink = "-"
    APIMethod = "-"

    for line in fileContent:
        lineSplit = re.split("[, ' \" { } ( ) . ; \n \] \[]", line)
        lineWords = filter(None, lineSplit)

        for i in range(len(methods)):
            if "apiRouter" in lineWords:
                #print line
                #print lineWords
                try:
                    index_element = lineWords.index(methods[i])
                    APIlink = lineWords[index_element + 1]
                    APIMethod = methods[i]
                    toPrint = "\n\n" + str(count) + ". " + APIMethod.upper() + " : " + API_BASES[files] + APIlink + " (" + files + ")\n"
                    count = count + 1
                    docObjectForModel.write(toPrint)
                    docObjectForModel.write("-"*len(toPrint)+ "\n")
                    params = []
                except ValueError:
                    pass

        if APIMethod != "-":
            currentMethodParam = methodsparams[methods.index(APIMethod)]
            for i in range(len(lineWords)):
                if lineWords[i] == currentMethodParam:
                    index_element = i
                    if lineWords[index_element + 1] not in params:
                        params.append(lineWords[index_element + 1])
                        docObjectForModel.write(lineWords[index_element + 1])
                        docObjectForModel.write("\n")
            # try:
            #     index_element = lineWords.index(currentMethodParam)
            #     if lineWords[index_element + 1] not in params:
            #         params.append(lineWords[index_element + 1])
            #         docObjectForModel.write(lineWords[index_element + 1])
            #         docObjectForModel.write("\n")
            # except ValueError:
            #     pass

    fileObject.close()

docObjectForModel.close()
