const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Schema = mongoose.Schema;

const modelSchema = new Schema({
	conversationGroupID: { // name of chat room
		type: String,
		require: true
	},
	userName: { // username of the user
		type: String,
		require: true
	},
	socketID: { // current socket-id of the user
		type: String,
		require: true
	}
}, {
	timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
});

module.exports = mongoose.model('ChatRoom', modelSchema);