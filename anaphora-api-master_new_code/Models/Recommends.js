const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Schema = mongoose.Schema;

const modelSchema  = new Schema({
    userNameRecommended: {type: String, require:true},
    userName: {type: String, require:true},
    recommendedOn: {type: String, require:true}
},{
  timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
});

module.exports = mongoose.model('Recommends', modelSchema);
