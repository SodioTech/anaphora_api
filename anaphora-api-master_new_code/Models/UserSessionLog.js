const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Schema = mongoose.Schema;

const modelSchema  = new Schema({
    userName: {type:String, require:true},
    loginTimeStamp: {type:String},
    tokenAssigned: {type:String}
},{
  timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
});

module.exports = mongoose.model('UserSessionLog', modelSchema);
