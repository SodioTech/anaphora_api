const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Schema = mongoose.Schema;

const modelSchema  = new Schema({
    myUserName: {type: String, require:true}, // username A
    iAmFollowingUserName: {type: String}, // username B (A follows B)
    followTimeStamp: {type: String},
    followRequestAccepted: {type: String} // 0 = deleted, 1 = request sent, 2 = request accepted, 3 = connection revoked
},{
  timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
});

module.exports = mongoose.model('ConnectionList', modelSchema);
