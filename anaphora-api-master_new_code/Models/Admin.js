const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Schema = mongoose.Schema;

const adminProfileSchema  = new Schema({
    name: {
		type: String,
		required: true,
		unique: true
	},
	emailId: {
		type: String,
		default: ""
	},
	role: {
		type: String,
		default: "ADMIN"
	},

	phone : {
		type: String,
		default : ""
	},
},{
  timestamps: true, // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
  usePushEach: true
});

module.exports = mongoose.model('adminsAnaphora', adminProfileSchema);