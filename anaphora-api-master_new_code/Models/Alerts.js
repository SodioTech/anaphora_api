const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Schema = mongoose.Schema;

const modelSchema  = new Schema({
    alertID: {type: String, require:true, unique: true},
    userNameReceiver: {type: String},
    title: {type: String},
    message: {type: String},
    imageUrl: {type: String},
    related_user: {type: String},
    related_data: {type: String},
    epochTimeStampSent: {type: String},
    epochTimeStampRead: {type: String},
    readStatus: {type: String}
},{
  timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
});

module.exports = mongoose.model('Alerts', modelSchema);
