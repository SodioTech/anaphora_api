const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Schema = mongoose.Schema;

const modelSchema = new Schema({
	userName: { // username of the user
		type: String,
		require: true
	},
	socketID: { // current socket-id of the user
		type: String,
		require: true
	},
	status: { // staus of the user => false (offline), true (online)
		type: Boolean,
		default: true,
		require: true
	},
	client: {
		type: String,
		require: true
	},
	user_gender: {
		type: String,
		default: "-",
		require: true
	},
	chatroom_status: {
		type: Boolean,
		default: false,
		require: true
	},
	chatroom_opt_type: {
		type: String,
		default: "-",
		require: true
	},
	chatroom_opt_gender: {
		type: String,
		default: "-",
		require: true
	},
	chatroom_user_coord: {
		type: [Number],
		index: '2d',
		require: true
	},
	conversationGroupID: {
		type: String,
		default: "-",
		require: true
	},
	connected: {
		type: Boolean,
		default: false,
		require: true
	}
}, {
	timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
});

module.exports = mongoose.model('UserSockets', modelSchema);