const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Schema = mongoose.Schema;

const modelSchema  = new Schema({
    conversationsID: {type: String, require:true, unique: true}, // ID for conversation
    conversationGroupID: {type: String}, // conversationGroupID of the conversation group
    users: {type: [String]}, // users participating at that moment
    usersDeletedConv: {type: [String]}, // users who have deleted the message from their app
    usersConvNotRead: {type: [String]}, // users who have not read the message
    sender: {type: String}, // sender of the message
    message_text: {type: String}, // message text
    message_file: {type: String}, // message file link
    message_type: {type: String}, // message type
    epochTimeStamp: {type: String} // when was the message sent
},{
  timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
});

module.exports = mongoose.model('ConversationList', modelSchema);
