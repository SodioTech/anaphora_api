const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Schema = mongoose.Schema;

const modelSchema  = new Schema({
    postID: {type: String, require:true},
    userName: {type: String, require:true},
    message: {type: String},
    logTimeStamp: {type: String}
},{
  timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
});

module.exports = mongoose.model('Comments', modelSchema);
