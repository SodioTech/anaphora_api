const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Schema = mongoose.Schema;

const modelSchema  = new Schema({
    userName: {type: String, require:true, unique: true},
    name: {type: String},
    profilePic: {type: String},
    email: {type: String},
    dob: {type: String},
    gender: {type: String},
    phNo: {type: String, require:true, unique: true},
    age: {type: String},
    location: {type: String},
    lat_lng: {type:[Number], index: '2d'},
    hobbies:{type:String},
    bio:{type:String},
    private_mode:{type:Boolean},
    timeZone: {type:String}, // +5:30 or -6:00
    homeTown: String,
    role: {type: String},
    status: {type: String, default: "ACTIVE"},
    deviceToken: String,
    badgeSentBy: [String],
    badgeCount: {
      type: Number,
      default: 0,
    },
},{
  timestamps: true, // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
  usePushEach: true
});

module.exports = mongoose.model('UserDetails', modelSchema);
