const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Schema = mongoose.Schema;

const modelSchema  = new Schema({
    conversationGroupID: {type: String, require:true, unique: true}, // primary key ID for a conversation group
    title: {type: String}, // title of conversation group
    imageURL: {type: String}, // imageURL of conversation group
    users: {type: [String]}, // users participating
    usersRemoved: {type: [String]}, // participating users who were removed
    usersDeletedConvGroup: {type: [String]}, // users who have deleted the conversation group from their app
    creatorUserName: {type: String}, // user who started the conversation
    creatorIP: {type: String}, // ip of the initiatorUserName
    creatorLocation: {type: String}, // location of the initiatorUserName
    creatorEpochTimeStamp: {type: String}, // timestamp of when initiatorUserName created the conversation
    isArchived: {type: Boolean}, // only the admin of DB can archive the conversation
    type: {type: String},
    limit: {type: Number},
    chatroomFlag: {type: Boolean},
    hasUnreadMessages: Boolean,
    lastMessageBy: String,
},{
  timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
});

module.exports = mongoose.model('ConversationGroupList', modelSchema);
