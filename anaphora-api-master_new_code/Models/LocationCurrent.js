const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Schema = mongoose.Schema;

const modelSchema  = new Schema({
    userName: {type:String, require:true, unique: true},
    userLocation: {type:[Number], index: '2d', require:true},
    locationName : {type:String, require:true},
    logTimeStamp: {type: String}
},{
  timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
});

module.exports = mongoose.model('LocationCurrent', modelSchema);
